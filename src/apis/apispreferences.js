const API = 'https://randomuser.me/api/?results=5';
import {  urls } from '../Constants';


 getFormData = object => Object.keys(object).reduce((formData, key) => {
    formData.append(key, object[key]);
    return formData;
  }, new FormData());

  export const fetchPreferences  = async(object) => {
  let url = urls.base_url +'api/models_list'

 // console.log("URL",url)
  return fetch(url).then( (res) => res.json() );


  }




  export const fetchPosts  = async (object) => {
    let url = urls.base_url +'api/home_screen'


    var formData = getFormData(object)
  
    
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data',
      },
     body: formData

    }).then( (res) => res.json() );
  
  
    }


  // fetchPreferences()
  // .then( (data) => {
  //   let results = data.results;
  //   this.setState({
  //     profiles: results
  //   });
  // });
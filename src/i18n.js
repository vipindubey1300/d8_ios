import I18n from 'react-native-i18n';


import en from './locales/en';
import fr from './locales/fr';
import hi from './locales/hi';
import th from './locales/th';
import zh from './locales/zh';
import es from './locales/es';
import ru from './locales/ru';
import de from './locales/de';
import it from './locales/it';

I18n.fallbacks = true;

I18n.translations = {
  en,
  fr,
  hi,
  th,
  zh,
  es,
  ru,
  de,
  it
};

export default I18n;

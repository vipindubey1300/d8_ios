import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,Dimensions} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';


import {  colors,urls ,dimensions} from '../Constants';

const defaultPlaceholder = {
  label: 'Select',
  value: null,
  color: colors.COLOR_PRIMARY,
  
};


export default class CustomDropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data ? this.props.data : [],
      selectedValue:null,
      selectedIndex:null,
      errorState:false ,
      errorText:''
    };
  

}
  

  //both below methods are called when parent update state and that state is props for this component

	// componentWillReceiveProps(nextProps) {
	// 	// You don't have to do this check first, but it can help prevent an unneeded render
	
  //   if (nextProps.error !== this.state.errorState) {
    
  //       this.setState({ errorState: nextProps.error,errorText:'sasdsadasdsdsd' },
  //       ()=>{
  //         console.log('componentWillReceiveProps.---------',this.state.errorText)
  //       });
  //   }
    
  //  console.log('componentWillReceiveProps......',nextProps)
  // }
  
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('shouldComponentUpdate',nextState)
  //  	if (nextProps.error !== this.state.errorState) {
  //     this.setState({ errorState: nextProps.error,errorText:'sasdsadasdsdsd' });
  //     return true
  //   }
  // }


  //

  getSelectedValue = () => ({
    selected_value:this.state.selectedValue,
    selected_index : this.state.selectedIndex

  })

  render() {
    const { selectedIndex, selectedValue } = this.state;
   


    return (
      <View>
               <View style={[styles.container, this.props.style]}>

                    <Image source ={this.props.inputImage}

                        style={{position:'absolute',height:40,width:45,bottom:0.4,top:0.1}}
                        resizeMode={'cover'}
                      />
      
                          <RNPickerSelect
                              //useNativeAndroidPickerStyle={false}
                              placeholder={this.props.placeholder ? this.props.placeholder : defaultPlaceholder}
                              placeholderTextColor={colors.COLOR_PRIMARY}
                              value={this.state.selectedValue}
                              onValueChange={(value,index) => {
                               this.props.onSelectValue ?  this.props.onSelectValue(value) : null

                                this.setState({
                                  
                               
                                  selectedValue: value,
                                  selectedIndex:index,
                                  errorState:false
                                  },
                                  ()=>{
                                    //selectedvalue will return value in data 
                                    //selected index is the index of selected data
                                    console.log('useNativeAndroidPickerStyle.---------',this.state.selectedValue)
                                  })
                              }}
                              items={this.props.data}
                              style={{
                                ...pickerSelectStyles,

                              }} />

              </View>
                        
                  
                      {
                        this.state.errorState  === true
                        ?
                        <Text style={{color:'red',textAlign:'center'}}>{this.state.errorText}</Text>
                        : null
                      }

              

       

      </View>
    );

   


  }
}

CustomDropDown.defaultProps = {

  style: {},
  placeholder: defaultPlaceholder,

  inputImage : require('../assets/user.png')
};

const styles = StyleSheet.create({
  container: {
    borderRadius:30,
    borderWidth:2,
    borderColor:colors.LIGHT_MAROON,
    //height:45,
    width:Dimensions.get('window').width * 0.85,
    backgroundColor:colors.DARK_MAROON,
    flexDirection:'row',
    alignContent:'center',
    textAlign:"center",
  

  },

  inputText: {
    textAlign:'center',
    fontSize: 16,
    flex:8,
    color:colors.WHITE,
    paddingRight:10,
   // height:45,
   // height:45,
   paddingTop:Platform.OS === 'ios' ? 15 : 5,
   paddingBottom: Platform.OS === 'ios' ? 15 : 5,
   
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    backgroundColor:"transparent",
    fontSize: 16,
    width:Dimensions.get('window').width * 0.72,
    color: 'white',
    alignItems:"center",
    height:40,
    textAlign:"center",
    justifyContent:"center",
    alignSelf:"center",
    alignItems:"center",
    marginLeft: dimensions.SCREEN_WIDTH * 0.08,
   // paddingLeft:50
   
  },
  inputAndroid: {
    height:40,
    fontSize: 16,
    width: dimensions.SCREEN_WIDTH * 0.72,
    marginLeft: dimensions.SCREEN_WIDTH * 0.31,
    color: 'white',
    alignItems:"center",
    textAlign:"center",
    justifyContent:"center",
    alignSelf:"center",
    fontSize:14
   
   
  },
});

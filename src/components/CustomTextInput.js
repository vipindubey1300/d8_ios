import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform} from 'react-native';

import {  colors,urls } from '../Constants';

export default class CustomTextInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
    errorState:false,
    errorText:''
  };

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
    const showPlaceHolderTop = isFocus || text !== '';


    return (
      <View>
      <View style={[styles.container, this.props.style]}>

      <Image source ={this.props.inputImage}
   //style={{height:'100%',flex:2,alignSelf:'flex-start'}}
      style={{position:'absolute',height:45,width:45}}
      resizeMode={'cover'}
     />
      
        <TextInput
          style={[styles.inputText,{paddingLeft : 45,paddingRight : 45}]}
          value={this.state.text}
          autoCapitalize={this.props.autoCapitalize}
          //autoCorrect={false}
          numberOfLines={1}
          maxLength ={this.props.maxLength}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          ref={this.props.inputRef}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          placeholder={this.props.placeholder}
          textContentType={this.props.textContentType}
          onSubmitEditing={this.props.onSubmitEditing}
          placeholderTextColor={colors.COLOR_PRIMARY}
          onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text, errorState:false })}
          editable={this.props.editable}
          multiline={false}
          numberOfLines={1}
          blurOnSubmit={false}
        />
        
      </View>
      {
          this.state.errorState
          ?
          <Text style={{color:'red',textAlign:'center'}}>{this.state.errorText}</Text>
          : null
        }
      </View>
    );

   


  }
}

CustomTextInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: '',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
  maxLength:20,
  inputImage : require('../assets/user.png')
};

const styles = StyleSheet.create({
  container: {
   flexDirection:'row',
   alignItems:'center',
   borderColor:colors.LIGHT_MAROON,
   borderWidth:2,
   borderRadius:30,
   backgroundColor:colors.DARK_MAROON,
   margin:7

  },

  inputText: {
    textAlign:'center',
    fontSize: 16,
    flex:8,
    color:colors.WHITE,
    paddingRight:10,
   // height:45,
   // height:45,
   paddingTop:Platform.OS === 'ios' ? 15 : 5,
   paddingBottom: Platform.OS === 'ios' ? 15 : 5,
   
  },
});

import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,Animated,Easing} from 'react-native';
import {  colors,urls,dimensions } from '../Constants';

export default class LogoProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }

        this.RotateValueHolder = new Animated.Value(0);

    }

    StartImageRotateFunction () {

        this.RotateValueHolder.setValue(0)
        
        Animated.timing(
          this.RotateValueHolder,
          {
            toValue: 1,
            duration: 4000,
            easing: Easing.bounce
          }
        ).start(() => this.StartImageRotateFunction())
      
      }
  
  
      componentDidMount(){
       
        this.StartImageRotateFunction();
        
      }
  

	

  render() {
   
    const RotateData = this.RotateValueHolder.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg']
      });


    return (
        <View style ={styles.userLoadingContainer}>
        <Animated.Image   source={require('../assets/logo.png')} 
        resizeMode='contain'
        style={{alignSelf:"center", 
        height:100,width: 100, marginTop:-50,
      transform: [{rotate: RotateData}] }}  />

      <Text style={{
        color:colors.COLOR_PRIMARY,
        fontSize:17,
        fontWeight:'bold',
        marginTop:20,textAlign:'center'

      }}>{this.props.title}</Text>

    </View>
    );

   


  }
}

LogoProgressBar.defaultProps = {
 
  title: 'D8 ! A place to have fun and enjoy!',
  
};

const styles = StyleSheet.create({
    userLoadingContainer:{
        height:dimensions.SCREEN_HEIGHT * 1,
        width:dimensions.SCREEN_WIDTH * 1,
        backgroundColor:colors.BLACK,
        justifyContent:'center',
        alignItems:'center',
        padding:30
      }
});

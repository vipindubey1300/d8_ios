import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {Header,Overlay} from 'react-native-elements';
import RBSheet from "react-native-raw-bottom-sheet";
import I18n from '../i18n';


const dimension = Dimensions.get("window")

import Geocoder from 'react-native-geocoder';
import Geolocation from '@react-native-community/geolocation';


const mapStyle =[
  [
    {
      "featureType": "poi.business",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.government",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.medical",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.place_of_worship",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.school",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.sports_complex",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ]
]



export default class LocationComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          region: {
            latitude: 0.0,
            longitude: 0.0,
            latitudeDelta: 0.2,
            longitudeDelta: 0.9
          },
          coordinates:{
            latitude: 0.0,
            longitude: 0.0,
          },
          current_latitude:0.00,
          current_longitude:0.00,
          nearby_latitude: 0.0,
          nearby_longitude: 0.0,
          current_address:'',
          nearby_address:'',
          myPlaces:[],currentLocation:''

      };


    }

    getCurrentLocation(lat,long){
      var NY = {
          lat: lat,
          lng: long
      };

      Geocoder.geocodePosition(NY).then(json => {
  
           //console.log(json)
          var addressComponent = json[0].formattedAddress
          this.setState({currentLocation:addressComponent})
      })
      .catch(err => console.log(err))
    }

    getNearby(){
             let url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+this.state.current_latitude+','+this.state.current_longitude+'&radius=3500&types=restaurant,bar,food&key=AIzaSyBx5f8NnFiA2kEv7ZcFJVtUs0_6TfZaMPw'
                fetch(url, {
                method: 'GET',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type':  'multipart/form-data',
                },
   
   
              }).then((response) => response.json())
                    .then((responseJson) => {
                      this.setState({loading_status:false})
                     var places = responseJson.results
                     this.setState({
                       myPlaces:places
                     })
                     
                    }).catch((error) => {
                     // ToastAndroid.show("Connection Error !", ToastAndroid.SHORT);
                      Platform.OS === 'android'
                      ?  ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
                      : Alert.alert("Connection Error !")
   
                    })
   
   
                   }
          //this one current
     getaddress(lat,long){
       // Position Geocoding
                 var NY = {
                   lat:lat,
                   lng: long
                 };   
                 Geocoder.geocodePosition(NY).then(res => {
                   //  ToastAndroid.show(JSON.stringify(res), ToastAndroid.LONG);
                   var addr  = res[0].formattedAddress
                     this.setState({current_address: addr.toString()})
                     this.getNearby()
   
   
                 })
                 .catch(err =>{
                  // ToastAndroid.show("Cannot get this location !",ToastAndroid.SHORT);
                   Platform.OS === 'android'
                   ? ToastAndroid.show("Cannot get this location !", ToastAndroid.SHORT)
                   : Alert.alert("Cannot get this location !")
                 this.setState({loading_status:false});
                 })
     }
          async  requestLocationPermission(){
           try {
             const granted = await PermissionsAndroid.request(
               PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
               {
                 'title': 'Example App',
                 'message': 'Example App access to your location '
               }
             )
             if (granted === PermissionsAndroid.RESULTS.GRANTED) {


                                
                    Geolocation.getCurrentPosition(
                        position => {
                            this.setState({
                                region: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                latitudeDelta: 0.23,
                                longitudeDelta: 0.5,
                                accuracy: position.coords.accuracy
                            },
                            coordinates:{
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude
                            },
                            current_latitude: position.coords.latitude,
                            current_longitude: position.coords.longitude,


                            });
                            this.getaddress(position.coords.latitude, position.coords.longitude)
                            this.getCurrentLocation(position.coords.latitude, position.coords.longitude)

                        },
                        error => {
                       
                           ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
                      


                        //this.setState({loading_status:false})
                        },
                        { enableHighAccuracy: false, timeout: 10000}
                    );
   
   
   
   
             } else {
               console.log("location permission denied")
               Alert.alert("location permission denied");
               //alert("Location permission denied");
             }
           } catch (err) {
             console.warn(err)
             Alert.alert(err.message);
           }
         }
   
   
         //https://maps.googleapis.com/maps/api/place/nearbysearch/json?
         //location=28.8670,77.1957&radius=3500&types=restaurant&key=AIzaSyBx5f8NnFiA2kEv7ZcFJVtUs0_6TfZaMPw
   
           async  requestLocationPermissionIOS(){
   
   
                           
                    Geolocation.getCurrentPosition(
                        position => {
                            this.setState({
                                region: {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude,
                                latitudeDelta: 0.23,
                                longitudeDelta: 0.5,
                                accuracy: position.coords.accuracy
                            },
                            coordinates:{
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude
                            },
                            current_latitude: position.coords.latitude,
                            current_longitude: position.coords.longitude,


                            });
                            this.getaddress(position.coords.latitude, position.coords.longitude)
                               this.getCurrentLocation(position.coords.latitude, position.coords.longitude)

                        },
                        error => {
                        //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);

                         ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
                       


                        //this.setState({loading_status:false})
                        },
                        { enableHighAccuracy: false, timeout: 10000}
                    );
         }
   
   
   
   async componentDidMount(){
     Platform.OS === 'android'
     ?  await this.requestLocationPermission()
     :  await this.requestLocationPermissionIOS()
   
   
     AsyncStorage.getItem('user_id')
     .then((item) => {
               if (item) {
   
                       this.setState({user_id:item})
   
             }
             else {
               ToastAndroid.show("Error !")
               }
     });

     
   
   
   }
   componentWillMount(){
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
   }
   
   
         sendLocation(addr,lat,lng){

            let obj = {
                'address':addr,
                'latitude':lat,
                'longitude':lng
            }
            this.props.getLocation(obj)

   //
    //        const { navigation } = this.props;
    //              navigation.goBack();
   
    //       // ToastAndroid.show(addr+',,'+lat+'...'+lng,ToastAndroid.SHORT)
    //       navigation.state.params.onSelect({ address: addr,
    //        latitude:lat,
    //        longtitude:lng,
    //    })
   
         }





    render() {
      const { card, index } = this.props;
  
      return(
        <RBSheet
        // ref={ref => {
        //   this.RBSheet = ref;
        // }}
        ref={this.props.inputRef}
        height={dimension.height * 0.64}
        //closeOnDragDown
        duration={240}
        customStyles={{
          container: {
          
            alignItems: "center",
            backgroundColor:'rgba(0,0,0,0.9)',
            padding:10,
            borderTopLeftRadius:15,
          borderTopRightRadius:15
          }
        }}
        animationType='fade'
        minClosingHeight={10}
      >
      <View>
                <Text style={{
                    color:colors.COLOR_PRIMARY,
                    fontSize:15,
                    fontWeight:'900',
                    margin:16,textAlign:'center'
            }}>
           { I18n.t('choose_location_chat')}
            </Text>


                    <View style={{width:'90%',margin:5,flexDirection:'row',justifyContent:'space-between',
                    alignItems:'center',
                    borderColor:'white',
                   padding:3,alignSelf:'center',backgroundColor:'grey'}}>
                    

                    <View style={{flexDirection:'row',alignItems:'center'}}>
                      <Image source={require('../assets/map-icon.png')} style={{width:25,height:25}} 
                      resizeMode='contain'/>
                    <Text style={{width:'85%',color:'white',marginLeft:5}}>{this.state.currentLocation}</Text>
                    </View>


                    <TouchableOpacity onPress={()=> this.sendLocation(this.state.currentLocation,
                     this.state.current_latitude,this.state.current_longitude)}>
                    <Image style={{width: 25, height: 25}}  source={require('../assets/send-white.png')} />
                    </TouchableOpacity>

                   
                 </View>



              <FlatList
            style={{marginBottom:10}}
            data={this.state.myPlaces}
                showsVerticalScrollIndicator={false}
                scrollEnabled={true}
                renderItem={({item}) =>

                   
                   
                    <View style={{width:'90%',margin:5,flexDirection:'row',justifyContent:'space-between',
                    alignItems:'center',
                    borderColor:'white',
                   padding:3,alignSelf:'center'}}>
                    

                    <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image source={{uri:item.icon}} resizeMode='contain'
                    style={{height:30,width:30,marginLeft:5,marginRight:9}}/>
                    <Text style={{width:'65%',color:'white'}}>{item.name}</Text>
                    </View>


                    <TouchableOpacity onPress={()=> this.sendLocation(item.name,item.geometry.location.lat,item.geometry.location.lng)}>
                    <Image style={{width: 25, height: 25}}  source={require('../assets/send-white.png')} />
                    </TouchableOpacity>



                   
                 </View>

                }
                keyExtractor={item => item.id}
             />
             </View>


         {/** bottom three buttons  end*/}


      </RBSheet>
      )
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},

}
)
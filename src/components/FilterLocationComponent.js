import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform,ActivityIndicator
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {Header,Overlay} from 'react-native-elements';
import RBSheet from "react-native-raw-bottom-sheet";
import RNPickerSelect from 'react-native-picker-select';
import { Chip } from 'react-native-paper';

import Icon from 'react-native-vector-icons/AntDesign';
const myIcon = <Icon name="caretdown" size={10} color={colors.COLOR_PRIMARY}  style={{marginTop:13}}/>;;
import I18n from '../i18n';

const dimension = Dimensions.get("window")

import Geocoder from 'react-native-geocoder';
import Geolocation from '@react-native-community/geolocation';

import {showMessage} from '../config/snackmsg';




export default class FilterLocationComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          cities:[],
          states:[],
          countries:[],
          city_boolean: [],
          city_id:[],
          city_names:[],
          final_city_boolean:[]
         

      };


    }
    getSelectedValue = () => ({
        city_boolean:this.state.final_city_boolean,
        city_id:this.state.city_id
      })

    _fetchCountries = async () =>{
        this.setState({loading_status:true})
    
                   let url = urls.BASE_URL +'api/countries'
                        fetch(url, {
                        method: 'GET',
    
                        }).then((response) => response.json())
                            .then((responseJson) => {
    
                              this.setState({loading_status:false})
                              if(!responseJson.error){
                               
                                var temp_arr=[]
                                
                                var country = responseJson.result
                                country.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({countries:country})
    
                              }
    
    
                            else{
                              showMessage(responseJson.message)
                            }
    
                          }
                            ).catch((error) => {
    
                              this.setState({loading_status:false})
                              showMessage("Try Again.")
                            });
    
    
    
      }
  
     _fetchStates = async (countryId) =>{
          console.log("Fetching---",countryId)
         this.setState({loading_status:true,states:[],cities:[]})
    
                   let url = urls.BASE_URL +'api/states?country_id=' + countryId
                        fetch(url, {
                        method: 'GET',
    
                        }).then((response) => response.json())
                            .then((responseJson) => {
    
                              this.setState({loading_status:false})
                              if(!responseJson.error){
                               
                                if(responseJson.result.length > 0){
                                  var temp_arr=[]
                                
                                  var state = responseJson.result
                                  state.map(item=>{
                                    Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                  })
                                  this.setState({states:state})
                                }
                                else{
                                  showMessage('No State Found')
                                }
    
                              }
    
    
                            else{
                              showMessage(responseJson.message)
                            }
    
                          }
                            ).catch((error) => {
    
                              this.setState({loading_status:false})
                              showMessage("Try Again.")
                            });
    
    
    
      }
  
     _fetchCities = async (stateId) =>{
        this.setState({loading_status:true,cities:[]})
    
                   let url = urls.BASE_URL +'api/cities?state_id=' + stateId
                        fetch(url, {
                        method: 'GET',
    
                        }).then((response) => response.json())
                            .then((responseJson) => {
    
                              this.setState({loading_status:false})
                              if(!responseJson.error){
                               
                                if(responseJson.result.length > 0){
                                  var temp_arr=[]
                                
                                  var city = responseJson.result
                                  city.map(item=>{
                                    Object.assign(item,{value:item._id,label:item.name,color:'black'})

                                    let cit = [...this.state.city_names];
                                    cit.push( item.name );
                                    let citys_ids = [...this.state.city_id];  
                                    citys_ids.push(item._id );
                                    let citys_bool = [...this.state.city_boolean];  
                                    citys_bool.push( false );
  
                                      this.setState({ city_id:citys_ids,
                                        city_names:cit,
                                        city_boolean:citys_bool });
                                  })
                                  this.setState({cities:city})
                                }
                                else{
                                  showMessage('No City Found')
                                }
    
                              }
    
    
                            else{
                              showMessage(responseJson.message)
                            }
    
                          }
                            ).catch((error) => {
    
                              this.setState({loading_status:false})
                              showMessage("Try Again.")
                            });
    
    
    
      }
  

 
    


    componentWillMount() {
        this._fetchCountries()

        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });   
      }

      _submit=()=>{
          var temp = this.state.city_boolean
          this.setState({
                final_city_boolean:temp
          })
          this.props.closeThis()
      }



    render() {

        let chips = this.state.city_names.map((r, i) => {

            return (
                  <Chip 
                  style={{width:null,margin:4}}
                  mode={'outlined'}
                  selected={this.state.city_boolean[i]}
                
                  selectedColor={'black'}
                  onPress={() => {
                          let ids = [...this.state.city_boolean];    
                          ids[i] =!ids[i];                  
                          this.setState({city_boolean: ids });         
                  }}>
                  {r}
                  </Chip>
            );
         })
     
  
        return(
            <RBSheet
            ref={this.props.inputRef}
            height={430}
           
            duration={240}
            customStyles={{
              container: {
                alignItems: "center",
                backgroundColor:colors.DARK_MAROON,
                padding:10,
                borderTopLeftRadius:1,
              borderTopRightRadius:1
              }
            }}
            animationType='fade'
            minClosingHeight={10}
          >

           {/** top content */}
          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',width:'100%'}}>
            <Text 
            style={{color:colors.COLOR_PRIMARY,textAlign:'center',fontWeight:'bold'}}>
            { I18n.t('choose_location_search')}</Text>

            <TouchableOpacity onPress={()=> this._submit()}>
            <View style={{backgroundColor:colors.LIGHT_GREEN,justifyContent:'center',
            alignItems:'center',paddingTop:4,paddingBottom:4,paddingLeft:10,paddingRight:10,borderRadius:20}}>
            <Text 
            style={{color:colors.BLACK,textAlign:'center',fontWeight:'bold',
           textTransform:'uppercase'}}>{ I18n.t('ok')}
           </Text>
            </View>
            </TouchableOpacity>

           </View>

           {/** top content end */}

           {/** country state dropwdown */}
           <View style={{flexDirection:'row',alignItems:'center',width:'100%',padding:2,marginTop:10}}>

           <View style={styles.dropDownParent}>
           
             <RNPickerSelect
             style={pickerSelectStyles}
            
             items={this.state.countries}
             Icon={() => {
               return myIcon;
             }}
             placeholder={{
              label:  I18n.t('country'),
              value: null,
              color: colors.COLOR_PRIMARY,
            }}
            value={this.state.selectedValue}
            onValueChange={(value,index) => {
             if(value) this._fetchStates(value)
            }}
           />
           </View>

           <View style={styles.dropDownParent}>
           
           <RNPickerSelect
           style={pickerSelectStyles}
           onValueChange={(value) => console.log(value)}
           items={this.state.states}
           Icon={() => {
             return myIcon;
           }}
           placeholder={{
            label: I18n.t('state'),
            value: null,
            color: colors.COLOR_PRIMARY,
          }}
          onValueChange={(value,index) => {
            if(value) this._fetchCities(value)
           }}
         />
         </View>


    </View>



           {/** country state dropwdown  end*/}


            {/** bottom three buttons */}
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{flexDirection:'row',flexWrap: 'wrap',marginTop:15}}>
                  {chips}
               </View>
  
             {/** bottom three buttons  end*/}

             {
                 this.state.loading_status 
                 ? <ActivityIndicator/>
                 : null
             }
  
           </ScrollView>
          </RBSheet>
          )
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},
dropDownParent:{flex:1,
    margin:3,
    borderRadius:20,borderWidth:0.7,
    borderColor:'white',
    padding:5
  }

}
)

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
       width:'90%',
      fontSize: 16,
    marginTop:5,
      color: colors.COLOR_PRIMARY,
    
    },
    inputAndroid: { height:40,
      width:'90%',
      fontSize: 13,
    
      color: colors.COLOR_PRIMARY,
    },
  });
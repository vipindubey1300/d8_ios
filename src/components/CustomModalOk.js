import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {Header,Overlay} from 'react-native-elements';


const d = Dimensions.get("window")





export default class CustomModalOk extends React.Component {
    state = {
        visibility:false
    };



  buttonClick =()=>{
      this.props.buttonClick()
  }
 


    render() {
      const { card, index } = this.props;
  
      return(
           
          <Overlay
          isVisible={this.state.visibility}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="white"
          width={300}
          height='auto'
          overlayStyle={styles.overlayStyle}
          
        >
  
          <View style={styles.mainLayout}>
                <View style={styles.topImageBorder}>
                <Image source={require('../assets/icon2.png')} 
                  style={{width:50,height:50}} resizeMode='contain'/>
                
                </View>
  
               <View>
               <Text style={{textAlign:'center',fontWeight:'bold',margin:6,fontSize:17,textTransform:'capitalize'}}>{this.props.title}</Text>
                <Text style={{textAlign:'center',color:'black',fontSize:14}}>{this.props.description}</Text>
              
  
               </View>
  
              
          </View>
  
              <View style={styles.buttonParent}>
                 
  
                  <TouchableOpacity onPress={()=> this.buttonClick()}
                  style={styles.clickButton}>
                  <Text accessibilityRole='button'
                  style={{color:'black',fontWeight:'bold',textTransform:'uppercase'}}>{this.props.buttonText}</Text>
                    
                  </TouchableOpacity>
  
              </View>

              <TouchableOpacity onPress={()=> this.setState({visibility:false})}
              style={{
               position:'absolute',
               top:10,right:10
             }}>
             <Image source={require('../assets/error.png')} 
             style={{width:20,height:20}} resizeMode='contain'/>
             </TouchableOpacity>
        </Overlay>
        )
  
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},
mainLayout:{alignItems:'center',justifyContent:'center',paddingBottom:70},
topImageBorder:{borderRadius:30,
    borderColor:colors.COLOR_PRIMARY,borderWidth:1,marginTop:-35,
    backgroundColor:'white',elevation:4,shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 0}
    ,marginBottom:15},
    buttonParent:{
        position : 'absolute',
        bottom:0,left:0,right:0,height:null,width:300,
       borderBottomLeftRadius:20,
       borderBottomRightRadius:20,
       overflow: 'hidden',
      },
      clickButton:{
        flex:1,
        backgroundColor:colors.COLOR_PRIMARY,
        alignItems:'center',
        justifyContent:'center',
         padding:20

      },
      button:{
        flex:1,
        backgroundColor:colors.COLOR_PRIMARY,
        alignItems:'center',
        justifyContent:'center',
        padding:20

      }


    
 

}
)
import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform,ActivityIndicator
} from 'react-native';
import { withNavigation } from 'react-navigation';
import RBSheet from "react-native-raw-bottom-sheet";
import RNPickerSelect from 'react-native-picker-select';
import { Chip } from 'react-native-paper';

import Icon from 'react-native-vector-icons/AntDesign';
const myIcon = <Icon name="caretdown" size={10} color={colors.COLOR_PRIMARY}  style={{marginTop:13}}/>;;
import I18n from '../i18n';

const dimension = Dimensions.get("window")

import Geocoder from 'react-native-geocoder';
import Geolocation from '@react-native-community/geolocation';
import { Slider,Overlay ,Header} from 'react-native-elements';

import {showMessage} from '../config/snackmsg';




export default class LocationRangeSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          location_value:5
          

      };


    }
 

   
  

 
    


    componentWillMount() {
      

        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });   
      }

 


    render() {

       
        return(
            <RBSheet
            ref={this.props.inputRef}
            height={200}
            duration={240}
            customStyles={{
              container: {
                alignItems: "center",
                backgroundColor:colors.BLACK,
                padding:10,
                borderTopLeftRadius:1,
                 borderTopRightRadius:1,
             
              }
            }}
            animationType='fade'
            minClosingHeight={10}
          >
          <Text 
          style={{color:colors.COLOR_PRIMARY,textAlign:'center',fontWeight:'900',fontSize:16,margin:5}}>
         {I18n.t('choose_range')}</Text>


           {/** top content */}
         
           {/** top content end */}

           {/** country state dropwdown */}
           <View style={{alignItems:'center',width:'100%',padding:2,marginTop:7}}>

                    <View style={{ width:'90%', justifyContent: 'center',marginBottom:5 }}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginBottom:-5}}>
                    <Text style={{color:'grey',fontSize:12}}>5</Text>
                    <Text style={{color:'grey',fontSize:12}}>100</Text>
                    </View>
                    <Slider
                    value={this.state.location_value}
                    thumbTintColor={colors.COLOR_PRIMARY}
                    maximumValue={100}
                    minimumValue={5}
                    onValueChange={location_value => this.setState({ location_value })}
                    />
                    <Text style={{marginBottom:15,alignSelf:'center',marginTop:0,color:colors.WHITE,fontSize:13}}>{I18n.t('distance')} : {parseInt(this.state.location_value)} km</Text>
                   
                </View>


    </View>



           {/** country state dropwdown  end*/}
           <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',width:'50%'}}>

           <TouchableOpacity onPress={()=> {this.props.cancel()}}>
           <View style={{backgroundColor:'red',
            justifyContent:'center',
            alignItems:'center',
            paddingLeft:7,
            paddingRight:7,
            paddingTop:4,
            paddingBottom:4,
            borderRadius:15}}>
           <Text 
           style={{color:colors.WHITE,textAlign:'center',fontWeight:'bold'}}>{I18n.t('cancel')}
          </Text>
           </View>
           </TouchableOpacity>
           

           <TouchableOpacity onPress={()=> {this.props.search(this.state.location_value)}}>
           <View style={{backgroundColor:colors.LIGHT_GREEN,
            justifyContent:'center',
            alignItems:'center',
            paddingLeft:7,
            paddingRight:7,
            paddingTop:4,
            paddingBottom:4,
            borderRadius:15}}>
           <Text 
           style={{color:colors.BLACK,textAlign:'center',fontWeight:'bold'}}>{I18n.t('search')}
          </Text>
           </View>
           </TouchableOpacity>

          </View>



            {/** bottom three buttons */}
            
          </RBSheet>
          )
     
  
  
    }
}





let styles = StyleSheet.create({
  
  overlayStyle: {
      borderRadius:20
     
},
dropDownParent:{flex:1,
    margin:3,
    borderRadius:20,borderWidth:0.7,
    borderColor:'white',
    padding:5
  }

}
)


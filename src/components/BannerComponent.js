import React from 'react';
import {  colors,urls } from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';


const d = Dimensions.get("window")



 const BannerComponent = (props) => {
  
  return (
    <View  style={{justifyContent:'center',alignItems:'center',marginBottom:-50}}
    key={props.index}>
    <Image resizeMode='contain'
     style={{ width: '80%', height: '80%' }} source={props.data.image}/>
    <Text style={{textAlign:'center',fontWeight:'bold',marginTop:10}}>{props.data.text}</Text>
  </View>
  )
};

export default BannerComponent

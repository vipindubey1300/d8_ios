import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
 
  RefreshControl,Fragment,Platform
} from 'react-native';


export const VinChandText = (props) => {
    return (
         <Text 
         style={[{fontFamily:'vincHand'},props.style]} >{props.children}</Text>
    )
 }
import React from 'react';
import {  colors,urls } from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';


const d = Dimensions.get("window")



 const ButtonComponent = (props) => {

  function _onPress()  {
    props.handler(1)
  }
  
  return (
    <TouchableOpacity
    onPress={()=> _onPress() }>
                             <View style={[styles.container,props.style]}>
                                <Text style={{fontWeight:'bold',color:colors.COLOR_PRIMARY}}>{props.label.toUpperCase()}</Text>

                             </View>
                             </TouchableOpacity>
  )
};

export default ButtonComponent

const styles = StyleSheet.create({

  container:{
   
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.BLACK,
    padding:10,
    borderRadius:30,
    marginTop:20,
    marginBottom:10
    
  }
})

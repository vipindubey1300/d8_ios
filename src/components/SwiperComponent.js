import React from 'react';
import {  colors,urls ,dimensions} from '../Constants';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform, TouchableHighlight
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { Item } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


const d = Dimensions.get("window")





 class SwiperComponent extends React.Component {
    state = {
    
    };



    _next(card,index){
     
      

        console.log(card._id)

        let obj={
         'user_id':card._id
         }
         this.props.navigation.navigate("UserProfile",{result:obj})


   }




  render() {
    const { card, index } = this.props;
    //console.log(urls.BASE_URL + card.user_image)

    return (
      <View style={styles.card}>
      <TouchableOpacity onPress = {()=> this._next(card,index)}>
       <Image
       resizeMode='cover'
        source={{uri : urls.BASE_URL + card.user_image}}
        style={
         {
           height:'100%',
           width:'100%'
         }
       }>

       </Image>
       </TouchableOpacity>

      


       {
         card.visibility == false?
         <TouchableHighlight style={{position:'absolute',top:0,
         left:0,bottom:0,right:0, padding:10,backgroundColor:'rgba(0,0,0,0.9)',justifyContent:'center',alignItems:'center'
    }}
         onPress={()=> this._next(card,index)}>
         <View style={{position:'absolute',top:0,
         left:0,bottom:0,right:0, padding:10,backgroundColor:'rgba(0,0,0,0.9)',justifyContent:'center',alignItems:'center'
    }}>   
      <Text style={{color:colors.COLOR_PRIMARY,marginBottom:30,fontWeight:'700',fontSize:17}}>Discreet Mode</Text>
       <Image source={require('../assets/logo.png')} style={{height:150,width:150}} resizeMode='contain'/>

       
       </View>
       </TouchableHighlight>
       : null

       }


       <View style={{position:'absolute',bottom:10,
       left:10,padding:10,backgroundColor:'rgba(0,0,0,0.5)',borderRadius:11,elevation:10
       }}>  
       <View style={{flexDirection:'row'}}>
        <Text style={{ textTransform: 'uppercase',color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:15}}>{card.display_name} </Text>
        <Text style={{ textTransform: 'uppercase',color:colors.COLOR_PRIMARY,fontWeight:'600',fontSize:14}}>, {card.age}</Text>

       </View>
       <Text style={{ color:colors.COLOR_PRIMARY,fontSize:12,textTransform:'capitalize'}}>{card.profession}</Text>
       {
        (card.country !== null)
        ?
        <Text style={{ color:colors.COLOR_PRIMARY,fontSize:12,fontWeight:'700'}}>
        {card.city ? card.city.name  : ''} { card.country.name}</Text>
        :
       null
       }
       


       </View>


       {
        card.verified ?
      
        <View style={{position:'absolute',bottom:10,
       right:10,padding:10,backgroundColor:'rgba(0,0,0,0.5)',borderRadius:11,elevation:10,alignItems:'center'
       }}>  
      
       <Image source={require('../assets/verify.png')} style={{height:30,width:30}} resizeMode='contain'/>
       <Text style={{ color:colors.COLOR_PRIMARY,fontSize:12,fontWeight:'300'}}>Verified</Text>
       
       </View>
     
       : null

       }
       



      </View>
    )

   


  }
}

export default withNavigation(SwiperComponent);




let styles = StyleSheet.create({
  
  card: {
      height:dimensions.SCREEN_HEIGHT * 0.6,
      borderRadius: 10,
      overflow:'hidden',
      borderWidth: 2,
      borderColor:'white',
      justifyContent: 'center',
      backgroundColor: colors.COLOR_PRIMARY,
    },
    
 

}
)
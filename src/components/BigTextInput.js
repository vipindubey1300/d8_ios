import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform} from 'react-native';

import {  colors,urls } from '../Constants';

export default class BigTextInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
    errorState:false,
    errorText:''
  };

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
    const showPlaceHolderTop = isFocus || text !== '';


    return (
      <View style={[styles.container, this.props.style]}>

      
        <TextInput
          style={[styles.inputText]}
          value={this.state.text}
          autoCapitalize={this.props.autoCapitalize}
          //autoCorrect={false}
          multiline={true}
          numberOfLines={1}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          ref={this.props.inputRef}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          placeholder={showPlaceHolderTop ? '' : this.props.placeholder}
          textContentType={this.props.textContentType}
          onSubmitEditing={this.props.onSubmitEditing}
          placeholderTextColor={colors.COLOR_PRIMARY}
          onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text,
            errorState:false })}
          editable={this.props.editable}
         
            numberOfLines={1}
            blurOnSubmit={false}
        />
        {
          this.state.errorState
          ?
          <Text style={{color:'red',textAlign:'center'}}>{this.state.errorText}</Text>
          : null
        }
      </View>
    );

   


  }
}

BigTextInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: '',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
  inputImage : require('../assets/user.png')
};

const styles = StyleSheet.create({
  container: {
    borderColor:colors.LIGHT_MAROON,
   borderWidth:2,
   borderRadius:15,
   backgroundColor:colors.DARK_MAROON,
   margin:7,
   

  },

  inputText: {
    height:75,
    textAlignVertical:'top',
    padding:10,
    fontSize: 14,
    color:colors.WHITE,
   
  },
});

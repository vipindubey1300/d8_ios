import Snackbar from 'react-native-snackbar';


export const showMessage = async (message,error = true) => {
  Snackbar.show({
    title: message,
    duration: Snackbar.LENGTH_SHORT,
    backgroundColor:'black',
    color: error ?  'red' : 'yellow',
   
  });
}





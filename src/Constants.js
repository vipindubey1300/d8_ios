import {Dimensions} from 'react-native';

export const colors = {
  STATUS_BAR_COLOR: "#d5b147",
  COLOR_PRIMARY:'#d5b147',
  COLOR_SECONDARY:'#000000',
  WHITE:'#FFFFFF',
  BLACK:'#000000',
  LIGHT_MAROON:'#301606',
  DARK_MAROON:'#170B03',
  LIGHT_YELLOW:'#FAFAD2',
  SILVER:' #C0C0C0',
  LIGHT_DARK:'#1e1e1e',
  LIGHT_GREEN:'#7CFC00',
  CRIMSON:'#DC143C',
  GREEN:'#008000',
  MAROON:'#2f0909',
  PALE_GREEN:'#98FB98'



};



export const urls = {
//BASE_URL: 'http://localhost:9000/',
 BASE_URL: 'http://ec2-52-54-1-108.compute-1.amazonaws.com:3000/',
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert,TextInput,FlatList,Keyboard,Linking, ImageBackground,Clipboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ProgressBar from '../components/ProgressBar';

import * as Animatable from 'react-native-animatable';
import firebaseSDK from '../config/firebaseSDK';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';
import LocationComponent from '../components/LocationComponent';

import {showMessage} from '../config/snackmsg';
import { sendNotificationToUser } from '../config/NotificationUtil';
import RBSheet from "react-native-raw-bottom-sheet";
import ImagePicker from 'react-native-image-picker';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { ScrollView } from 'react-native-gesture-handler';
import { isIphoneX } from 'react-native-iphone-x-helper'


class Chats extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            messages:[],
            message:'',
            bottom:0,
            to_id:'',
            to_name:'',
            to_image:'',
            to_device_token:'',
            pair_id:0,
            imageSource: null ,
            photo:null,
            isOpponentTyping:false,
            isOpponentOnline:false,
            isMyselfTyping:false,
            block_status:false,
            block_status_no : -1
            
          
        }
        this._isMounted = false;

    }
    _menu = null;

    changeSelfTypingStatus(isTyping){
        var key_from = `${this.props.user.user_id}_typing`
             firebase.database().ref('recents/' + this.state.pair_id).update({
               [key_from] : isTyping
           });
    }

    seeLocation = (addr,lat,lng) => {
     let f = Platform.select({
          ios: () => {
              Linking.openURL('http://maps.apple.com/maps?daddr='+lat+','+lng);
          },
          android: () => {
              Linking.openURL('http://maps.apple.com/maps?daddr='+lat+','+lng).catch(err => console.error('An error occurred', err));;
          }
      });

      f();
  }



    _checkBlockStatus =(to_id) =>{
      
      var formData = new FormData();
      formData.append('user_id',this.props.user.user_id);
      formData.append('to_id',to_id);
    
    
     
  
             let url = urls.BASE_URL +'api/check_block_status'
              // this.setState({loading_status:true})
             fetch(url, {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'multipart/form-data',
             },
             body: formData
            }).then((response) => response.json())
                 .then( (responseJson) => {
                  this.setState({loading_status:false})
                 
                      if (responseJson.status){
  
                          this.setState({
                            block_status:true,
                            block_message:responseJson.message,
                            block_status_no:responseJson.block_status
                          })
  
                     }
                     else{
                      this.setState({
                        block_status:false,
                        block_message:'',
                        block_status_no:-1
                      })
                     }
                 }).catch((error) => {
                           this.setState({loading_status:false})
                           showMessage('Try Again.')

                 });
  }
  _unBlockUser =() =>{
   
    var formData = new FormData();
    formData.append('user_id',this.props.user.user_id);
    formData.append('to_id',this.state.to_id);
    

           let url = urls.BASE_URL +'api/unblock_user'
            this.setState({loading_status:true})
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
          }).then((response) => response.json())
               .then( (responseJson) => {
                this.setState({loading_status:false})
                    if (responseJson.status){

                        this.props.navigation.navigate('ChatLists')

                   }else{
                        showMessage(responseJson.message)
        
                     }
               }).catch((error) => {
                         this.setState({loading_status:false})
                         showMessage('Try Again.')

               });
  }

    makeRecent(id,messageType,message){

     // console.log("PAIRRRR",id)
        var d = new Date()
        var hours = d.getHours()
        var minutes = d.getMinutes()
        var time = hours + ':' + minutes
    
        var key_to = `${this.state.to_id}_typing`
        var key_from = `${this.props.user.user_id}_typing`
        var key_to_read = `${this.state.to_id}_read`
        var key_from_read = `${this.props.user.user_id}_read`
    
        //[] is variable name in ES6  ex.. [key_to]
        firebase.database().ref('recents/' + id).set({
          last_message :message, 
          timestamp:firebase.database.ServerValue.TIMESTAMP,
          time:time,
          msg_type:messageType,
          receiver_id:this.state.to_id,
          receiver_name:this.state.to_name,
          receiver_image:this.state.to_image,
          sender_id:this.props.user.user_id,
          sender_name:this.props.user.name,
          sender_image:this.props.user.image,
          [key_to] : false, //front user
          [key_from]:false,
          [key_to_read] : false,//front user
          [key_from_read]:true
        }).then((data)=>{
            //success callback
           console.log("Recent chats added successfully....")
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
        })
    }
    initialize = (to_id,pairId) =>{
   
             /**  get device token of the to_user */
          firebase.database().ref('Users/' + to_id ).on('value', (snapshot)=>  {
              //console.log("TOKEN",snapshot.val().device_token)
             if(snapshot.val()){
              this.setState({
                to_device_token:snapshot.val() == null ? '' : snapshot.val().device_token
              })
             }
          });
    
           /** opponent uses typing status is handled here */
          firebase.database().ref('recents/' + pairId ).on('value', (snapshot)=>  {
            var key = `${to_id}_typing`
           
            if(snapshot.val()){
              this.setState({
                isOpponentTyping:snapshot.val() === null ? false : snapshot.val()[key]
              })
             }
            
          });
    
    
        
         /** opponent user offline online status is handled here */
        firebase.database().ref('Users/'+ to_id).on('value', (snapshot)=> {
            if(snapshot.val()){
              let status = snapshot.val().active === null ? 'offline' : snapshot.val().active
              this.setState({
                isOpponentOnline: status == 'offline' ? false : true
            })
             }
            
         });
    
    
              
    
        /**this works for every child added and also this fetches previouly
          * and for every node inserted .. this resturns all the data 
          */
        firebase.database().ref('messages/' + pairId).once('value',(snapshot)=> {
                  var temp = []
                  snapshot.forEach((childSnapshot) =>{
                   
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();
                     
                        temp = [...temp , childData]
                     })
    
                   //The shift() command will remove the first element of the array 
                   //and the unshift() command will add an element to the beginning of the array.
                   var arr = temp.reverse()
                   arr.splice(0,1)
                   if(arr.length == 0){
                     //means first time chat started so make recent chats
                     this.makeRecent(pairId,'text','')
                   }
                   this.setState({messages:arr})
        });
    
    
        firebase.database().ref('messages/'+ pairId).on('child_removed', (snapshot)=>{

        })
    
    
     /** retrieve the last record from `ref` or messages schema */

       firebase.database().ref('messages/'+ pairId).endAt().limitToLast(1).on('child_added', (snapshot)=> {
      
           
              let oldList = [...this.state.messages];
                // oldList.push(snapshot.val());
                // oldList.unshift(snapshot.val())
                // oldList.pop()
                //remove last message to avoid redunancy of last message in flatlist
              oldList.splice(0, 0, snapshot.val())
              this.setState({messages:oldList})
             
              
              snapshot.forEach((subSnapshot) =>{
                  /** for read unread message */
                   const { user } = this.props;
                   var id = user.user_id+'_read'
                   if(snapshot.val().receiver_id == user.user_id){

                  

                    if(this._isMounted){

                     // console.log("---------------------Mounted")

                      var key_from_read = `${this.props.user.user_id}_read`

                      var ref1 =  firebase.database().
                        ref('recents/' + pairId  )
                        ref1.update({[key_from_read]:true});

                        var ref =  firebase.database().
                        ref('messages/' + pairId + '/' + snapshot.key + '/' )
                        ref.update({read_status:this.props.user.user_id+'_read'});

                        firebase.database().ref('messages/' + pairId + '/' + snapshot.key + '/').on('value',  (snapshot) => {
                          if(snapshot.val() !== null){
                            if(snapshot.val().read_status == this.props.user.user_id +'_read' ){
                              //means that message already read so no update
                            }
                          }
                        })
                      }
    
                  }else{
                     let messages = this.state.messages.map(el => (
                        el.receiver_id == user.user_id ? {...el, read_status : snapshot.val().receiver_id+'_read'}: el
                     ))
                     this.setState({ messages });
    
                  }
                   })
      });
    
    

    
    
          /** change read and unread message in flatlist from here */
          firebase.database().ref('messages/'+ pairId).orderByChild('read_status').on('child_changed',  (snapshot)=> {
            if(snapshot.val().sender_id == this.props.user.user_id){
              let messages = this.state.messages.map(el => (
                 (el.node_id  ==  snapshot.val().node_id) || el.node_id === undefined 
                  ? 
                  {...el, read_status: snapshot == null ? false : snapshot.val().read_status }
                   : el
               ))
              this.setState({ messages },()=> console.log('done'));                
             }
                snapshot.forEach((childSnapshot) =>{
                });
           
            }, (error) => {
                console.log(error)
         });
    
    
         /** on deleting message handle here */
        firebase.database().ref('messages/'+ pairId).on('child_removed', (snapshot)=> {
          //remove the deleted message from the state 
           let oldList = this.state.messages
           let list = oldList.filter(chat => (
             chat.node_id != snapshot.val().node_id 
           ));
          let enhancedList = list.filter(chat => (
            chat.node_id != undefined
          ));
         this.setState({messages:enhancedList})
        });
    
    }

    componentWillMount(){
        let result = this.props.navigation.getParam('result')
        var to_id = result['to_id']
        var to_name = result['to_name']
        var to_image = result['to_image']

    
        var res = this.props.user.user_id.localeCompare(to_id)
        // -1 means user_id is less than to_id
        // 0 means user_id is equal to to_id
        // 1 means user_id is greater than to_id

        let pairId = ( res == -1 )
        ?  this.props.user.user_id + '_' + to_id
        : to_id + '_' + this.props.user.user_id




        // console.log("USer id --- " + this.props.user.user_id)
        // console.log("To id --- " + to_id)
        // console.log("Pair id --- " + pairId)


        this.setState({
            to_id:to_id,
            to_name:to_name,
            to_image:to_image,
            pair_id:pairId
      
          })

        this.initialize(to_id,pairId)
        this._checkBlockStatus(to_id)

        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });  

    }

     
    componentWillUnmount(){
    this._isMounted = false;
    firebase.database().ref('messages/' + this.state.pair_id).off('child_added');
    clearInterval(this.timer)
    // this.keyboardDidShowListener.remove();
    // this.keyboardDidHideListener.remove();
  
    }
  
    componentDidMount(){
        this._isMounted = true;
        this.timer = setInterval(()=> this._checkBlockStatus(this.state.to_id), 4000)
    }

    sendNotification(currentMessage){
        let title =  "New Message from " + this.props.user.name
        const notification = {
          "notification": {
              "title": title,
              "body": currentMessage,
              "sound": "default",
           
          },
          "data" : {
            "to_id" : this.props.user.user_id,
            "to_name":this.props.user.name,
            "to_image":this.props.user.image,
            'to_status':'offline'
    
    
          
          },
          "priority": "high",
          //"action" : "Chats"
    
         }
        sendNotificationToUser(this.state.to_device_token, notification);
    }

    addKey(pairId,key,data){
        var o = Object.assign({}, data);
        o.node_id = key;
    
              firebase.database().ref('messages/'+ pairId + '/' + key).set(o).then((snapshot)=>{
              const messages = this.state.messages.map(el => {
                return el.timestamp  ===  data.timestamp ?  {...el, node_id: key } : el 
              })
                 this.setState({ messages : messages},()=> console.log('')); 
                 
              }).catch((error)=>{
                  //error callback
                  console.log('error ' , error)
              })
      }
    
     writeChat(id,to_id,from_id){
    
          var d = new Date()
          var hours = d.getHours()
          var minutes = d.getMinutes()
          var time = hours + ':' + minutes
    
          let currentMessage = this.state.message
          this.setState({message:''})
         
         let timestamp = firebase.database.ServerValue.TIMESTAMP
          const dataMessage = {
              message : currentMessage, 
              sender_id:from_id,
              receiver_id:to_id,
              timestamp:Date.now(),
              type:'text',
              read_status:to_id + '_unread',
              picture:'',
              location:'',
              time:time,
           
          }
    
    
         var ref = firebase.database().ref('messages/' + id)
        let dbRef =  ref.push(dataMessage).then((data)=>{
        Keyboard.dismiss()
    
        this.makeRecent(id,'text',currentMessage)
        this.addKey(id,data.key,dataMessage)
    
        this.sendNotification(currentMessage)
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
            ToastAndroid.show(error.message,ToastAndroid.LONG)
        })
      }
    

      upload = async (filepath: string, filename: string, filemime: string) => {
        const metaData = { contentType: filemime };
        const res = await firebase
            .storage()
            .ref(`${this.state.pair_id}/${filename}`)
            .putFile(filepath, metaData)

            // put image file to GCS

            //console.log("RES",JSON.stringify(res))
              return res;
    };
    
    sendMessage(){
       if(this.state.message.trim().length > 0){

        var from_id = this.props.user.user_id
        var to_id = this.state.to_id
        var pairId = this.state.pair_id
    
        this.writeChat(pairId,to_id,from_id)
    
       }
      }

    sendImage = async(photo,id,to_id,from_id) => {
        const res = await this.upload(photo.uri, photo.name, `image/jpeg`); // function in step 1
        console.log("IMAGERES",res.downloadURL)
        var d = new Date()
        var hours = d.getHours()
        var minutes = d.getMinutes()
        var time = hours + ':' + minutes


        const dataMessage = {

          message : '',
          sender_id:from_id,
          receiver_id:to_id,
          timestamp:Date.now(),
          type:'image',
          read_status:to_id + '_unread',
          picture:res.downloadURL,
          location:'',
          time:time,
      }
        var ref = firebase.database().ref('messages/' + id)
          var ref = firebase.database().ref('messages/' + id)
          let dbRef =  ref.push(dataMessage).then((data)=>{
              //success callback
              this.makeRecent(id,'image','')
              this.addKey(id,data.key,dataMessage)
              this.sendNotification('Image Recieved !')
          }).catch((error)=>{
              //error callback
              console.log('error ' , error)
              ToastAndroid.show(error.message,ToastAndroid.LONG)
          })

    }


    sendLocation = async(location,id,to_id,from_id) => {
      //location: {"address": "Cosmos Industries", "latitude": 28.6291731, "longitude": 77.277301}
      
      var d = new Date()
      var hours = d.getHours()
      var minutes = d.getMinutes()
      var time = hours + ':' + minutes

      const dataMessage = {

        message : '',
        sender_id:from_id,
        receiver_id:to_id,
        timestamp:Date.now(),
        type:'location',
        read_status:to_id + '_unread',
        picture:'',
        location:location,
        time:time,
    }
      var ref = firebase.database().ref('messages/' + id)
        var ref = firebase.database().ref('messages/' + id)
        let dbRef =  ref.push(dataMessage).then((data)=>{
            //success callback
            this.makeRecent(id,'location','')
            this.addKey(id,data.key,dataMessage)
            this.sendNotification('Location Recieved !')
        }).catch((error)=>{
            //error callback
            console.log('error ' , error)
            ToastAndroid.show(error.message,ToastAndroid.LONG)
        })

  }

      _addPhoto() {
        const options = {  maxWidth: 500, maxHeight: 500,  storageOptions: { skipBackup: true  }
        };
    
        //launchCamera
        //showImagePicker
        //launchImageLibrary
        ImagePicker.showImagePicker(options, (response) => {
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
           //const image_data = { uri: `data:image/jpeg;base64,${response.data}` };
            let source = { uri: response.uri};
            var photo = {
              uri: response.uri,
              type:"image/jpeg",
              name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
            };
    
            this.setState({
              imageSource: source ,
              photo:photo,
    
            });

            var from_id = this.props.user.user_id
            var to_id = this.state.to_id
            var pairId = this.state.pair_id
            this.sendImage(photo,pairId,to_id,from_id)
          }
        });
     }

     checkType(item,index,send_status){
      if(item.type == 'text'){
        return (
          <TouchableOpacity style={{elevation:3}} onPress={() => { console.log("onPress") }}
          onLongPress={() => { Clipboard.setString(item.message)
            showMessage('Message Copied To Clipboard',false)}}>

          <Text style={{color: send_status ? 'black' : 'black'}}>{item.message.trim()}</Text>
          </TouchableOpacity>

        )
      }
      else if(item.type == 'image'){
          return (
            <TouchableOpacity  onPress={() => {
              let obj = {
                'image_url':item.picture
              }
              this.props.navigation.navigate('ImageView',{result:obj})
            }}
           >
            <Image style={{height:100,width:100}}
             source={{uri:item.picture}}/>
             </TouchableOpacity>

          )
      }
      else if(item.type == 'location'){
        return (
          <TouchableOpacity onPress={()=> this.seeLocation(item.location.address,item.location.latitude,item.location.longitude)}>
          <View style={{width:dimensions.SCREEN_WIDTH*0.3,padding:4,alignSelf:'flex-end',backgroundColor:colors.LIGHT_MAROON}}>
          <Image style={{width:40, height: 40,margin:10,alignSelf:'center'}}  source={require('../assets/location-view.png')} />
          <Text style={{marginRight:10,fontSize:12,color:colors.COLOR_PRIMARY,alignSelf:'center',textAlign:'center'}}>{item.location.address}</Text>

          </View>
            </TouchableOpacity>

        )
    }
    }

    renderItem = ({item, index}) => {
        //console.log(item)
        const send_status = this.props.user.user_id == item.receiver_id ? true : false
        const width = dimensions.SCREEN_WIDTH
        return(
          <View  style={{width : item.message.length > 20  ? width * 0.7 : null,
            alignSelf : send_status ? 'flex-start' : 'flex-end',margin:5}}>

            <TouchableOpacity 
            style={{backgroundColor:send_status ? '#d5b147' : '#C0C0C0',padding:7,borderRadius:10}}>
            {
              this.checkType(item,index,send_status)
            } 
            </TouchableOpacity>
            <Text style={{marginLeft:5,fontSize:10,fontWeight:'500',
              alignSelf : send_status ? 'flex-start' : 'flex-end',marginRight:5,color:'white'}}> {item.time}</Text>
         
          </View>
        )
       
  
      }

      goToBlock =() =>{
        let obj = {
          'user_id':this.state.to_id,
          'name':this.state.to_name,
          'image' : this.state.to_image
        }
        this.props.navigation.navigate('BlockUser',{result:obj})
      }
      setMenuRef = ref => {
        this._menu = ref;
      };
    
      hideMenu = () => {
        this._menu.hide();
      };
    
      showMenu = () => {
        this._menu.show();
      };
      _getLocation = (location) =>{
          console.log('location:...',location)
          var from_id = this.props.user.user_id
          var to_id = this.state.to_id
          var pairId = this.state.pair_id
          this.sendLocation(location,pairId,to_id,from_id)
          this.locationSheet.close()
      }
   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
                statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


               rightComponent ={
               
                <TouchableOpacity onPress={()=>  this.goToBlock()}>
                <Image source={require('../assets/block.png')}
                                style={{width:17,height:17,marginRight:0}} resizeMode='contain'/>


                </TouchableOpacity>

              }




              //centerComponent={{ text: this.state.to_name , style: { color: colors.WHITE } }}
              centerComponent={
                <View>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                {
                  this.state.isOpponentOnline
                  ? <View style={{height:8,width:8,
                  borderRadius:6,backgroundColor:'#7CFC00',
                  marginRight:7,marginTop:3}}></View>
                  : <View style={{height:8,width:8,
                  borderRadius:6,backgroundColor:'#FF0000'
                  ,marginRight:7,marginTop:3}}></View>
                }
                <Image source={{uri:urls.BASE_URL +this.state.to_image}}
                style={{width:30,height:30,borderRadius:15,overflow:'hidden',borderColor:colors.COLOR_PRIMARY,
                borderWidth:3
               }}
                resizeMode='cover'/>
                {
                  this.state.isOpponentTyping ?
                  <Text style={{color:'#7CFC00',alignSelf:'center',fontSize:13,fontWeight:'500',marginLeft:10}}>{I18n.t('typing')}...</Text>
                  :                 <Text style={{color:'white',marginLeft:10}}>{this.state.to_name}</Text>


                }
                </View>
                
                
                </View>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
                 }}
                />
                <LocationComponent
                inputRef={ref => this.locationSheet = ref}
                // ref={ref => this.locationBottomSheet = ref}
                getLocation={this._getLocation}
              />
              <KeyboardAwareScrollView
              extraScrollHeight={isIphoneX() ? dimensions.SCREEN_HEIGHT * 0.02 : dimensions.SCREEN_HEIGHT * 0.001} //solve problem keyboard hide keybard in iphone XR
               keyboardShouldPersistTaps="always"
              contentContainerStyle={{flexGrow:1}}>


              <ImageBackground source={require('../assets/d8login-bg.png')}
              style={{width:'100%',height:'100%',marginRight:0,backgroundColor:'black'}} resizeMode='stretch'>
                
               <View style={{width:'100%',padding:4,
                    height:dimensions.SCREEN_HEIGHT * 0.850,paddingBottom:30,backgroundColor:'black'}}>

                   <FlatList   
                            data={this.state.messages}
                            inverted 
                            scrollEnabled={true}
                            showsVerticalScrollIndicator={false}
                            renderItem={(item,index) => this.renderItem(item,index)}
                            keyExtractor={item => item._id}
                            // Performance settings
                            removeClippedSubviews={true} // Unmount components when outside of window
                            initialNumToRender={2} // Reduce initial render amount
                            maxToRenderPerBatch={1} // Reduce number in each render batch
                            maxToRenderPerBatch={100} // Increase time between renders
                            windowSize={7} // Reduce the window size
                           
                     />


                {
                  this.state.block_status
                  ?
                  <View style={{backgroundColor:'red',height:null,
                  width:dimensions.SCREEN_WIDTH * 0.7,alignSelf:'center',
                  padding:20,justifyContent:'center',alignItems:'center'}}>
                  <Text 
                  style={{color:colors.COLOR_PRIMARY,textAlign:'center',fontWeight:'bold'}}>{this.state.block_message}</Text>
                  {
                    (this.state.block_status_no == 0 || this.state.block_status_no == 2)
                    ?
                    <Text 
                    onPress={()=>this._unBlockUser()}
                    style={{color:'green',
                    textAlign:'center',fontWeight:'bold',backgroundColor:'black',padding:5,marginTop:5}}>Unblock </Text>
                    :null

                  }
                  </View>
                  :
                  null
                  
                }

                 

              
                </View>
                </ImageBackground>

                <View  style={{width:'100%',padding:3,
                     height:50,position:'absolute',bottom:0,elevation:4,left:0,right:0,
                     backgroundColor:'white',flexDirection:'row',alignItems:'center',marginBottom:0}}>
                     <TextInput
                        style={{ height:'100%',
                        padding:Platform.OS == 'android' ? 6 : 17,
                        fontSize: 13,
                        color:colors.WHITE,
                        borderRadius:30,
                        backgroundColor:'grey',flex:7}}
                        editable={this.state.block_status ? false : true}
                        value={this.state.message}
                         multiline={true}
                         placeholder={'Enter message '}
                        placeholderTextColor={'grey'}
                       // onSubmitEditing={()=> this.sendMessage()}
                        blurOnSubmit={false}
                        onEndEditing={()=> this.setState({isMyselfTyping:false},()=>  {
                          this.changeSelfTypingStatus(false)
                        })}
                        onSubmitEditing={()=> this.setState({isMyselfTyping:false},()=> {
                          this.changeSelfTypingStatus(false)
                          this.sendMessage()
                        })}
                        onChangeText={ (message) => {
                          //var a = message.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
                          this.setState({message:message , isMyselfTyping:true},()=> {
                            this.changeSelfTypingStatus(true)
                          })
                        } }
                      />

                      <View style={{flex:1}}>
                      <TouchableOpacity 
                      disabled={this.state.block_status ? true : false}
                      onPress={()=> {this._addPhoto()}}>
                      <Image source={require('../assets/camera-gold.png')} style={{alignSelf:'center',height:30,width:30}} />
                      </TouchableOpacity>
                      </View>
                      <View style={{flex:1}}>
                      <TouchableOpacity 
                      disabled={this.state.block_status ? true : false}
                      onPress={()=> {this.locationSheet.open()}}>
                      <Image source={require('../assets/location-gold.png')} style={{alignSelf:'center',height:30,width:30}} />
                      </TouchableOpacity>
                      </View>
                      <View style={{flex:1}}>
                      <TouchableOpacity 
                      disabled={this.state.block_status ? true : false}
                      onPress={()=> {this.setState({isMyselfTyping:false},()=> {
                        this.sendMessage()
                        this.changeSelfTypingStatus(false)
                       
                      })}}>
                      <Image source={require('../assets/send-gold.png')} style={{alignSelf:'center',height:30,width:30}} />
                      </TouchableOpacity>
                      </View>

                   
             </View>
             </KeyboardAwareScrollView>
                               
           { this.state.loading_status && <ProgressBar/> }
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Chats);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
   
  },
  container:{
   
   
    padding:10,
    height :dimensions.SCREEN_HEIGHT * 0.95,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
  },
 
}
)



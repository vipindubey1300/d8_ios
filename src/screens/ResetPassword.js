import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import {showMessage} from '../config/snackmsg';
import {removeUser } from '../actions/actions';
import { connect } from 'react-redux';
import I18n from '../i18n';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import {  colors,urls,dimensions } from '../Constants';


const d = Dimensions.get("window")

 class ResetPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false
        }

    }



componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}

_isValid = () =>{

  const oldPassword = this.oldPasswordInput.getInputValue();
  const newPassword = this.newPasswordInput.getInputValue();
  const confirmPassword = this.confirmPasswordInput.getInputValue();
  
  if(oldPassword.trim().length == 0){
    showMessage('Enter Old Password')
    return false

  }
  if(newPassword.trim().length == 0){
    showMessage('Enter New Password')
    return false

  }
  else if (newPassword.trim().length < 8 || newPassword.trim().length > 16) {
    showMessage('New Password should be 8-16 characters long')
    return false;
  }
  else if(confirmPassword.trim().length == 0){
    showMessage('Enter Confirm Password')
    return false

  }
  else if (confirmPassword.trim().length < 8 || confirmPassword.trim().length > 16) {
    showMessage('Confirm Password should be 8-16 characters long')
    return false;
  }
  else if(confirmPassword !== newPassword ){
    showMessage('New Password and Confirm Password Should match')
    return false

  }

  else{
    return true;
  }

}


_resetPassword =() =>{

if(this._isValid()){
  const oldPassword = this.oldPasswordInput.getInputValue();
  const newPassword = this.newPasswordInput.getInputValue();
  const confirmPassword = this.confirmPasswordInput.getInputValue();

  this.setState({loading_status:true})
    var formData = new FormData();
  
  
     formData.append('user_id',this.props.user.user_id);
     formData.append('old_password',oldPassword);
     formData.append('new_password',newPassword);
     formData.append('confirm_password',confirmPassword);
     console.log(formData)
           let url = urls.BASE_URL +'api/reset_password'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
          }).then((response) => response.json())
               .then( async(responseJson) => {
                   this.setState({loading_status:false})

                   console.log("FFF",JSON.stringify(responseJson))


                if (responseJson.status){
                  try {
                    this.props.remove({});
                    await AsyncStorage.removeItem("user_id");
                    await AsyncStorage.removeItem("name");
                    await AsyncStorage.removeItem("image");
                    await AsyncStorage.removeItem("email");
                
                    this.props.navigation.navigate("Welcome")
                    
                    return true;
                  }
                  catch(exception) {
                    
                    return false;
                  }


                    
                   }else{
                         showMessage(responseJson.message)
                  }
               }).catch((error) => {
                         this.setState({loading_status:false})
                         showMessage('Try Again.')
                    
               });
}

}
   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.pop()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
             // enableAutomaticScroll={(Platform.OS === 'ios')}
           
             >



              <View style={styles.container}>
            

           

                 <Image source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>

                 <Text style={styles.headingText}>{I18n.t('reset_password')} </Text>


                 <Text style={styles.titleText}>{I18n.t('please_update_your_password')}</Text>


                          {/* text fields */}

                        <CustomTextInput
                          placeholder={I18n.t('old_password')}
                          keyboardType="default"
                           secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.newPassword.focus()}
                          inputRef={ref => this.oldPassword = ref}
                          ref={ref => this.oldPasswordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />


                      <CustomTextInput
                          placeholder={I18n.t('new_password')}
                          keyboardType="default"
                         secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.confirmPassword.focus()}
                          inputRef={ref => this.newPassword = ref}
                          ref={ref => this.newPasswordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />


                      <CustomTextInput
                          placeholder={I18n.t('confirm_password')}
                          keyboardType="default"
                         secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this._resetPassword()}
                          inputRef={ref => this.confirmPassword = ref}
                          ref={ref => this.confirmPasswordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="done"
                        />

                       
                        
                             <ButtonComponent 
                             style={{width : d.width * 0.8,}}
                              handler={this._resetPassword}
                             label ={I18n.t('reset')}/>



                             

                                


                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           
           { this.state.loading_status && <ProgressBar/> }

			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 1,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
    
    
  },
  logo_image:{
    height:"25%", 
    width:'30%',
    
  },
  headingText:{
   color:'white',
   fontWeight:'bold',
   fontSize:18,
   marginBottom:15
   },
   titleText :{
     color:'white',
     
     fontSize:14,
     marginBottom:35
     }
  
 

}
)



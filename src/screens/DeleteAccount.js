import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert,TextInput,Keyboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';

import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';
import ProgressBar from '../components/ProgressBar';

import {showMessage} from '../config/snackmsg';
import { VinChandText } from '../components/CustomText';

const d = Dimensions.get("window")


class DeleteAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          loading_status:false,
          password:''
        }

    }





componentWillMount(){
  
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
    //I18n.locale = 'en'
}





logout = async() =>{
  
    try {
     
  
      this.props.remove({});
  
  
  
      await AsyncStorage.removeItem("user_id");
      await AsyncStorage.removeItem("name");
      await AsyncStorage.removeItem("image");
      await AsyncStorage.removeItem("email");
      await AsyncStorage.removeItem("request");
      await AsyncStorage.removeItem("accept");
      //await AsyncStorage.removeItem("lang");
      
      this.props.navigation.navigate("Welcome")
   
      return true;
    }
    catch(exception) {
      
      return false;
    }
  }
  
  _isValid(){
      var password = this.state.password
      if(password.trim().length == 0){
          showMessage("Enter Password First!")
          return false
      }
      else return true
  }
 

  _deleteAccount = async () =>{
   
        Keyboard.dismiss()
             
    var formData = new FormData();
  
    formData.append('user_id',this.props.user.user_id);
   // formData.append('password',this.state.password);

  this.setState({loading_status:true})
             let url = urls.BASE_URL +'api/delete_account'
                  fetch(url, {
                  method: 'POST',
                  body:formData
  
                  }).then((response) => response.json())
                      .then((responseJson) => {
                        this.setState({loading_status:false})
                        showMessage(responseJson.message)

                        if(responseJson.status){
                            this.logout()
                        }
                       
                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
  
                        showMessage(error.message)
                      });
  
    
  
  
  }
 
  


   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
                 centerComponent={{ text: I18n.t('delete_account'), style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
          

       <KeyboardAwareScrollView contentContainerStyle={styles.container}>

              <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center',marginTop:-40}}>

              <Image source={{uri:urls.BASE_URL + this.props.user.image}} 
              overflow='hidden'
              style={styles.logo_image} resizeMode='cover'/>

                  <Text style={{color:colors.COLOR_PRIMARY,marginTop:15,width:'80%',marginBottom:4,textAlign:'center'}}>{I18n.t('delete_account_text')}</Text>

                  {/** 
                  <TextInput
                  style={{
                      width:'80%',
                      height:43,
                      borderColor:colors.COLOR_PRIMARY,
                      borderWidth:1.6,
                      borderRadius:0,
                      backgroundColor:'white',
                      textAlign:'center',
                      margin:10
                  }}
                  placeholder={I18n.t('password')}
                  value={this.state.password}
                    numberOfLines={1} 
                    secureTextEntry={true}  
                    onSubmitEditing={this.props.onSubmitEditing}
                    placeholderTextColor={'grey'}
                    onChangeText={ (password) => this.setState({ password })}
                  >
                  </TextInput>

                  */}


                  <View style={{width:'50%',flexDirection:'row',alignItems:'center',justifyContent:'space-around',marginTop:25}}>
                  <TouchableOpacity onPress={()=> this.props.navigation.pop()}
                  style={{borderColor:'white',borderWidth:2,borderRadius:10,paddingBottom:7,
                     paddingTop:7,paddingLeft:19,paddingRight:19,justifyContent:'center',alignItems:'center',backgroundColor:'white'}}>
                     <Text style={{color:colors.BLACK}}>{I18n.t('no')}</Text>
                  </TouchableOpacity>
                  
                  <TouchableOpacity onPress={()=> this._deleteAccount()}
                  style={{borderColor:colors.COLOR_PRIMARY,borderWidth:2,borderRadius:10,paddingBottom:7,
                  paddingTop:7,paddingLeft:19,paddingRight:19,justifyContent:'center',alignItems:'center',backgroundColor:colors.COLOR_PRIMARY}}>
                  <Text style={{color:'black'}}>{I18n.t('yes')}</Text>
               </TouchableOpacity>
               
                  
                  </View>

             
                </View>

                              

                      </KeyboardAwareScrollView>         
         

           
                { this.state.loading_status && <ProgressBar/> }

			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(DeleteAccount);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
    flexGrow:1,
    
    padding:10,
    alignItems:'center',
    backgroundColor:colors.BLACK,
    borderColor:colors.COLOR_PRIMARY,
   
    justifyContent:"center"
   
  },
  logo_image:{
    height:120, 
    width:120,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:60,
   
    marginBottom:10
    
  },

 
}
)



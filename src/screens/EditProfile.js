import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert,TextInput,TouchableWithoutFeedback} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser ,changeUser} from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import ImagePicker from 'react-native-image-picker';

import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';

import {showMessage} from '../config/snackmsg';

const d = Dimensions.get("window")

const age = [{label:'18',value:18,color:'grey'},
            {label:'19',value:19,color:'grey'},
            {label:'20',value:20,color:'grey'},
            {label:'21',value:21,color:'grey'},
            {label:'22',value:22,color:'grey'},
            {label:'23',value:23,color:'grey'},
            {label:'24',value:24,color:'grey'},
            {label:'25',value:25,color:'grey'},
            {label:'26',value:26,color:'grey'},
            {label:'27',value:27,color:'grey'},
            {label:'28',value:28,color:'grey'},
            {label:'29',value:29,color:'grey'},
            {label:'30',value:30,color:'grey'},
            {label:'31',value:31,color:'grey'},
            {label:'32',value:32,color:'grey'},
            {label:'33',value:33,color:'grey'},
            {label:'34',value:34,color:'grey'},
            {label:'35',value:35,color:'grey'},
            {label:'36',value:36,color:'grey'},
            {label:'37',value:37,color:'grey'},
            {label:'38',value:38,color:'grey'},
            {label:'39',value:39,color:'grey'},
            {label:'40',value:40,color:'grey'},

          ]


class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          visibilityStatus:true,
          display_name : '',
          about :'',
          profession: '',
          email:'',
          age:'',
          gender:'',
          interested_gender:'',
          gallery:'',
          user_details:'',
          country :'',
         country_id :'',
         state : '',
         state_id : '',
         city : '',
         city_id : '',
         ethnicity :'',
         ethnicity_id : '',
         body_type : '',
         body_type_id : '',
         height:0,
         weight:0,

          countries:[],
          states:[],
          cities:[],
          body_types:[],
          ethnicities:[],

          userImageSource:null,
          userImagePhoto:null,
          imageTwoSource:null,
          imageTwoPhoto:null,
          imageThreeSource:null,
          imageThreePhoto:null,
          imageFourSource:null,
          imageFourPhoto:null,
          imageFiveSource:null,
          imageFivePhoto:null,

          videoSource:null,
          videoObj : null,

          documentSource:null,
          documentPhoto:null,

          genderData:[
            { label: 'Male', value: 'male' ,color:'grey' },
            { label: 'Female', value: 'female'  ,color:'grey'},
            
        ]

        }

    }

    _fetchCountries = async () =>{
      this.setState({loading_status:true,states:[],cities:[]})
  
                 let url = urls.BASE_URL +'api/countries'
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              var temp_arr=[]
                              
                              var country = responseJson.result
                              country.map(item=>{
                                Object.assign(item,{value:item._id,label:item.name,color:'black'})
                              })
                              this.setState({countries:country})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchStates = async (countryId) =>{
      this.setState({loading_status:true,cities:[]})
  
                 let url = urls.BASE_URL +'api/states?country_id=' + countryId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var state = responseJson.result

                                state.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));


                                state.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({states:state})
                              }
                              else{
                                showMessage('No State Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchCities = async (stateId) =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/cities?state_id=' + stateId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var city = responseJson.result

                                city.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                                city.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({cities:city})
                              }
                              else{
                                showMessage('No City Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }



    _isValid(){
      const {display_name,about,profession,age,country_id,gender,interested_gender,
      height,weight,state_id,city_id,ethnicity_id,body_type_id} = this.state

      var heightIsNum = /^\d+$/.test(height);
      var weightIsNum = /^\d+$/.test(weight);

    
      if(display_name.trim().length == 0){
        showMessage('Enter Display Name')
          //this.userNameInput.setState({errorState:true,errorText:'Enter User Name'});
         return false
      }
      else if(profession.trim().length == 0){
        showMessage('Enter Profession')
        return false
    
      }
      else if(height.trim().length == 0){
        showMessage('Enter Height')
        return false
    
      }
      else if(!heightIsNum){
        showMessage('Enter Valid Height')
        return false
    
      }
      else if(height < 100 ||height == 0){
        showMessage('Enter height greater than 100')
         return false;
       }
      else if(height > 220 ){
        showMessage('Enter height less than 220')
          return false;
        }
      else if(weight.trim().length == 0){
        showMessage('Enter Weight')
        return false
    
      }
      else if(!weightIsNum){
        showMessage('Enter Valid Weight')
        return false
    
      }
      else if(weight < 40 ||weight == 0){
        showMessage('Enter weight greater than 40')
         return false;
       }
      else if(weight > 150 ){
        showMessage('Enter weight less than 150')
          return false;
      }
      else if(age == null){
        showMessage('Enter Age')
        return false
    
      }
      else if(ethnicity_id == null){
        showMessage('Enter Ethnicity')
        return false
    
      }
      else if(body_type_id == null){
        showMessage('Enter Body Type')
        return false
    
      }
      else if(country_id == null){
        showMessage('Enter Country')
        return false
    
      }
      else if(state_id == null){
        showMessage('Enter State')
        return false
    
      }
      else if(city_id == null){
        showMessage('Enter City')
        return false
    
      }
      else if(gender == null){
        showMessage('Enter Gender')
        return false
    
      }
      else if(interested_gender == null){
        showMessage('Enter Interested Gender')
        return false
    
      }
      
      else{
        return true;
      }
    
    
    }

   
    changeReduxValue =  (id,name,email,image) => {
  

    
      this.props.change({ user_id : id, 
        name : name, 
        email:email , 
        image:image});
     }


    _editProfile = () =>{
      if(this._isValid()){
    
    
        this.setState({loading_status:true})
        var formData = new FormData();
      
       formData.append('display_name',this.state.display_name);
       formData.append('about',this.state.about.trim());
       formData.append('gender', this.state.gender);
       formData.append('interested_gender', this.state.interested_gender);
       formData.append('height', this.state.height);
       formData.append('weight', this.state.weight);
       formData.append('country_id', this.state.country_id);
       formData.append('state_id', this.state.state_id);
       formData.append('city_id', this.state.city_id);
       formData.append('ethnicity_id', this.state.ethnicity_id);
       formData.append('body_type_id', this.state.body_type_id);
       formData.append('user_id', this.props.user.user_id);
       formData.append('profession',this.state.profession.trim());
       formData.append('age', this.state.age);

       //files
       formData.append('user_image',this.state.userImagePhoto);
       formData.append('image_two', this.state.imageTwoPhoto);
       formData.append('image_three', this.state.imageThreePhoto);
       formData.append('image_four', this.state.imageFourPhoto);
       formData.append('image_five', this.state.imageFivePhoto);
       formData.append('document', this.state.documentPhoto);
       formData.append('video', this.state.videoObj);
      
    
    
         console.log("FFF",JSON.stringify(formData))
    
    
               let url = urls.BASE_URL +'api/edit_profile'
              //  console.log("FFF",JSON.stringify(url))
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))
    
    
                    if (responseJson.status){
                      var  user_id = responseJson.user._id.toString()
                      var name = responseJson.user.display_name
                      var email = responseJson.user.email
                      var image = responseJson.user.user_image
                  

                      AsyncStorage.multiSet([
                        ["user_id", user_id.toString()],
                        ["email", email.toString()],
                        ["name", name.toString()],
                        ["image", image.toString()],
                        ]);
                      await this.changeReduxValue(user_id,name,email,image)
                      //this.props.navigation.navigate('MyProfile');
                      this.props.navigation.goBack()
                      this.props.navigation.state.params.onSelect({})
                  
                              
                     }else{
  
                        showMessage(responseJson.message)
                        
                    }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                             showMessage(error.message)
    
                   });
      }
    
     }


    handler =()=>{
        this._editProfile() 
    }


    _myProfile =() =>{

           
            var formData = new FormData();
            formData.append('user_id',this.props.user.user_id);
          
          
        
                   let url = urls.BASE_URL +'api/my_profile'
                  //  console.log("FFF",JSON.stringify(url))
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data',
                   },
                   body: formData
                  }).then((response) => response.json())
                       .then(async (responseJson) => {
                          
                        if (responseJson.status){
        
                         var visibility = responseJson.user.visibility
                         this.setState({visibilityStatus:visibility})
        
                           }else{
                             //showMessage(responseJson.message)
                
                             }
                       }).catch((error) => {
                                 this.setState({loading_status:false})
                                 showMessage('Try Again.')
                                
        
                       });
     }

   

  selectMainImage() {
      const options = { maxWidth: 500,maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };

      ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
          console.log('User cancelled photo picker')
        }else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }else {
        
          let source = { uri: response.uri};
          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };
         this.setState({ userImageSource: source , userImagePhoto:photo,})
          }
      });
   }
   _selectImageTwo() {
    const options = { maxWidth: 500,maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker')
      }else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }else {
      
        let source = { uri: response.uri};
        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       this.setState({ imageTwoSource: source , imageTwoPhoto:photo,})
        }
    });
   }
  _selectImageThree() {
      const options = { maxWidth: 500,maxHeight: 500,
        storageOptions: {
          skipBackup: true
        }
      };
  
      ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
          console.log('User cancelled photo picker')
        }else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }else {
        
          let source = { uri: response.uri};
          var photo = {
            uri: response.uri,
            type:"image/jpeg",
            name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
          };
         this.setState({ imageThreeSource: source , imageThreePhoto:photo,})
          }
      });
   }
  _selectImageFour() {
        const options = { maxWidth: 500,maxHeight: 500,
          storageOptions: {
            skipBackup: true
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          if (response.didCancel) {
            console.log('User cancelled photo picker')
          }else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }else {
          
            let source = { uri: response.uri};
            var photo = {
              uri: response.uri,
              type:"image/jpeg",
              name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
            };
           this.setState({ imageFourSource: source , imageFourPhoto:photo,})
            }
        });
  }
  _selectImageFive() {
          const options = { maxWidth: 500,maxHeight: 500,
            storageOptions: {
              skipBackup: true
            }
          };
      
          ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
              console.log('User cancelled photo picker')
            }else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }else {
            
              let source = { uri: response.uri};
              var photo = {
                uri: response.uri,
                type:"image/jpeg",
                name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
              };
             this.setState({ imageFiveSource: source , imageFivePhoto:photo,})
              }
          });
  }
  selectDocument() {
    const options = { maxWidth: 500,maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker')
      }else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }else {
      
        let source = { uri: response.uri};
        var photo = {
          uri: response.uri,
          type:"image/jpeg",
          name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
        };
       this.setState({ documentSource: source , documentPhoto:photo,})
        }
    });
  }

selectVideo() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 30,
      takePhotoButtonTitle: 'Take Video...',
      allowsEditing: true,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
     }else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }else {
      

     

        let source = { uri: response.uri};
        var video={
          name: 'name.mp4',
          uri: source.uri,
          type: 'video/mp4'
        }
        this.setState({videoSource: source,videoObj:video});
      }
    });
 }


 _fetchBodyTypes = async () =>{
  this.setState({loading_status:true})

             let url = urls.BASE_URL +'api/body_types'
                  fetch(url, {
                  method: 'GET',

                  }).then((response) => response.json())
                      .then((responseJson) => {

                        this.setState({loading_status:false})
                        if(!responseJson.error){
                         
                          var temp_arr=[]
                          
                          var type = responseJson.result
                          type.map(item=>{
                            Object.assign(item,{value:item._id,label:item.name,color:'black'})
                          })
                          this.setState({body_types:type})

                        }


                      else{
                        showMessage(responseJson.message)
                      }

                    }
                      ).catch((error) => {

                        this.setState({loading_status:false})
                        showMessage("Try Again.")
                      });



}

_fetchEthnicities = async () =>{
  this.setState({loading_status:true})

             let url = urls.BASE_URL +'api/ethnicities'
                  fetch(url, {
                  method: 'GET',

                  }).then((response) => response.json())
                      .then((responseJson) => {

                        this.setState({loading_status:false})
                        if(!responseJson.error){
                         
                          var temp_arr=[]
                          
                          var ethnicity = responseJson.result
                          ethnicity.map(item=>{
                            Object.assign(item,{value:item._id,label:item.name,color:'black'})
                          })
                          this.setState({ethnicities:ethnicity})

                        }


                      else{
                        showMessage(responseJson.message)
                      }

                    }
                      ).catch((error) => {

                        this.setState({loading_status:false})
                        showMessage("Try Again.")
                      });



}




componentWillMount(){
    var result  = this.props.navigation.getParam('result')
    console.log('=====',result.details.gallery)

      result.details.gallery.sort((a,b) => {
        if(a && b) a.image_tag - b.image_tag
      });
      //console.log('=====',result.details.gallery)
      var tempArr = result.details.gallery
      var final_g = [result.details.gallery[0],null,null,null,null]
     

      if(tempArr[1] && tempArr[1].image_tag == 1){
        final_g[1] = tempArr[1]
      }
      else if(tempArr[1] && tempArr[1].image_tag != 1){
       final_g[tempArr[1].image_tag] = tempArr[1]

      }

      if(tempArr[2] && tempArr[2].image_tag == 2){
        final_g[2] = tempArr[2]
      }
      else if(tempArr[2] && tempArr[2].image_tag != 2){
       final_g[tempArr[2].image_tag] = tempArr[2]

      }
      

      if(tempArr[3] && tempArr[3].image_tag == 3){
        final_g[3] = tempArr[3]
      }
      else if(tempArr[3] && tempArr[3].image_tag != 3){
       final_g[tempArr[3].image_tag] = tempArr[3]

      }
      


      if(tempArr[4] && tempArr[4].image_tag == 4){
        final_g[4] = tempArr[4]
      }
      else if(tempArr[4] && tempArr[2].image_tag != 4){
       final_g[tempArr[4].image_tag] = tempArr[4]

      }
      


      var i;
      // for (i = 1; i < 5; i++) {
      //   if(tempArr[i]){
      //     if(tempArr[i].image_tag == i){
      //       final_g[i] = tempArr[i] 
      //     }
      //     else{

      //     }

      //   }
      //   else{
      //     final_g[i] = null
      //   }
      //   if(i < 4){

      //     if(!tempArr[i]){
      //       final_g.push(null)
      //     }

      //     else if(tempArr[i].image_tag != i){
      //       final_g.push(null)
      //       if(tempArr[i] && tempArr[i].image_tag != i) final_g.push(tempArr[i])
           

      //     }
      //     else{
      //       final_g.push(tempArr[i])
      //     }
      //   }


      //   if(i == 4){
      //     if(tempArr[i] != null && tempArr[i].image_tag == 4){
      //       final_g.push(tempArr[i])
      //     }
      //   }
      // }

      result.details.gallery=final_g
      console.log(final_g)


     this.setState({
      display_name : result.details.display_name,
      about : result.details.about,
      profession: result.details.profession,
      email: result.details.email,
      age: result.details.age,
      gender:result.details.gender,
      height:result.details.height.toString(),
      weight:result.details.weight.toString(),
      interested_gender: result.details.interested_gender,
      country : result.details.country.name,
      country_id : result.details.country._id,
      state : result.details.state.name,
      state_id : result.details.state._id,
      city : result.details.city.name,
      city_id : result.details.city._id,
      ethnicity : result.details.ethnicity.name,
      ethnicity_id : result.details.ethnicity._id,
      body_type : result.details.body_type.name,
      body_type_id : result.details.body_type._id,
      gallery:final_g,
      user_details:result.details

    })
  
   

    this._fetchBodyTypes()
    this._fetchEthnicities()
    this._fetchCountries()
    this._fetchStates(result.details.country._id)
    this._fetchCities(result.details.state._id)
     this._myProfile()


     AsyncStorage.getItem('lang')
     .then((item) => {
               if (item) {
                 I18n.locale = item.toString()
             }
             else {
                  I18n.locale = 'en'
               }
     });  


}

_updateVisibility = async (visibility_status) =>{
         
  var formData = new FormData();

  formData.append('user_id',this.props.user.user_id);
  formData.append('visibility_status',visibility_status);
  

  console.log("----",formData)

           let url = urls.BASE_URL +'api/change_visibility'
                fetch(url, {
                method: 'POST',
                body:formData

                }).then((response) => response.json())
                    .then((responseJson) => {
                      console.log("----",responseJson)
                      showMessage(responseJson.message,false)
                     
                  }
                    ).catch((error) => {

                      showMessage(error.message)
                    });



}


   leftButtonClick= () =>{
     this.logout()
    //this.logoutModal.setState({visibility:false})
   }


   rightButtonClick= () =>{
    this.logoutModal.setState({visibility:false})
  
   }


  _checkImageUpload =() =>{
    var counter = 0
    const {user_details}  = this.state
    if(!(user_details.gallery[0] == null || this.state.userImageSource == null)){
        counter = counter + 1
    }
    if(!(user_details.gallery[1] == null || this.state.imageTwoSource == null)){
      counter = counter + 1
    }
    if(!(user_details.gallery[2] == null || this.state.imageThreeSource == null)){
      counter = counter + 1
    }
    if(!(user_details.gallery[3] == null || this.state.imageFourSource == null)){
      counter = counter + 1
    }
    if(!(user_details.gallery[4] == null || this.state.imageFiveSource == null)){
      counter = counter + 1
    }


    if (counter > 4) return false
    else return true
  }

  _checkImageCount =() =>{

    var counter = 0
    const {user_details}  = this.state

    if((user_details.gallery[0] !== null || user_details.gallery[0] !== undefined ) && this.state.userImageSource !== null  ){
        counter = counter + 1
    }
    if((user_details.gallery[0] !== null || user_details.gallery[0] !== undefined ) && this.state.imageTwoSource !== null ){
      counter = counter + 1
    }
    if((user_details.gallery[0] !== null || user_details.gallery[0] !== undefined )  && this.state.imageThreeSource !== null){
      counter = counter + 1
    }
    if((user_details.gallery[0] !== null || user_details.gallery[0] !== undefined )  && this.state.imageFourSource !== null){
      counter = counter + 1
    }
    if((user_details.gallery[0] !== null || user_details.gallery[0] !== undefined )  && this.state.imageFiveSource !== null){
      counter = counter + 1
    }

    console.log(counter)
    return counter
  }

  checkImage =()=>{
    var counter = 0;
    console.log("IMAge two-------- if",this.state.user_details.gallery)

    const {user_details}  = this.state
        if(!(this.state.imageTwoPhoto == null && user_details.gallery[1] == null) ){
          counter = counter + 1
          
        }
        

        if(!(this.state.imageThreePhoto == null  && user_details.gallery[2] == null )){
          console.log("IMAge three--------")
          counter = counter + 1
        }
        

         if(!(this.state.imageFourPhoto == null  && user_details.gallery[3] == null) ){
          console.log("IMAge four--------")
          counter = counter + 1     
         }
        

        else if(!(this.state.imageFivePhoto == null  && user_details.gallery[4] == null) ){
          console.log("IMAge five--------")
          counter = counter + 1
        }


        



           console.log(counter)

              if(counter > 3){
                Platform.OS == 'android'?
                ToastAndroid.show("You can't upload more images.",ToastAndroid.SHORT)
                :
                Alert.alert("You can't upload more images.")
              }
              else{
                this._selectImageRunTime(counter + 1)
              }

  }

  deleteImage = (image_tag)=>{
    this.setState({loading_status:true})
    var formData = new FormData();
   formData.append('user_id',this.props.user.user_id);
   formData.append('image_tag',image_tag);

         let url = urls.BASE_URL +'api/remove_gallery_pic'
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then( (responseJson) => {
                 this.setState({loading_status:false})

                 console.log("FFF",JSON.stringify(responseJson))


              if (responseJson.status){
                var someProperty = {...this.state.user_details}
                  if(image_tag ==1){
                    someProperty.gallery[1] = null;
                    
                  }
                  if(image_tag ==2){
                    someProperty.gallery[2] = null;
                    
                  }
                  if(image_tag ==3){
                    someProperty.gallery[3] = null;
                    
                  }
                  if(image_tag ==4){
                    someProperty.gallery[4] = null;
                    
                  }
                 
                 
                  this.setState({user_details:someProperty})
                  
                 


                 }else{
                       showMessage(responseJson.message)
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                       showMessage('Try Again.')
                  
             });
  }

  


  _selectImageRunTime = (index) =>{
   //showMessage(index.toString())
    if(index == 0){
      this.selectMainImage()

    }
    else if(index == 1){
      this._selectImageTwo()
    }
    else if(index == 2){
      this._selectImageThree()
    }
    else if(index == 3){
     // showMessage(index.toString())
      this._selectImageFour()
    }
    else if(index == 4){
      this._selectImageFive()
    }
   // showMessage('cannot select more images')
  }


  _galleryOneImage =()=>{
    if(this.state.user_details.gallery[1] == null && this.state.imageTwoPhoto==null){
      return null
    }
    if(this.state.user_details.gallery[1] !== null && this.state.imageTwoPhoto==null){
      return(  
        <View style={styles.mainView}>

        <Image source={{uri:urls.BASE_URL + this.state.user_details.gallery[1].image}}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageTwo.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>

        <View style={styles.deleteView}>
        <TouchableOpacity onPress={()=>this.deleteImage(1)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/error2.png')} />
           </TouchableOpacity>
      
        </View>



        </View>
      )
    }
    if(this.state.user_details.gallery[1] == null && this.state.imageTwoPhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageTwoSource}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageTwo.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
    if(this.state.user_details.gallery[1] !== null && this.state.imageTwoPhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageTwoSource}
               style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:40}}  />
               <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageTwo.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
  }

  _galleryTwoImage =()=>{
    if(this.state.user_details.gallery[2] == null && this.state.imageThreePhoto==null){
      return null
    }
    if(this.state.user_details.gallery[2] !== null && this.state.imageThreePhoto==null){
      return(  
        <View style={styles.mainView}>

        <Image source={{uri:urls.BASE_URL + this.state.user_details.gallery[2].image}}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageThree.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>

        <View style={styles.deleteView}>
        <TouchableOpacity onPress={()=>this.deleteImage(2)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/error2.png')} />
           </TouchableOpacity>
      
        </View>
        </View>
      )
    }
    if(this.state.user_details.gallery[2] == null && this.state.imageThreePhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageThreeSource}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageThree.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
    if(this.state.user_details.gallery[2] !== null && this.state.imageThreePhoto !==null){
      return(   <View style={styles.mainView}>

        <Image source={this.state.imageThreeSource}
               style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
               <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageThree.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
  }

  _galleryThreeImage =()=>{
    if(this.state.user_details.gallery[3] == null && this.state.imageFourPhoto==null){
      return null
    }
    if(this.state.user_details.gallery[3] !== null && this.state.imageFourPhoto==null){
      return(  
        <View style={styles.mainView}>

        <Image source={{uri:urls.BASE_URL + this.state.user_details.gallery[3].image}}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFour.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>

        <View style={styles.deleteView}>
        <TouchableOpacity onPress={()=>this.deleteImage(3)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/error2.png')} />
           </TouchableOpacity>
      
        </View>
        </View>
      )
    }
    if(this.state.user_details.gallery[3] == null && this.state.imageFourPhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageFourSource}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFour.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
    if(this.state.user_details.gallery[3] !== null && this.state.imageFourPhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageFourSource}
               style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
               <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFour.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
  }

  _galleryFourImage =()=>{
    if(this.state.user_details.gallery[4] == null && this.state.imageFivePhoto==null){
      return null
    }
    if(this.state.user_details.gallery[4] !== null && this.state.imageFivePhoto==null){
      return(  
        <View style={styles.mainView}>

        <Image source={{uri:urls.BASE_URL + this.state.user_details.gallery[4].image}}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFive.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>

        <View style={styles.deleteView}>
        <TouchableOpacity onPress={()=>this.deleteImage(4)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} 
           source={require('../assets/error2.png')} />
           </TouchableOpacity>
      
        </View>
        </View>
      )
    }
    if(this.state.user_details.gallery[4] == null && this.state.imageFivePhoto !==null){
      return(  <View style={styles.mainView}>
        <Image source={this.state.imageFiveSource}
        style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
        <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFive.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
    if(this.state.user_details.gallery[4] !== null && this.state.imageFivePhoto !==null){
      return(  <View style={styles.mainView}>

        <Image source={this.state.imageFiveSource}
               style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden',borderRadius:0}}  />
               <View style={styles.crossView}>
        <TouchableWithoutFeedback onPress={this._selectImageFive.bind(this)}>
           <Image style={{width: 25, height: 25,marginTop:0,alignSelf:'flex-end'}} source={require('../assets/editpic.png')} />
           </TouchableWithoutFeedback>
      
        </View>
        </View>)
    }
  }



   
    render() {
        const {user_details}  = this.state
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
               />
           <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }

              centerComponent={{ text: I18n.t('edit_profile'), style: { color: colors.WHITE } }}
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}/>
          


         <View  style={{flex:1}}>
           <KeyboardAwareScrollView 
           resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={styles.container}
          >

           



         
              
                <View style={{height:'20%',backgroundColor:colors.COLOR_PRIMARY,width:'100%'}}>
                {
                  this.state.userImagePhoto == null ?
                  <Image style={{ width: '100%', height: '100%' }} 
                  //resizeMode='contain'
                  source={{uri:urls.BASE_URL + user_details.user_image}}/>
                  :
                  <Image style={{ width: '100%', height: '100%' }} 
                  //resizeMode='contain'
                  source={this.state.userImageSource}/>

                }
              

                <View style={{position:'absolute',bottom:20,right:20}}>
                <TouchableOpacity onPress={()=>{
                        this.selectMainImage()
                   }}>
                 <Image 
                 source={require('../assets/cam.png')} style={{width:40,height:40}} resizeMode='contain'/>
   
                 </TouchableOpacity>
                </View>

                </View>


                

               

                <View style={{padding:10}}>

                {/**galleries */}
                <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center',marginTop:5}}>

                {
                 
                  this._galleryOneImage()
                 
                }{ this._galleryTwoImage()
                 }{ this._galleryThreeImage()}
                 {this._galleryFourImage()}

                <View style={{height:50,width:50,borderRadius:35}}>
                <TouchableWithoutFeedback onPress={this.checkImage}>

                    <Image style={{width: '100%', height: '100%',marginTop:0}}
                    source={require('../assets/add-pic.png')} />
                 </TouchableWithoutFeedback>
                 </View>

                </View>

                   {/*gallery ends  */}
      

                  {/*inputsss  */}

                  <Text style={{textTransform:'uppercase',
                  color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('email')}</Text>
                  <TextInput
                  style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'white',height:45}}
                  value={this.state.email}
                  numberOfLines={1}
                  placeholder={'Email...'}
                  placeholderTextColor={'grey'}
                  onChangeText={(email) => this.setState({ email})}
                  multiline={true}
                  editable={false}
                  numberOfLines={1}
                  blurOnSubmit={false}
                />

                 <Text style={{textTransform:'uppercase',
                 color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:10}}>{I18n.t('display_name')}</Text>
                 <TextInput
                 style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'grey',height:45}}
                 value={this.state.display_name}
                 numberOfLines={1}
                 placeholder={'Name...'}
                 placeholderTextColor={'grey'}
                 onChangeText={(display_name) => this.setState({ display_name})}
                 multiline={false}
                 numberOfLines={1}
                 blurOnSubmit={false}
               />

              

             <Text style={{textTransform:'uppercase',
               color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('profession')}</Text>
               <TextInput
               style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'grey',height:45}}
               value={this.state.profession}
               numberOfLines={1}
               placeholder={'Profession...'}

               placeholderTextColor={'grey'}
               onChangeText={(profession) => this.setState({ profession})}
               multiline={true}
               numberOfLines={1}
               blurOnSubmit={false}
             />

             <Text style={{textTransform:'uppercase',
             color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('height_cms')}</Text>
             <TextInput
             style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'grey',height:45}}
             value={this.state.height}
             numberOfLines={1}
             placeholder={'Height...'}
             placeholderTextColor={'grey'}
             onChangeText={(height) => this.setState({ height})}
             maxLength={3}
             keyboardType = 'numeric'
             textContentType='telephoneNumber'
             blurOnSubmit={false}
           />

           <Text style={{textTransform:'uppercase',
           color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('weight_kgs')}</Text>
           <TextInput
           style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'grey',height:45}}
           value={this.state.weight}
           numberOfLines={1}
           placeholder={'Weight...'}
           placeholderTextColor={'grey'}
           onChangeText={(weight) => this.setState({ weight})}
           multiline={true}
           maxLength={3}
           keyboardType = 'numeric'
           textContentType='telephoneNumber'
           blurOnSubmit={false}
         />


         <Text style={{textTransform:'uppercase',
         color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('about')}</Text>
         <TextInput
         style={{width:'98%',borderBottomColor:'grey',borderBottomWidth:2,color:'grey',height:65}}
         value={this.state.about}
         numberOfLines={1}
         placeholder={I18n.t('about')}

         placeholderTextColor={'grey'}
         onChangeText={(about) => this.setState({ about})}
         multiline={true}
         numberOfLines={1}
         blurOnSubmit={false}
       />


            



              {/* inputss  end*/}


              {/* interested gender and gender */}

              <View style={{flexDirection:'row',alignItems:'center',marginTop:10,marginBottom:10}}>
              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
              
                        <RNPickerSelect
                        placeholder={{
                            label:I18n.t('gender'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                          enablesReturnKeyAutomatically
                          Icon={() => {
                            return <FontAwesomeIcon icon="coffee" color='white'/>;
                        }}
                        placeholderTextColor={'grey'}
                        value={this.state.gender}
                        onValueChange={(value,index) => this.setState({
                               gender: value,
                         })}
                        items={this.state.genderData}
                        style={pickerSelectStyles}
                       />

              </View>

              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
            
                        <RNPickerSelect
                        placeholder={{
                            label:I18n.t('interested_gender'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                            }}
                            placeholderTextColor={'grey'}
                            value={this.state.interested_gender}
                        onValueChange={(value,index) => this.setState({
                          interested_gender: value,
                        })}
                        items={this.state.genderData}
                        style={pickerSelectStyles}
                        />

                  </View>
              
              </View>

                {/* interested gender  and gender ends */}


                 {/* ethnicity  and body type  */}

              <View style={{flexDirection:'row',alignItems:'center',marginTop:10,marginBottom:10}}>
              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('ethnicity'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                          enablesReturnKeyAutomatically
                          Icon={() => {
                            return <FontAwesomeIcon icon="coffee" color='white'/>;
                        }}
                        placeholderTextColor={'grey'}
                        value={this.state.ethnicity_id}
                        onValueChange={(value,index) => this.setState({
                          ethnicity_id: value,
                         })}
                        items={this.state.ethnicities}
                        style={pickerSelectStyles}
                       />

              </View>

              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('body_type'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                            }}
                            placeholderTextColor={'grey'}
                            value={this.state.body_type_id}
                        onValueChange={(value,index) => this.setState({
                          body_type_id: value,
                        })}
                        items={this.state.body_types}
                        style={pickerSelectStyles}
                        />

                  </View>
              
              </View>

                {/* ethnicity  and body type ends */}


                 {/* age  and country */}

              <View style={{flexDirection:'row',alignItems:'center',marginTop:10,marginBottom:10}}>
              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('age'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                        
                          placeholderTextColor={'grey'}
                          value={this.state.age}
                        onValueChange={(value,index) => this.setState({
                        age: value,
                      })}
                        items={age}
                        style={pickerSelectStyles}
                       />

              </View>

              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('country'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                            }}
                            placeholderTextColor={'grey'}
                            value={this.state.country_id}
                        onValueChange={(value,index) => {
                             if(value)  this._fetchStates(value)
                            this.setState({
                              country_id: value,
                            })
                          }}
                        items={this.state.countries}
                        style={pickerSelectStyles}
                        />

                  </View>
              
              </View>

                {/* age  and countryr ends */}


                  {/* state  and cities */}

              <View style={{flexDirection:'row',alignItems:'center',marginTop:10,marginBottom:10}}>
              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('state'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                          enablesReturnKeyAutomatically
                          Icon={() => {
                            return <FontAwesomeIcon icon="coffee" color='white'/>;
                        }}
                        placeholderTextColor={'grey'}
                        value={this.state.state_id}
                        onValueChange={(value,index) => {
                          if(value)  this._fetchCities(value)
                         this.setState({
                           state_id: value,
                         })
                       }}
                        items={this.state.states}
                        style={pickerSelectStyles}
                       />

              </View>

              <View style={{flex:1,borderColor:colors.WHITE,borderWidth:2,margin:5,height:50}}>
                        <RNPickerSelect
                        placeholder={{
                            label: I18n.t('city'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                            }}
                            placeholderTextColor={'grey'}
                            value={this.state.city_id}
                        onValueChange={(value,index) => this.setState({
                          city_id: value,
                        })}
                        items={this.state.cities}
                        style={pickerSelectStyles}
                        />

                  </View>
              
              </View>

                {/* state  and cities */}
               

                  {/* upload coucmnets */}
                  <Text style={{textTransform:'uppercase',
                  color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:5}}>{I18n.t('add_document')} (+)</Text>

                  <View style={{backgroundColor:colors.WHITE,
                  height:70,width:70,justifyContent:'center',alignItems:'center',margin:10,}}> 
                  <TouchableOpacity onPress={()=> this.selectDocument()}>
                  <Image 
                  source={require('../assets/doc.png')} style={{width:40,height:40}} resizeMode='contain'/>
                  </TouchableOpacity>
                 
                  
                  </View>
                  {
                    this.state.user_details.document != null?
                    <Text style={{textTransform:'uppercase',
                    color:'green',fontWeight:'200',marginTop:2,marginLeft:10}}>{I18n.t('document_added')}</Text>
                    : null
                  }
                

                  {/* upload coucmnets ends*/}

                   {/* upload video */}

                   {/*
                   <Text style={{textTransform:'uppercase',
                   color:colors.COLOR_PRIMARY,fontWeight:'bold',marginTop:15}}>{I18n.t('add_video')} (+)</Text>

                   <View style={{backgroundColor:colors.WHITE,
                    height:70,width:70,justifyContent:'center',alignItems:'center',margin:10,}}> 
                    <TouchableOpacity onPress={()=> this.selectVideo()}>
                    <Image 
                    source={require('../assets/video.png')} style={{width:40,height:40}} resizeMode='contain'/>
                    </TouchableOpacity>
      
                    
                    </View>
                     */}
                   {/* upload video ends*/}



                   </View>
                  
                   {
                    this.state.user_details.video != null?
                    <Text style={{textTransform:'uppercase',
                    color:'green',fontWeight:'200',marginTop:0,marginLeft:10}}>{I18n.t('video_added')}</Text>
                    : null
                  }

                 
                            
                   <ButtonComponent 
                   style={{width : d.width * 0.8,alignSelf:'center'}}
                    handler={this.handler}
                   label ={I18n.t('save')}/> 
           

                        

                               
           </KeyboardAwareScrollView>
           { this.state.loading_status && <ProgressBar/> }

           </View>       
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    change: (userinfo) => dispatch(changeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   backgroundColor:colors.COLOR_PRIMARY,
    flex:1
  },
  container:{
    height:dimensions.SCREEN_HEIGHT * 2.1,

    paddingBottom:10,
    backgroundColor:colors.BLACK,

   
  },
  topContainer:{
    height:dimensions.SCREEN_HEIGHT * 0.3,
    width:dimensions.SCREEN_WIDTH * 0.9,
    justifyContent:'center',
    alignItems:'center',
   

  },
  imageParentView:{
                            
    height:dimensions.SCREEN_HEIGHT * 0.2,
    width:dimensions.SCREEN_HEIGHT * 0.2,
  },
  editImage:{
    position:'absolute',
    bottom:Platform.OS === 'android' ? 45 :55,
    right:dimensions.SCREEN_WIDTH * 0.33
  },
  bottomContainer:{
    width:'100%',
    marginTop:30
  },
  menuParent:{
    flexDirection:'row',
    alignItems:'center'
  },
  menuImage:{width:25,height:25,marginRight:20},
  menuLabel:{
    color:colors.WHITE,
    fontSize:15
   
  },
  menuDivider:{
    height:0.5,
    width:'100%',
    backgroundColor:'grey',
    marginBottom:5,
    marginTop:5
  },
  logo_image:{
    height:"95%", 
    width:'100%',
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:3,
    borderRadius:dimensions.SCREEN_HEIGHT * 0.14,
    
  },
  mainView:{height:60,width:60,borderRadius:0,borderColor:colors.COLOR_PRIMARY,
    borderWidth:0.5},
  crossView:{position:'absolute',left:45,top:-10},
  deleteView:{position:'absolute',left:45,bottom:-10}

 
}
)

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
   
    borderRadius: 4,
    color: 'grey',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
  
    borderRadius: 8,
    color: 'grey',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});



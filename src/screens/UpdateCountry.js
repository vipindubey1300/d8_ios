import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';

import {showMessage} from '../config/snackmsg';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import CustomDropDown from '../components/CustomDropDown';
import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';


const d = Dimensions.get("window")


export default class UpdateCountry extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          countries:[],
          states:[],
          cities:[],
          loading_status:false
        }

    }
    _fetchCountries = async () =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/countries'
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              var temp_arr=[]
                              
                              var country = responseJson.result
                              country.map(item=>{
                                Object.assign(item,{value:item._id,label:item.name,color:'black'})
                              })
                              this.setState({countries:country})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchStates = async (countryId) =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/states?country_id=' + countryId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var state = responseJson.result
                                state.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                                state.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({states:state})
                              }
                              else{
                                showMessage('No State Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchCities = async (stateId) =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/cities?state_id=' + stateId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var city = responseJson.result
                                city.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                                city.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({cities:city})
                              }
                              else{
                                showMessage('No City Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }
    

componentWillMount(){
    this._fetchCountries()
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
}




_isValid (){
  
  const country = this.countryDropDown.getSelectedValue()
  const state = this.stateDropDown.getSelectedValue()
  const city = this.cityDropDown.getSelectedValue()
 if(country.selected_value == null){
    showMessage('Select Country')
    return false

  }
  if(state.selected_value == null){
    showMessage('Select state')
    return false

  }
  if(city.selected_value == null){
    showMessage('Select city')
    return false

  }
  
  else{
    return true;
  }


}


_onSelect = () =>{
  
    if(this._isValid()){
      const country = this.countryDropDown.getSelectedValue()
      const state = this.stateDropDown.getSelectedValue()
     const city = this.cityDropDown.getSelectedValue()
      var result  = this.props.navigation.getParam('result')

      this.setState({loading_status:true})
      var formData = new FormData();
  
  
     formData.append('user_id',result['user_id']);
     formData.append('country_id',country.selected_value);
     formData.append('state_id', state.selected_value);
     formData.append('city_id', city.selected_value);


     console.log("FFF",JSON.stringify(formData))
           let url = urls.BASE_URL +'api/update_country'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
          }).then((response) => response.json())
               .then( (responseJson) => {
                   this.setState({loading_status:false})

                   console.log("FFF",JSON.stringify(responseJson))


                if (responseJson.status){
                    let tempObject = {
                      'user_id' : result['user_id'],
                    }
                   this.props.navigation.navigate("SelectGender",{result:tempObject})

                   }else{
                         showMessage(responseJson.message)
                  }
               }).catch((error) => {
                         this.setState({loading_status:false})
                         showMessage('Try Again.')
                    
               });

    }
 }

 onSelectCountry =(countryId)=>{
  // console.log(countryId)
  if(countryId){
    this.setState({states:[],cities:[]})
    this._fetchStates(countryId)
  }
}

onSelectState =(stateId)=>{
 // console.log(countryId)
 if(stateId){
   this.setState({cities:[]})
   this._fetchCities(stateId)
 }
}


   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


        


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
             // enableAutomaticScroll={(Platform.OS === 'ios')}
           
             >



              <View style={styles.container}>
            

           

                 <Image source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>

                 <Text style={styles.headingText}>{ I18n.t('choose_country')}</Text>


               


                          {/* text fields */}

                          <CustomDropDown
                          style={{marginTop:20}}
                          data={this.state.countries}
                          ref={ref => this.countryDropDown = ref}
                          inputImage={require('../assets/location-text.png')}
                          placeholder={{
                            label: I18n.t('country'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                          onSelectValue = {this.onSelectCountry}

                          />


                        <CustomDropDown
                        style={{marginTop:10}}
                        data={this.state.states}
                        placeholder={{
                          label: I18n.t('state'),
                          value: null,
                          color: colors.COLOR_PRIMARY,
                        }}
                        
                        onSelectValue = {this.onSelectState}
                        ref={ref => this.stateDropDown = ref}
                        inputImage={require('../assets/location-text.png')}
                        />

                        <CustomDropDown
                        style={{marginTop:10}}
                        data={this.state.cities}
                        placeholder={{
                          label: I18n.t('city'),
                          value: null,
                          color: colors.COLOR_PRIMARY,
                        }}
                        ref={ref => this.cityDropDown = ref}
                        inputImage={require('../assets/location-text.png')}
                        />

                             <ButtonComponent 
                             style={{width : d.width * 0.8,}}
                              handler={this._onSelect}
                             label ={I18n.t('save')}/>
                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           

           { this.state.loading_status && <ProgressBar/> }


          
          </View>

        )
    }
}

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 1,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
    
    
  },
  logo_image:{
    height:"25%", 
    width:'30%',
    
  },
 headingText:{
  color:'white',
  fontWeight:'bold',
  fontSize:18,
  marginBottom:15
  },
  titleText :{
    color:'white',
    
    fontSize:14,
    marginBottom:35
    }
 

}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
//import Video from 'react-native-video-controls';
import Video from 'react-native-video';

import I18n from '../i18n';

import Carousel from 'react-native-snap-carousel';
import { ActivityIndicator } from 'react-native-paper';


const d = Dimensions.get("window")

class FriendStory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          stories:[],
          video_loading:false
        
           
        }

    }


    componentWillMount(){
       // this._getMyStory()
       var result  = this.props.navigation.getParam('result')
        var stories = result.stories
        this.setState({stories})
        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });  
        
    }

    onBuffer =() =>{
      console.log('onbuffer')
        this.setState({video_loading:true})
    }

    onLoadStart =() =>{
      console.log('onloadstart')
      this.setState({video_loading:true})
  }

    onLoad =() =>{
      console.log('onload ')
      this.setState({video_loading:false})
  }

    _renderItem = ({item, index}) => {
      return (
          <View style={{height:'100%',backgroundColor:'grey',justifyContent:'center',alignItems:'center'}}>
          {
            item.media_type == 0
            ? <Image source={{uri:urls.BASE_URL + item.media_link}} 
            overflow='hidden'
            style={{height:'100%',width: '100%',}} resizeMode='cover'/>
            :
            <Video source={{uri:urls.BASE_URL+ item.media_link}}
            onBuffer={this.onBuffer}    
            onLoad={this.onLoad}
            onLoadStart={this.onLoadStart} 
            disableFocus
            repeat
             muted
             resizeMode='cover'
             style={{height:'100%',width:'100%'}}  />

          }

         {
            item.caption.toString().length > 0 ?
            <View style={{position:'absolute',bottom:10,
            left:10,right:10,padding:10,backgroundColor:'rgba(0,0,0,0.5)',borderRadius:11,elevation:10
            }}>  
            <View style={{flexDirection:'row'}}>
             <Text style={{ textTransform: 'uppercase',color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:15}}>{item.caption} </Text>
     
            </View>
            
     
     
            </View>
            : null

         }


         {
           this.state.video_loading == true ?
           <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.3)', justifyContent: 'center' }
          ]}>
          <ActivityIndicator color={'red'}/>
          </View>
          :null

         }

        
   

          
          </View>
      );
  }


    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('story'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}/>
            <View style={{flex:1,backgroundColor:colors.BLACK,padding:20}}>
               
                        <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.stories}
                        renderItem={this._renderItem}
                        sliderWidth={dimensions.SCREEN_WIDTH * 0.9}
                        itemWidth={dimensions.SCREEN_WIDTH * 0.8}
                    />


              
           
            </View>

            
         
       
           { this.state.loading_status && <ProgressBar/> }
             
           
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(FriendStory);

let styles = StyleSheet.create({
  safeAreaContainer:{
    flex:1 
  },
  container:{
    height: '100%',
  },
  user_image:{
    height:50, 
    width:50,
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
 
}
)



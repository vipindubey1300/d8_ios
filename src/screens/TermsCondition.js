import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';


const d = Dimensions.get("window")

export default class TermsCondition extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: "Terms and Services", style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
             />
              
                    <WebView
                    style={{backgroundColor:colors.BLACK}}
                        source = {{ uri:
                          urls.BASE_URL + 'api/terms' }}>
                      
                        </WebView>
                
              

           
            

       
         
       
             
             
           
         
          </View>

        )
    }
}

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
 
}
)



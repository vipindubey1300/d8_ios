import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';



const d = Dimensions.get("window")

class ImageView extends React.Component {

    constructor(props) {
      super(props);
      this.state={
        loading_status:false,
        image_url:''
      
       
      };
  
  
    }
  

  
  
  
    componentWillMount(){
  
      let result = this.props.navigation.getParam('result')
      var url = result['image_url']
      this.setState({image_url:url})

      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });  
    
    
    }
  
  
     
   
  
      render()
      {
  
  
    
        
          return(
            <SafeAreaView style={styles.container}>
            <Header
            statusBarProps={{ barStyle: 'light-content' }}
            barStyle="light-content" // or directly
 
             leftComponent={
             <TouchableOpacity onPress={()=>{
 
               this.props.navigation.goBack()
              }}>
            <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>
 
            </TouchableOpacity>
            }
            statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
            containerStyle={{
              //to remove bottom border
              borderBottomColor: colors.COLOR_SECONDARY,
            backgroundColor: colors.COLOR_SECONDARY,
        
        
          }}
           />
 
      
  
            <View style={{backgroundColor:'black',width:'100%',
            flex:1,justifyContent:'center',alignItems:'center'}}
            >
  
              <Image source={{uri:this.state.image_url}}
              resizeMode='contain'
              style={{width:'100%',height:Dimensions.get('window').height * 0.9}}/>
                  
  
  
  
              </View>
  
             
  
  
              
      
             
            </SafeAreaView>
  
          )
      }
  }
  
  const mapStateToProps = state => {
      return {
      
          user: state.user,
      
      };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
     
      change: (userinfo) => dispatch(changeUser(userinfo)),
     
      
    }
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(ImageView);
  
  
  
  const styles = StyleSheet.create({
      container: {
        flex: 1,
      },
     
    loading: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'rgba(128,128,128,0.5)'
    }
    });

import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import Video from 'react-native-video-controls';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';

import Carousel from 'react-native-snap-carousel';
import RBSheet from "react-native-raw-bottom-sheet";
import { Item } from 'native-base';
import I18n from '../i18n';


const d = Dimensions.get("window")

class MyStory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          stories:[]
        
           
        }

    }


    componentWillMount(){
       // this._getMyStory()
       var result  = this.props.navigation.getParam('result')
        var stories = result.stories
        this.setState({stories})
        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });  
        
    }

    _renderItem = ({item, index}) => {
      
        return (
            <View style={{height:'100%',backgroundColor:colors.COLOR_PRIMARY,justifyContent:'center',alignItems:'center'}}>
            {
              item.media_type == 0
              ? <Image source={{uri:urls.BASE_URL + item.media_link}} 
              overflow='hidden'
              style={{height:'100%',width: '100%',}} resizeMode='cover'/>
              :
              <Video source={{uri:urls.BASE_URL+ item.media_link}}
              disableBack
                disableFullscreen
                disableVolume
                disableSeekbar
                muted={true}
                autoplay={true}
                paused={false}
                onPause={()=> {}}
                onPlay={()=> {}}
                showOnStart={false}
                seekColor={colors.COLOR_PRIMARY}
               style={{height:'100%',width:'100%'}}  />

            }

           {
              item.caption.toString().length > 0 ?
              <View style={{position:'absolute',bottom:10,
              left:10,right:10,padding:10,backgroundColor:'rgba(0,0,0,0.5)',borderRadius:11,elevation:10
              }}>  
              <View style={{flexDirection:'row'}}>
               <Text style={{ textTransform: 'none',color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:15}}>{item.caption} </Text>
       
              </View>
              
       
       
              </View>
              : null

           }

          
     

            
            </View>
        );
    }

    render() {

      const ViewSheet = () =>{
        return(
          <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
         height={220}
          closeOnDragDown
          duration={250}
          customStyles={{
            container: {
            
            
              backgroundColor:colors.BLACK,
              padding:20,
              borderTopLeftRadius:15,
            borderTopRightRadius:15
            }
          }}
          animationType='fade'
          minClosingHeight={10}
        >
        
        <View style={{
          flexDirection:'row',
          justifyContent:'space-between',
          alignItems:'center'
        }}>

        <Text style={{color:colors.COLOR_PRIMARY,
          textTransform:"uppercase",fontWeight:'bold'}}>Viewed By</Text>
        <Image source={require('../assets/left-arrow-white.png')} style={{width:20,height:20}} resizeMode='contain'/>

        
        </View>

        </RBSheet>
        )
      }



        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
           <ViewSheet/>
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('story'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}/>
            <View style={{flex:1,backgroundColor:colors.BLACK,padding:20}}>
               
                        <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.stories}
                        renderItem={this._renderItem}
                        sliderWidth={dimensions.SCREEN_WIDTH * 0.9}
                        itemWidth={dimensions.SCREEN_WIDTH * 0.8}
                    />


              
           
            </View>

            
         
       
           { this.state.loading_status && <ProgressBar/> }
             
             {/* floating action button*/}
              {/*
             <View style={{
              position:'absolute',bottom:30,right:20,elevation:10,shadowOpacity:0.7
            }}>
            <TouchableOpacity onPress={()=>  this.RBSheet.open()
          }>
            <Image 
            source={require('../assets/story-add.png')} style={{width:60,height:60}} resizeMode='contain'/>

            </TouchableOpacity>
            </View>
            */}
               {/* floating action button end */}
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(MyStory);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
 
}
)



import React, {Component} from 'react';
import {Text, View,Alert, Image,Dimensions,ScrollView,ToastAndroid,Platform,
  StatusBar,ActivityIndicator,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {  Content,  Card,  CardItem,  Body,  Form,  Item,  Input,  Picker,} from "native-base";
import { CheckBox } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';
import I18n from '../i18n';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {showMessage} from '../config/snackmsg';
import {  colors,urls,dimensions } from '../Constants';
import ButtonComponent from '../components/ButtonComponent';
import ProgressBar from '../components/ProgressBar';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {Header} from 'react-native-elements';

 class ContactUs extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status: false ,
          complain_status : false,
          message:'',
          description:'',
          username:'',
          complain_detail:'',
          reason_one:false,
          reason_two:false,
          reason_three:false,
          backgroundColor:'#ffffff',
          user_id:''


      };


    }

    isContactValid =()=> {


      let valid = false;

      if (this.state.message.toString().trim().length > 0 && this.state.description.toString().trim().length > 0 ) {
        valid = true;
      }

      if (this.state.message.trim().length === 0) {
        showMessage('Enter Message Title')
        return false;
      }
      else if(this.state.description.trim().length === 0){
        showMessage('Enter description')
        return false;
      }
      return valid;
  }


    onContact =()=>{

    var formData = new FormData();
    formData.append('user_id', this.props.user.user_id);
    formData.append('message', this.state.message);
    formData.append('description', this.state.description);
    console.log(JSON.stringify(formData))
    if(this.isContactValid()){
                    this.setState({loading_status:true})
                    let url = urls.BASE_URL +'api/contact_us'

                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'multipart/form-data',
                    },
                body: formData

                  }).then((response) => response.json())
                        .then((responseJson) => {
                   this.setState({loading_status:false})
              // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                             if(responseJson.status){

                              showMessage('Sent successfully ')
                            this.props.navigation.goBack()

                          }
                          else{
                            showMessage(responseJson.message)
                            
                            //ToastAndroid.show(responseJson.message, ToastAndroid.SHORT);
                          }
                        }).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage('Try Again')
                          
                        });


    }
}




isComplainValid =()=> {


  let valid = false;

  if (this.state.username.toString().trim().length > 0 && this.state.complain_detail.toString().trim().length > 0 ) {
    valid = true;
  }

  if (this.state.username.trim().length === 0) {
    showMessage('Enter username')
    return false;
  }


  else if(this.state.complain_detail.trim().length === 0){
    showMessage('Enter Complain description')
    return false;
  }


  return valid;
}


onComplain =()=>{

  var response = '';
  if(this.state.reason_one ){
    response = response + "Model Accepted bid" + ","
  }
  if(this.state.reason_two){
    response = response + "Model paid coin" + ","
  }
  if(this.state.reason_three){
    response = response + "Model did not meet" + ","
  }

  response  = response.length > 1 ? 
      response.substring(0, response.length - 1)
     : response

var formData = new FormData();


formData.append('user_id',this.props.user.user_id);
formData.append('username', this.state.username.toLowerCase().trim());
formData.append('reason', response + ' ' + this.state.complain_detail);

console.log(JSON.stringify(formData))

if(this.isComplainValid()){
                this.setState({loading_status:true})
                let url = urls.BASE_URL +'api/report_user'
                fetch(url, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'multipart/form-data',
                },
               body: formData

              }).then((response) => response.json())
                    .then((responseJson) => {
               this.setState({loading_status:false})
          // ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);
                         if(responseJson.status){

                          this.props.navigation.goBack()
                          showMessage('Sent successfully')
                      
                      }
                      else{
                        showMessage(responseJson.message)
                       
                      }
                    }).catch((error) => {
                      this.setState({loading_status:false})
                      showMessage('Try Again')
                    });


}
}



componentWillMount() {
  AsyncStorage.getItem('user_id')
  .then((item) => {
            if (item) {

                    this.setState({user_id:item})
          }
          else {
            ToastAndroid.show("Error !")
            }
  });

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });

}



    componentDidMount() {



    }


    render() {
        return (

       <View style={{flex:1}}>


         {/*for header*/}

         <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly
               leftComponent={
               <TouchableOpacity onPress={()=>{
                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              centerComponent={{ text: I18n.t('contact_us'), style: { color: colors.WHITE } }}
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
             }}
       />


       {/*for main content*/}
       <KeyboardAwareScrollView style={styles.container} >

          <ScrollView style={{width:'100%',flex:1,height:'100%'}}
           contentContainerStyle={{ alignItems: 'center',
           }}>

           <View style={styles.body}>



           <View style={{padding:15}}>
               {/*for above part*/}
               <Text style={styles.name_text}>{I18n.t('message_title')}</Text>
                <TextInput
                style={[styles.inputText]}
                value={this.state.message}
                multiline={false}
                numberOfLines={1}
                editable={!this.state.complain_status}
                placeholderTextColor={colors.COLOR_PRIMARY}
                onChangeText={(message) => this.setState({ message })}
                 />

                <Text style={styles.about_text}>{I18n.t('description')}</Text>
                <TextInput
                style={[styles.bigInputText]}
                value={this.state.description}
                multiline={true}
                numberOfLines={1}
                editable={!this.state.complain_status}
                description={colors.COLOR_PRIMARY}
                onChangeText={(description) => this.setState({ description })}
                 />

               
                    {
                      this.state.complain_status
                      ? null
                      :
                                    <ButtonComponent 
                                    style={{width : dimensions.SCREEN_WIDTH * 0.8,alignSelf:'center'}}
                                      handler={this.onContact}
                                    label ={I18n.t('submit')}/>

                    }


                <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:-10}}>

                      <CheckBox
                         center
                         checkedColor={colors.COLOR_PRIMARY}
                          onPress={() => this.setState({ complain_status: !this.state.complain_status })}
                          checked={this.state.complain_status}
                        />
                      <Text style={{marginTop: 18,marginLeft:-15,color:'white'}}
                       >{I18n.t('d8_complain')}</Text>
                  </View>


             {/*en of first part*/}


                  {/*second part  */}
            {
              !this.state.complain_status
              ?
              null
              :
              <View style={{width:'100%'}}>
              <Text style={styles.name_text}>{I18n.t('username')}</Text>
              
              <TextInput
              style={[styles.inputText]}
              value={this.state.username}
              multiline={false}
              numberOfLines={1}
              editable={this.state.complain_status}
              placeholderTextColor={colors.COLOR_PRIMARY}
              onChangeText={(username) => this.setState({ username })}
               />


              <Text style={{marginTop: 10,marginLeft:10,color:colors.COLOR_PRIMARY,fontSize:15}}
                   >{I18n.t('suggestions')} :</Text>
                   <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:10}}>
                   <CheckBox
                      center
                      checkedColor={colors.COLOR_PRIMARY}
                       onPress={() => this.setState({ reason_one: !this.state.reason_one })}
                       checked={this.state.reason_one}
                     />
                   <Text style={{marginTop: 18,marginLeft:-15,color:'white'}}
                    >{I18n.t('reason_one')}</Text>
               </View>

             

           <View style={{ flexDirection: 'row' ,alignSelf:'flex-start',marginLeft:10}}>
           <CheckBox
              center
              checkedColor={colors.COLOR_PRIMARY}
               onPress={() => this.setState({ reason_three: !this.state.reason_three })}
               checked={this.state.reason_three}
             />
           <Text style={{marginTop: 18,marginLeft:-15,color:'white'}}
            >{I18n.t('reason_three')}</Text>
       </View>

       <Text style={styles.about_text}>{I18n.t('please_detail_complain')}</Text>
       <TextInput
       style={[styles.bigInputText]}
       value={this.state.complain_detail}
       multiline={true}
       numberOfLines={1}
       editable={this.state.complain_status}
       description={colors.COLOR_PRIMARY}
       onChangeText={(complain_detail) => this.setState({ complain_detail })}
        />


        <ButtonComponent 
        style={{width : dimensions.SCREEN_WIDTH * 0.8,alignSelf:'center'}}
          handler={this.onComplain}
        label ={I18n.t('submit')}/>

       
       </View>
            }




                   {/*end second part  */}




                  {/*height */}




                  {/*interest */}




           </View>



         </View>


         </ScrollView>
         </KeyboardAwareScrollView>

         { this.state.loading_status && <ProgressBar/> }


       </View>


        )
    }
}

const mapStateToProps = state => {
	return {
		user: state.user,
	};
};


export default connect(mapStateToProps, null)(ContactUs);


let styles = StyleSheet.create({
  container:{

    flex:1,
    backgroundColor: 'black'

  },
  header:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    width:'100%',
    height: Platform.OS === 'android'  ? '07%':'09%',
    paddingTop: Platform.OS === 'android'  ? 0:10,
    backgroundColor: colors.COLOR_PRIMARY,
    elevation:7,
    shadowOpacity:0.3


  },
  body:{
    width:'100%',
    flex:1,
    height:'100%',
  


  },
  bigInputText: {
    height:75,
    textAlignVertical:'top',
    padding:10,
    fontSize: 14,
    color:colors.WHITE,
    borderColor:colors.LIGHT_MAROON,
    borderWidth:2,
    borderRadius:15,
    backgroundColor:colors.DARK_MAROON,
    margin:7,
   
  },
  inputText: {
    height:40,
    textAlignVertical:'top',
    padding:10,
    fontSize: 12,
    color:colors.WHITE,
    borderColor:colors.LIGHT_MAROON,
    borderWidth:2,
    borderRadius:10,
    backgroundColor:colors.DARK_MAROON,
    margin:7,
   
  },
 name_text:{
  width:'100%',
  marginBottom:10,
  color:'white'

 },

 name_input: {
  width: "100%",
  height: 50,
padding:6,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
 // backgroundColor:this.state.complain_status ? "#D3D3D3" : "#ffffff"
},
about_text:{
  width:'100%',
  marginBottom:10,
  color:'white'
 },

 about_input: {
  width: "100%",
  height: 120,
  textAlignVertical:'top',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
  padding:6
},

saveButton:{

    marginTop:20,
     width:"100%",
     height:50,
     alignSelf:'center',
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',


  },
  saveText:{
    color:'black',
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  model_text:{
   fontSize:19,
   margin:3,
    alignSelf:'center',
    color:'black'
   },
   loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  }



}
)

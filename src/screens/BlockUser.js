import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';
import BigTextInput from '../components/BigTextInput';

import {  colors,urls,dimensions } from '../Constants';

import {showMessage} from '../config/snackmsg';
import I18n from '../i18n';



class BlockUser extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            loading_status:false,
         user_id:'',
         name:'',
         image:''
        }

    }



        _blockUser =() =>{
            var result  = this.props.navigation.getParam('result')
            var user_id = result.user_id
            var gender = result.gender

            const reason = this.reasonInput.getInputValue();


           
            var formData = new FormData();
            formData.append('user_id',this.props.user.user_id);
            formData.append('to_id',this.state.user_id);
            formData.append('reason',reason);
          
            
        
                   let url = urls.BASE_URL +'api/block_user'
                    this.setState({loading_status:true})
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data',
                   },
                   body: formData
                  }).then((response) => response.json())
                       .then( (responseJson) => {
                        this.setState({loading_status:false})
                            if (responseJson.status){
                              showMessage(responseJson.message,false)
                                this.props.navigation.navigate('ChatLists')
        
                           }else{
                                showMessage(responseJson.message)
                
                             }
                       }).catch((error) => {
                                 this.setState({loading_status:false})
                                 showMessage('Try Again.')
    
                       });
        }


componentWillMount(){
    //the user which i have to block
    var result  = this.props.navigation.getParam('result')
    var user_id = result.user_id
    var name = result.name
    var image = result.image

    this.setState({user_id,name,image})

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
   
}

   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              centerComponent={{ text: I18n.t('block_user'), style: { color: colors.WHITE } }}

              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
           style={{backgroundColor:'black'}}
           contentContainerStyle={{alignItems:'center',flexGrow:1,justifyContent:'center'}}
           >
           <View style={{height:100,width:100}}>
            <Image 
                source={{uri:urls.BASE_URL +this.state.image}} style={{height:'100%',width:'100%',
                overflow:'hidden',borderRadius:0}} resizeMode='contain'/>
          </View>

          <Text style={{color:'red',margin:15,fontSize:17}}> {I18n.t('block') }
          <Text style={{color:'grey',fontSize:13}}>  {this.state.name} </Text>
          </Text>


           <BigTextInput
           placeholder="Enter reason for blocking.. (Optional)"
           keyboardType="default"
           focus={this.onChangeInputFocus}
           inputRef={ref => this.reason = ref}
           ref={ref => this.reasonInput = ref}
           style={{width:'90%',marginTop:0}}
           onSubmitEditing={this._blockUser}
           returnKeyType="done"
         />

               <ButtonComponent 
              style={{width : dimensions.SCREEN_WIDTH * 0.8}}
               handler={this._blockUser}
              label ={'block'}/>


           </KeyboardAwareScrollView>
         

           
           { this.state.loading_status && <ProgressBar/> }
			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(BlockUser);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
 
  
}
)



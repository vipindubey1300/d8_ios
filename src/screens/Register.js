import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header,CheckBox} from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/FontAwesome'
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomDropDown from '../components/CustomDropDown';
import {getFCMToken} from '../config/DeviceTokenUtils';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {  colors,urls,dimensions } from '../Constants';
import {showMessage} from '../config/snackmsg';
import I18n from '../i18n';
import appleAuth, {
  AppleButton,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
  AppleAuthCredentialState,
} from '@invertase/react-native-apple-authentication';
const d = Dimensions.get("window")


GoogleSignin.
configure({webClientId: '794693261364-5ve8uiuo9oicgfce0c594tgdm8olf88b.apps.googleusercontent.com'});



class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            checked:false,
            countries:[],
            states:[],
            cities:[],
            deviceToken:'',
            dummyError:false,
           
        }

    }

    _fetchCountries = async () =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/countries'
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              var temp_arr=[]
                              
                              var country = responseJson.result
                              country.map(item=>{
                                Object.assign(item,{value:item._id,label:item.name,color:'black'})
                              })
                              this.setState({countries:country})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchStates = async (countryId) =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/states?country_id=' + countryId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var state = responseJson.result
                                state.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                                state.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({states:state})
                              }
                              else{
                                showMessage('No State Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchCities = async (stateId) =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/cities?state_id=' + stateId
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              if(responseJson.result.length > 0){
                                var temp_arr=[]
                              
                                var city = responseJson.result
                                city.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                                city.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})
                                })
                                this.setState({cities:city})
                              }
                              else{
                                showMessage('No City Found')
                              }
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }


    _isValid(){
      const name = this.userNameInput.getInputValue();
      const email = this.emailInput.getInputValue();
      const confirmPassword = this.confirmPasswordInput.getInputValue();
      const password = this.passwordInput.getInputValue();
      const country = this.countryDropDown.getSelectedValue()
      const state = this.stateDropDown.getSelectedValue()
      const city = this.cityDropDown.getSelectedValue()
    
     // var isnum = /^\d+$/.test(mobile);
    
      if(name.trim().length == 0){
        showMessage('Enter User Name')
          //this.userNameInput.setState({errorState:true,errorText:'Enter User Name'});
         return false
      }
      else if(email.trim().length == 0){
        showMessage('Enter Email')
        return false
    
      }
      else if(!this.validateEmail(email)){
        showMessage('Enter Valid Email')
        return false
    
      }
      else if(password.trim().length == 0){
        showMessage('Enter Password')
        return false
    
      }
      else if (password.trim().length < 8 || password.trim().length > 16) {
        showMessage('Password should be 8-16 characters long')
        return false;
      }
      else if(confirmPassword.trim().length == 0){
        showMessage('Enter Confirm Password')
        return false
    
      }
      else if (confirmPassword.trim().length < 8 || confirmPassword.trim().length > 16) {
        showMessage('Confirm Password should be 8-16 characters long')
        return false;
      }
      else if (confirmPassword.trim() !== password.trim()) {
        showMessage('Confirm Password and Password Should match.')
        return false;
      }
      
     
      else if(country.selected_value == null){
        showMessage('Select Country')
        return false
    
      }
      else if(state.selected_value == null){
        showMessage('Select State')
        return false
    
      }
      else if(city.selected_value == null){
        showMessage('Select City')
        return false
    
      }
      else if(!this.state.checked){
        showMessage('Accept Terms & Services.')
        return false
    
      }
    
      else{
        return true;
      }
    
    
    }


componentWillMount(){
  getFCMToken().then(response => {  
    this.setState({
      deviceToken : response
    },()=>{
      console.log("Login FCM TOKEN----------",this.state.deviceToken)
    })

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
   
});

Icon.loadFont();
this._fetchCountries()
}


_google =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('google_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)
  console.log("FFF",JSON.stringify(formData))
         let url = urls.BASE_URL +'api/gmail_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))



              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}

_googleSignIn = async () => {

   try {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
     const userInfo = await GoogleSignin.signIn();
     var obj ={
       "name" : userInfo.user.name,
       "email" : userInfo.user.email,
       "id":userInfo.user.id
     }
    //  console.log("GMAIL...",JSON.stringify(obj))
    //alert(JSON.stringify(obj))
     
     this._google(userInfo.user.id,userInfo.user.name,userInfo.user.email)
 
   } catch (error) {
     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
         // user cancelled the login flow
         Alert.alert("user cancelled the login flow")
     } else if (error.code === statusCodes.IN_PROGRESS) {
         // operation (f.e. sign in) is in progress already
         Alert.alert("f.e. sign in) is in progress already")
     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
         // play services not available or outdated
 
         Alert.alert("play services not available or outdatedy")
     } else {
         // some other error happened
        ToastAndroid.show(JSON.stringify(error),ToastAndroid.LONG)
        // Alert.alert(JSON.stringify(error))
     }
   }
 };

 _fb =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('facebook_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

         let url = urls.BASE_URL +'api/facebook_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))



              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage(error.message)
             });
}
 _fbSignIn(){



  LoginManager.logInWithPermissions(['public_profile', 'email']).then(
    function (result) {

      if (result.isCancelled) {
      

        Platform.OS === 'android'
        ?  ToastAndroid.show('Login cancelled ', ToastAndroid.SHORT)
        : Alert.alert('Login cancelled ')


      } else {
          AccessToken.getCurrentAccessToken().then(
         (data) => {
           let accessToken = data.accessToken
           // Alert.alert(accessToken.toString())

           const responseInfoCallback = (error, result) => {
             if (error) {
              Platform.OS === 'android'
              ?  ToastAndroid.show('Error ', ToastAndroid.SHORT)
              : Alert.alert('Error ')

               //ToastAndroid.show('Error  fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
             } else {
               //console.log(result)

                //when success this will call
               // ToastAndroid.show('Success fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
               //Alert.alert(JSON.stringify(result))

               let id = result["id"]
               let name = result["name"]
               let email = result["email"]
               let first_name = result["first_name"]
               let last_name = result["last_name"]

               this._fb(id,name,email)

                 
             }
           }

           const infoRequest = new GraphRequest(
             '/me',
             {
               accessToken: accessToken,
               parameters: {
                 fields: {
                   string: 'email,name,id,first_name,last_name'
                 }
               }
             },
             responseInfoCallback
           );

           // Start the graph request.
           let a = new GraphRequestManager().addRequest(infoRequest).start()
          // ToastAndroid.show("Result ..."+ a.toString(),ToastAndroid.LONG)
          console.log("faceook REsult" , a)

         }
       )
      }

}.bind(this),
function (error) {
console.log('Login fail with error: ' + error)
  Platform.OS === 'android'
  ?  ToastAndroid.show("Login fail with error!" ,ToastAndroid.SHORT)
  : Alert.alert("Login fail with error!" )
  //ToastAndroid.show('Login fail with error: ' + JSON.stringify(error),ToastAndroid.LONG)
}
)
}

_apple =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('apple_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

         let url = urls.BASE_URL +'api/apple_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))

              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}

appleSignIn = async () => {
  console.log('Beginning Apple Authentication');

  // start a login request
  try {
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [
        AppleAuthRequestScope.EMAIL,
        AppleAuthRequestScope.FULL_NAME,
      ],
    });

    //console.log('appleAuthRequestResponse', appleAuthRequestResponse);
    var name = appleAuthRequestResponse.fullName.givenName +' ' +appleAuthRequestResponse.fullName.familyName
    var email = appleAuthRequestResponse.email
    var id = appleAuthRequestResponse.user

    this._apple(id,name,email)

   
    console.log(`Apple Authentication Completed, ${name}, ${email}`);
  } catch (error) {
    //console.error(error);
    showMessage('Error !')
   // Alert.alert(error.message)
  }
};

_handler = (post_id) =>{
  
  let tempObject = {
    'user_id' : 'user_id',
    }
    this.props.navigation.navigate("SelectGender",{result:tempObject})
 }

 validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
  }


  _onRegister = () =>{
    if(this._isValid()){
      const name = this.userNameInput.getInputValue();
    const email = this.emailInput.getInputValue();
    const confirmPassword = this.confirmPasswordInput.getInputValue();
    const password = this.passwordInput.getInputValue();
    const country = this.countryDropDown.getSelectedValue()
    const state = this.stateDropDown.getSelectedValue()
    const city = this.cityDropDown.getSelectedValue()

  
  
      this.setState({loading_status:true})
      var formData = new FormData();
    
     formData.append('user_name',name);
     formData.append('email',email);
     formData.append('password', password);
     formData.append('confirm_password', confirmPassword);
     formData.append('country_id', country.selected_value);
     formData.append('state_id', state.selected_value);
     formData.append('city_id', city.selected_value);
     formData.append('device_token', this.state.deviceToken);
     Platform.OS =='android'
     ?  formData.append('device_type',1)
     : formData.append('device_type',2)
  
  
       console.log("FFF",JSON.stringify(formData))
  
  
             let url = urls.BASE_URL +'api/register'
            //  console.log("FFF",JSON.stringify(url))
             fetch(url, {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'multipart/form-data',
             },
             body: formData
            }).then((response) => response.json())
                 .then(async (responseJson) => {
                     this.setState({loading_status:false})
  
                     console.log("FFF",JSON.stringify(responseJson))
  
  
                  if (responseJson.status){
  
                    var  user_id = responseJson.user._id
                    var name = responseJson.user.display_name
                    var email = responseJson.user.email
                    var image = responseJson.user.user_image
  
                       
                              //means profile was not created

                              let tempObject = {
                                'user_id' : user_id,
                              }
                              this.props.navigation.navigate("SelectGender",{result:tempObject})
                            
                     }else{

                      showMessage(responseJson.message)
                      
                       }
                 }).catch((error) => {
                           this.setState({loading_status:false})
                           showMessage('Try Again.')
  
                 });
    }
  
   }


   onSelectCountry =(countryId)=>{
     // console.log(countryId)
     if(countryId){
       this.setState({states:[],cities:[]})
       this._fetchStates(countryId)
     }
   }

   onSelectState =(stateId)=>{
    // console.log(countryId)
    if(stateId){
      this.setState({cities:[]})
      this._fetchCities(stateId)
    }
  }




   
    render() {
      const placeholder = {
        label: 'Select country...',
        value: null,
        color: colors.COLOR_PRIMARY,
        
      };

        return (
          
          <View style={styles.safeAreaContainer}>
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

         leftComponent={<TouchableOpacity onPress={()=>{

          this.props.navigation.goBack()
        }}>
          <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

          </TouchableOpacity>}


        


         statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
        // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
        // outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
         containerStyle={{
          borderBottomColor: colors.COLOR_SECONDARY,
           backgroundColor: colors.COLOR_SECONDARY,
          
               }}
              />
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
          
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
            
             // enableAutomaticScroll={(Platform.OS === 'ios')}
             >

              <View style={styles.container}>
            

              <View style={{height:dimensions.SCREEN_HEIGHT * 0.2,width:'100%',alignItems:'center',justifyContent:'center'}}>
              <Image 
              source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>
              </View>
           

                


                          {/* text fields */}

                        <CustomTextInput
                          placeholder={I18n.t('app_user_name')}
                          keyboardType="default"
                         
                          inputImage = {require('../assets/user.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.email.focus()}
                          inputRef={ref => this.username = ref}
                          ref={ref => this.userNameInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />
                        <CustomTextInput
                          placeholder={I18n.t('email')}
                          keyboardType="default"
                         
                          inputImage = {require('../assets/mail-text.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.password.focus()}
                          inputRef={ref => this.email = ref}
                          ref={ref => this.emailInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />

                        <CustomTextInput
                          placeholder={I18n.t('password')}
                          keyboardType="default"
                          secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.confirmpassword.focus()}
                          inputRef={ref => this.password = ref}
                          ref={ref => this.passwordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />
                        <CustomTextInput
                          placeholder={I18n.t('confirm_password')}
                          keyboardType="default"
                          secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          focus={this.onChangeInputFocus}
                          //onSubmitEditing={()=> this.email.focus()}
                          inputRef={ref => this.confirmpassword = ref}
                          ref={ref => this.confirmPasswordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />

                        <CustomDropDown
                        style={{marginTop:5}}
                        data={this.state.countries}
                        error={this.state.dummyError}
                        placeholder={{
                          label: I18n.t('country'),
                          value: null,
                          color: colors.COLOR_PRIMARY,
                          
                        }}
                        onSelectValue = {this.onSelectCountry}
                        ref={ref => this.countryDropDown = ref}
                        inputImage={require('../assets/location-text.png')}
                        />


                        <CustomDropDown
                        style={{marginTop:10}}
                        data={this.state.states}
                        placeholder={{
                          label: I18n.t('state'),
                          value: null,
                          color: colors.COLOR_PRIMARY,
                        }}
                        onSelectValue = {this.onSelectState}
                        ref={ref => this.stateDropDown = ref}
                        inputImage={require('../assets/location-text.png')}
                        />

                        <CustomDropDown
                        style={{marginTop:10}}
                        data={this.state.cities}
                        placeholder={{
                          label:I18n.t('city'),
                          value: null,
                          color: colors.COLOR_PRIMARY,
                        }}
                        ref={ref => this.cityDropDown = ref}
                        inputImage={require('../assets/location-text.png')}
                        />


                          {/* text fields end */}
  



                            {/* forgot password */}

                            <View style={
                              { flexDirection: 'row',alignItems:'center',marginBottom:-20}
                              }>
                              <CheckBox
                              center
                              checkedColor={colors.WHITE}
                              onPress={() => this.setState({ checked: !this.state.checked })}
                              checked={this.state.checked}
                            />
                              <Text onPress={()=> this.props.navigation.navigate('TermsCondition')}
                              style={{color:'white',textAlign:'center',marginLeft:-15}}>{I18n.t('accept_terms_and_conditions')}</Text>
                          </View>

                          

                             {/* forgot password  end*/}

                             <ButtonComponent 
                             style={{width : d.width * 0.8}}
                              handler={this._onRegister}
                             label ={I18n.t('register')}/>



                              <Text onPress={()=>{
                                this.countryDropDown.setState({errorState:true})
                                this.setState({dummyError : !this.state.dummyError})
                              }}
                              style={{color:'white',marginBottom:-10}}>{I18n.t('register_with_social_account')}</Text> 

                             {/* social icons */}

                             <View style={styles.socialContainer}>
                             <TouchableOpacity onPress={this._fbSignIn.bind(this)}>
                                  <Image
                                  style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/fb.png')}/>

                                </TouchableOpacity>

                                <TouchableOpacity>
                                  <Image
                                  style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/insta.png')}/>

                                </TouchableOpacity>

                                <TouchableOpacity onPress={this._googleSignIn.bind(this)}>
                                  <Image
                                   style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/google.png')}/>

                                </TouchableOpacity>

                              </View>

                                {/* social icons end */}

                                {
                                  Platform.OS === 'ios'
                                  ?
                                
                                  <AppleButton
                                    style={{ height: 40, width: 180 ,marginBottom:20,marginTop:-10}}
                                    buttonStyle={AppleButton.Style.WHITE}
                                    buttonType={AppleButton.Type.SIGN_IN}
                                    onPress={() => this.appleSignIn()}
                                  />
                               
                                : null
                                }


                                <Text style={{color:'white',marginTop:-10,marginBottom:0}}>{I18n.t('already_have_an_account')}
                                <Text onPress={()=> this.props.navigation.navigate("Login")}
                                style={{color:'white',fontWeight:'bold',marginLeft:5}}> {I18n.t('sign_in_now')}</Text>
                                </Text> 
                                


                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           { this.state.loading_status && <ProgressBar/> }


			   

          
          </View>

        )
    }
}

const mapDispatchToProps = dispatch => {
  return {
      add: (userinfo) => dispatch(addUser(userinfo)),
  }
}
export default connect(null, mapDispatchToProps)(Register);



let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 1.22,
    alignItems:'center',
    backgroundColor:colors.BLACK,
     alignItems:'center',
   
    
    
  },
  logo_image:{
    height:"80%", 
    width:'50%',
    marginBottom:20
  
   
    
  },
  socialImage:{
    height:40,
    width:40,
    flex:1,
    margin:6
  },
  socialContainer:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    margin:10,
    padding:7,
    height:80
    
  },
  viewStyle:{
    borderRadius:30,
    borderWidth:2,
    borderColor:colors.LIGHT_MAROON,
    //height:45,
    width:Dimensions.get('window').width * 0.85,
    backgroundColor:colors.DARK_MAROON,
    flexDirection:'row',
    alignContent:'center',
    textAlign:"center"
  }
 

}
)

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    backgroundColor:"transparent",
    fontSize: 16,
    width:Dimensions.get('window').width * 0.72,
    color: 'white',
    alignItems:"center",
    height:40,
    textAlign:"center",
    justifyContent:"center",
    alignSelf:"center",
    alignItems:"center",
    marginLeft: dimensions.SCREEN_WIDTH * 0.1,
  },
  inputAndroid: {
    height:40,
    fontSize: 16,
    width: dimensions.SCREEN_WIDTH * 0.72,
    backgroundColor:"transparent",
   marginLeft: dimensions.SCREEN_WIDTH * 0.28,
    color: 'white',
    alignItems:"center",
    textAlign:"center",
    justifyContent:"center",
    alignSelf:"center",
   
  },
});



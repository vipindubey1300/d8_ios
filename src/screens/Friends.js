import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';

import I18n from '../i18n';


const d = Dimensions.get("window")

class Friends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          friends:[]

        }

    }

    _getFriends = async () =>{

      var formData = new FormData();

      formData.append('user_id',this.props.user.user_id);

        this.setState({loading_status:true})

        console.log("----",formData)

               let url = urls.BASE_URL +'api/get_friends'
                    fetch(url, {
                    method: 'POST',
                    body:formData

                    }).then((response) => response.json())
                        .then((responseJson) => {


                          this.setState({loading_status:false})

                          if(responseJson.status){
                           // showMessage(responseJson.message,false)
                            this.setState({friends:responseJson.friends})
                          }
                          else{
                            //showMessage(responseJson.message)
                          }


                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });



    }


    componentWillMount(){
        this._getFriends()
        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });  
    }

    _next(id,index){


      let obj={
       'user_id':id
       }
       this.props.navigation.navigate("UserProfile",{result:obj})


 }


    renderItem = ({item, index}) => {
      console.log(item)
      return(
        <TouchableOpacity onPress={()=> this._next(item.user_two._id)}>
        <View style={
            {padding:15,
            backgroundColor:colors.LIGHT_YELLOW,
            width:dimensions.SCREEN_WIDTH * 0.93,
            margin:5,height:null,
            flexDirection:'row',justifyContent:'flex-start',alignItems:'center',borderRadius:5,alignSelf:'center'}}>

            <Image source={{uri:urls.BASE_URL + item.user_two.user_image}}
            overflow='hidden'
            style={styles.user_image} resizeMode='cover'/>

            <View style={{marginLeft:10}}>
            <Text style={{color:'black',fontSize:14,fontWeight:'900',textTransform:'capitalize'}}>{item.user_two.display_name}</Text>
            <Text style={{color:'grey',fontSize:12,fontWeight:'900'}}>{item.user_two.country.name}</Text>

            </View>
       </View>
        </TouchableOpacity>
      )


    }

    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('friends'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,


            }}
             />

             <View style={{flex:1,backgroundColor:colors.BLACK,width:'100%'}}>
             <TouchableOpacity onPress={()=> this.props.navigation.navigate("PendingFriends")}>
             <View style={{
                    paddingLeft:10,paddingRight:10,
                    paddingTop:6,
                    paddingBottom:6,backgroundColor:colors.COLOR_PRIMARY,
                    width:'auto',borderRadius:10,
                    margin:15,justifyContent:'center',alignItems:'center',

             }}>
                    <Text style={{color:colors.BLACK}}>{I18n.t('see_pending_requests')}</Text>
             </View>
             </TouchableOpacity>

             {
              this.state.friends.length > 0
              ?


              <FlatList
              style={{marginBottom:10,width:'100%',padding:4}}
              data={this.state.friends}
             // ItemSeparatorComponent = { this.FlatListItemSeparator }
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
              keyExtractor={item => item._id}
               // Performance settings
             removeClippedSubviews={true} // Unmount components when outside of window
             initialNumToRender={2} // Reduce initial render amount
             maxToRenderPerBatch={1} // Reduce number in each render batch
             maxToRenderPerBatch={100} // Increase time between renders
             windowSize={7} // Reduce the window size
            />
             :

             this.state.loading_status ?
             null
             :


                 <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
             <Image source={require('../assets/no-friends.png')}
               style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.16}} resizeMode='contain'/>
               </View>


            }






           </View>


           { this.state.loading_status && <ProgressBar/> }



          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),



  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Friends);

let styles = StyleSheet.create({
  safeAreaContainer:{


    flex:1

  },
  container:{


    height: '100%',


  },
  user_image:{
    height:50,
    width:50,

    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15

  },

}
)

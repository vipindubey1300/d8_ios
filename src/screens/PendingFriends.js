import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
import RBSheet from "react-native-raw-bottom-sheet";

import I18n from '../i18n';


const d = Dimensions.get("window")

class PendingFriends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          friends:[],
          friend_name_id:null,
          friend_id:null,
          friend_name_image:'',
          friend_name_name:'',
           
        }

    }

    _getFriends = async () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
        this.setState({loading_status:true})
    
     console.log(formData)
    
               let url = urls.BASE_URL +'api/get_pending_friends'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                            console.log("----",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           // showMessage(responseJson.message,false)
                            this.setState({friends:responseJson.friends})
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });
    
    
    
    }


     _cancelFriendRequest = async (_id,friendshipId) =>{
         
        var formData = new FormData();
      
        formData.append('from_id',this.props.user.user_id);
        formData.append('to_id',_id);

       
          this.setState({loading_status:true})
      
       console.log(formData)
      
                 let url = urls.BASE_URL +'api/reject_friend_request'
                      fetch(url, {
                      method: 'POST',
                      body:formData
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                              console.log("----",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                            showMessage(responseJson.message,false)
                             this.setState(prevState => ({
                                friends: prevState.friends.filter(friend => friend._id != friendshipId) 
                              
                              }));
                            }
                            else{
                              showMessage(responseJson.message)
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                            showMessage(error.message)
                          });
      
      
      
      }

      _acceptFriendRequest = async (_id,friendshipId) =>{
         
        var formData = new FormData();
      
        formData.append('from_id',this.props.user.user_id);
        formData.append('to_id',this.state.friend_name_id);

       
          this.setState({loading_status:true})
      
       console.log(formData)
      
                 let url = urls.BASE_URL +'api/accept_friend_request'
                      fetch(url, {
                      method: 'POST',
                      body:formData
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                              console.log("----",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                                showMessage(responseJson.message,false)
                              this.setState(prevState => ({
                                  friends: prevState.friends.filter(friend => friend._id != this.state.friend_id) 
                              
                              }));

                              if(responseJson.status){
                                showMessage(responseJson.message,false)
                                this.setState(prevState => ({
                                  friends: prevState.friends.filter(friend => friend._id != friendshipId) 
                                
                                }));

                              let obj={
                                'to_id':this.state.friend_name_id,
                                'to_name':this.state.friend_name_name,
                                'to_image':this.state.friend_name_image,
                                'to_status':'offline',
                                'to_typing':false
                              }
                              this.props.navigation.navigate("Chats",{result:obj})


                            }
                            else{
                              showMessage(responseJson.message)
                            }
                            
                           
                        }
                      }).catch((error) => {
                            this.setState({loading_status:false})
  
                            showMessage(error.message)
                          });
      
      
      
      }


    
      

      _rejectFriendRequest = async (with_chat=false) =>{
         
        var formData = new FormData();
      
        formData.append('from_id',this.props.user.user_id);
        formData.append('to_id',this.state.friend_name_id);
        with_chat ? formData.append('with_chat',true) : null
       
        this.setState({loading_status:true})
      
       console.log(formData)
      
                 let url = urls.BASE_URL +'api/reject_friend_request'
                      fetch(url, {
                      method: 'POST',
                      body:formData
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                              console.log("----",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                           showMessage(responseJson.message,false)
                             
                              if(with_chat){
                                  //means user cancelled request but want to chat firest
                                  let obj={
                                    'to_id':this.state.friend_name_id,
                                    'to_name':this.state.friend_name_name,
                                    'to_image':this.state.friend_name_image,
                                    'to_status':'offline',
                                    'to_typing':false
                                  }
                                  this.props.navigation.navigate("Chats",{result:obj})
    

                              }
                              else{
                                this.setState(prevState => ({
                                  friends: prevState.friends.filter(friend => friend._id != this.state.friend_id) 
                                
                                }));
                              }
                            }
                            else{
                              showMessage(responseJson.message)
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                            showMessage(error.message)
                          });
      
      
      
      }


        componentWillMount(){
            this._getFriends()
            AsyncStorage.getItem('lang')
            .then((item) => {
                      if (item) {
                        I18n.locale = item.toString()
                    }
                    else {
                         I18n.locale = 'en'
                      }
            });  
        }


        _next(id){
          let obj={
           'user_id':id
           }
           this.props.navigation.navigate("UserProfile",{result:obj})
  
  
     }

    renderItem = ({item, index}) => {
      
      return(
        <TouchableOpacity onPress = {()=> this._next(item.user_two._id)}>
        <View style={
            {padding:15,
            backgroundColor:colors.LIGHT_YELLOW,
            width:dimensions.SCREEN_WIDTH * 0.93,
            margin:5,height:null,
            flexDirection:'row',justifyContent:'space-between',alignItems:'center',borderRadius:5,alignSelf:'center'}}>

            <View style={{flexDirection:'row',alignItems:'center'}}>

                        <Image source={{uri:urls.BASE_URL + item.user_two.user_image}} 
                        overflow='hidden'
                        style={styles.user_image} resizeMode='cover'/>
                        
                        <View style={{marginLeft:5}}>
                        <Text style={{color:'black',fontSize:14,fontWeight:'900',textTransform:'capitalize'}}>{item.user_two.display_name}</Text>
                        <Text style={{color:'grey',fontSize:12,fontWeight:'900'}}>{item.user_two.country.name}</Text>
                        
                        </View>
            </View>


            {
                item.status == 1
                ?
                <View style={{flexDirection:'row',alignItems:'center'}}>
                

                <TouchableOpacity style={{paddingTop:4,
                    paddingBottom:4,
                    paddingLeft:7,
                    paddingRight:7,
                    backgroundColor:colors.COLOR_PRIMARY,
                borderRadius:5,
                borderWidth:2,
                justifyContent:'center',
                alignItems:'center'}}
                onPress={()=> this._cancelFriendRequest(item.user_two._id,item._id)}>
                    <Text style={{color:colors.BLACK,fontWeight:'bold',fontSize:13}}>Cancel</Text>
                </TouchableOpacity>
            
                </View>
            : null
            }

            {
                item.status == 2
                ?
                <View style={{flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity onPress={()=> {
                    this.setState({friend_name_id : item.user_two._id , friend_id: item._id,
                      friend_name_name: item.user_two.display_name, friend_name_image:item.user_two.image},()=>{
                      // AsyncStorage.getItem('accept').
                      // then(value => {
                      //   console.log('_________________________',value)
                      //   if(value) this._acceptFriendRequest()
                      //   else this.RBSheet.open();
                      // })
                        this.RBSheet.open()
                    })
                }}>
                <Image source={require('../assets/menu-req.png')} style={{width:40,height:40}} resizeMode='contain'/>
                </TouchableOpacity>

              
            
                </View>
                 : null
            }


            
       </View>
        </TouchableOpacity>
      )
     

    }

    render() {

        const AcceptSheet = () =>{
            return(
              <RBSheet
              ref={ref => {
                this.RBSheet = ref;
              }}
             height={250}
              closeOnDragDown
              onClose={()=>{
                this.setState({
                  friend_name_id:null,
                  friend_id:null,
                  friend_name_image:'',
                  friend_name_name:'',
                },()=>{
                  console.log(JSON.stringify(this.state))
                })
              }}
              duration={250}
              customStyles={{
                container: {
                
                  alignItems: "center",
                  backgroundColor:colors.BLACK,
                  padding:10,
                  borderTopLeftRadius:15,
                borderTopRightRadius:15
                }
              }}
              animationType='fade'
              minClosingHeight={10}
            >
             <Text 
             style={{color:colors.COLOR_PRIMARY,textAlign:'center',fontWeight:'bold'}}>
             {I18n.t('accept_text')}
            </Text>
    
              {/** bottom three buttons */}
    
              <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',width:'100%',marginTop:20}}>
                  <TouchableOpacity onPress={()=> {
                    this._acceptFriendRequest()
                  }}>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'green',fontWeight:'bold',fontSize:9}}>{I18n.t('accept')}</Text>
                  <Image source={require('../assets/mail-green.png')} style={{width:50,height:50}} 
                      resizeMode='contain'/>
                  </View>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=> {
                        this._rejectFriendRequest(true)
                   // console.log(JSON.stringify(this.state))
                  }}>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:9}}>{I18n.t('chat_first')}</Text>
                  <Image source={require('../assets/chat-heart.png')} style={{width:50,height:50}} 
                      resizeMode='contain'/>
                  </View>
                  </TouchableOpacity>
    

    
    
    
                  <TouchableOpacity  onPress={()=> {
                  this._rejectFriendRequest()
                  }}>
                  <View style={{justifyContent:'center',alignItems:'center'}}>
                  <Text style={{color:'red',fontWeight:'bold',fontSize:9,textAlign:'center'}}>{I18n.t('nope')}</Text>
                 <Image source={require('../assets/mail-red.png')} style={{width:50,height:50}} 
                  resizeMode='contain'/>
                  </View>
                  </TouchableOpacity>
              </View>
    
               {/** bottom three buttons  end*/}
    
    
            </RBSheet>
            )
          }


        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
           <AcceptSheet/>
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text:I18n.t('speed_d8_requests'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
             />

             <View style={{flex:1,backgroundColor:colors.BLACK,width:'100%'}}>
            

             {
              this.state.friends.length > 0
              ?
 
                   
              <FlatList
              style={{marginBottom:10,width:'100%',padding:4}}
              data={this.state.friends}
             // ItemSeparatorComponent = { this.FlatListItemSeparator }
              showsVerticalScrollIndicator={false}
              renderItem={(item,index) => this.renderItem(item,index)}
              keyExtractor={item => item._id}
               // Performance settings
             removeClippedSubviews={true} // Unmount components when outside of window
             initialNumToRender={2} // Reduce initial render amount
             maxToRenderPerBatch={1} // Reduce number in each render batch
             maxToRenderPerBatch={100} // Increase time between renders
             windowSize={7} // Reduce the window size
            />
             : 

             this.state.loading_status ?
             null
             :

 
                 <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
             <Image source={require('../assets/pending-requests.png')} 
               style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.16}} resizeMode='contain'/>
               </View>
                 
 
            }
              
                  

            


           </View>
         
       
           { this.state.loading_status && <ProgressBar/> }
             
           
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PendingFriends);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
 
}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,
  StyleSheet, TouchableOpacity, SafeAreaView,Alert,Platform,Keyboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from "react-native-fbsdk";
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import Snackbar from 'react-native-snackbar';
import I18n from '../i18n';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import {getFCMToken} from '../config/DeviceTokenUtils';

import {  colors,urls,dimensions } from '../Constants';
import {showMessage} from '../config/snackmsg';
import appleAuth, {
  AppleButton,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
  AppleAuthCredentialState,
} from '@invertase/react-native-apple-authentication';

const d = Dimensions.get("window")

GoogleSignin.
configure({webClientId: '794693261364-5ve8uiuo9oicgfce0c594tgdm8olf88b.apps.googleusercontent.com'});





class Login extends React.Component {
    constructor(props) {
        super(props);
       
        this.state={
          loading_status:false,
          deviceToken:''
        }

    }

    componentDidMount() {
      /**
       * subscribe to credential updates.This returns a function which can be used to remove the event listener
       * when the component unmounts.
       */
    
    }

    componentWillUnmount() {
      /**
       * cleans up event listener
       */
     // this.authCredentialListener();
    }

  componentWillMount(){
    

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  

    
      getFCMToken().then(response => {  
        this.setState({
          deviceToken : response
        },()=>{
          console.log("Login FCM TOKEN----------",this.state.deviceToken)
        })
       
    });
    }

    async onAppleButtonPress() {
      // performs login request
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
      });
    
      // get current authentication state for user
      const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
    
      // use credentialState response to ensure the user is authenticated
      if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
        // user is authenticated
        alert(' user is authenticated')
      }
    }
  
_google =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('google_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)
  console.log("FFF",JSON.stringify(formData))
         let url = urls.BASE_URL +'api/gmail_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))



              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}

_googleSignIn = async () => {

   try {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
     const userInfo = await GoogleSignin.signIn();
     var obj ={
       "name" : userInfo.user.name,
       "email" : userInfo.user.email,
       "id":userInfo.user.id
     }
    //  console.log("GMAIL...",JSON.stringify(obj))
    //alert(JSON.stringify(obj))
     
     this._google(userInfo.user.id,userInfo.user.name,userInfo.user.email)
 
   } catch (error) {
     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
         // user cancelled the login flow
         Alert.alert("user cancelled the login flow")
     } else if (error.code === statusCodes.IN_PROGRESS) {
         // operation (f.e. sign in) is in progress already
         Alert.alert("f.e. sign in) is in progress already")
     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
         // play services not available or outdated
 
         Alert.alert("play services not available or outdatedy")
     } else {
         // some other error happened
        ToastAndroid.show(JSON.stringify(error),ToastAndroid.LONG)
        // Alert.alert(JSON.stringify(error))
     }
   }
 };

 _fb =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('facebook_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

         let url = urls.BASE_URL +'api/facebook_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))



              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}
 _fbSignIn(){



  LoginManager.logInWithPermissions(['public_profile', 'email']).then(
    function (result) {

      if (result.isCancelled) {
      

        Platform.OS === 'android'
        ?  ToastAndroid.show('Login cancelled ', ToastAndroid.SHORT)
        : Alert.alert('Login cancelled ')


      } else {
          AccessToken.getCurrentAccessToken().then(
         (data) => {
           let accessToken = data.accessToken
           // Alert.alert(accessToken.toString())

           const responseInfoCallback = (error, result) => {
             if (error) {
              Platform.OS === 'android'
              ?  ToastAndroid.show('Error ', ToastAndroid.SHORT)
              : Alert.alert('Error ')

               //ToastAndroid.show('Error  fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
             } else {
               //console.log(result)

                //when success this will call
               // ToastAndroid.show('Success fetching data: ' + JSON.stringify(result) ,ToastAndroid.LONG)
               //Alert.alert(JSON.stringify(result))

               let id = result["id"]
               let name = result["name"]
               let email = result["email"]
               let first_name = result["first_name"]
               let last_name = result["last_name"]

               this._fb(id,name,email)

                 
             }
           }

           const infoRequest = new GraphRequest(
             '/me',
             {
               accessToken: accessToken,
               parameters: {
                 fields: {
                   string: 'email,name,id,first_name,last_name'
                 }
               }
             },
             responseInfoCallback
           );

           // Start the graph request.
           let a = new GraphRequestManager().addRequest(infoRequest).start()
          // ToastAndroid.show("Result ..."+ a.toString(),ToastAndroid.LONG)
          console.log("faceook REsult" , a)

         }
       )
      }

}.bind(this),
function (error) {
console.log('Login fail with error: ' + error)
  Platform.OS === 'android'
  ?  ToastAndroid.show("Login fail with error!" ,ToastAndroid.SHORT)
  : Alert.alert("Login fail with error!" )
  //ToastAndroid.show('Login fail with error: ' + JSON.stringify(error),ToastAndroid.LONG)
}
)
}

_apple =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('apple_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

         let url = urls.BASE_URL +'api/apple_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))

              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country === null || country == 'null'){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name === null || name == 'null'){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}

appleSignIn = async () => {
  console.log('Beginning Apple Authentication');

  // start a login request
  try {
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [
        AppleAuthRequestScope.EMAIL,
        AppleAuthRequestScope.FULL_NAME,
      ],
    });

    //console.log('appleAuthRequestResponse', appleAuthRequestResponse);
    var name = appleAuthRequestResponse.fullName.givenName +' ' +appleAuthRequestResponse.fullName.familyName
    var email = appleAuthRequestResponse.email
    var id = appleAuthRequestResponse.user

    this._apple(id,name,email)

   
    console.log(`Apple Authentication Completed, ${name}, ${email}`);
  } catch (error) {
    //console.error(error);
    showMessage('Error !')
    //Alert.alert(JSON.stringify(error))
  }
};


_insta =(id,name,email) =>{
  this.setState({loading_status:true})
  var formData = new FormData();

  formData.append('instagram_id', id);
  formData.append('email', email);
  formData.append('name',name)
  formData.append('device_token', this.state.deviceToken);
  Platform.OS =='android'
  ?  formData.append('device_type',1)
  : formData.append('device_type',2)

         let url = urls.BASE_URL +'api/instagram_login'
       
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

              console.log("FFF",JSON.stringify(responseJson))



              showMessage(responseJson.message)
              if (responseJson.status){

                var  user_id = responseJson.user._id.toString()
                var name = responseJson.user.display_name
                var email = responseJson.user.email
                var image = responseJson.user.user_image
                var country = responseJson.user.country

                if(country == null){
                  //means first time
                  let tempObject = {
                    'user_id' : user_id,
                  }
                  this.props.navigation.navigate("UpdateCountry",{result:tempObject})
                  return
                }
                else if(name == null){
                    //means profile was not created
                    let tempObject = {
                      'user_id' : user_id,
                    }
                    this.props.navigation.navigate("SelectGender",{result:tempObject})
                    return
                }
                else{

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    ]);

                   await  this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })
                    

                    this.props.navigation.navigate('Home');


                  }
                
                 }else{
                  
                  
                  
                }
             }).catch((error) => {
                       this.setState({loading_status:false})
                    
                       showMessage('Try Again')
             });
}
_instagram(token){
  fetch('https://api.instagram.com/v1/users/self/?access_token='+token, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data',
    },


  }).then((response) => response.json())
        .then((responseJson) => {
   this.setState({loading_status:false})
     ToastAndroid.show(JSON.stringify(responseJson), ToastAndroid.SHORT);


             if(responseJson.meta.code == 200){

              var id = responseJson.data.id
              var username = responseJson.data.username
              var full_name = responseJson.data.full_name


           
          }
          else{

            Platform.OS === 'android'
          ?   ToastAndroid.show("Error !", ToastAndroid.SHORT)
          : Alert.alert("Error !")
          }
        }).catch((error) => {
          this.setState({loading_status:false})

          Platform.OS === 'android'
          ?   ToastAndroid.show("Connection Error !", ToastAndroid.SHORT)
          : Alert.alert("Connection Error !")
        });


}



_isValid = () =>{
  const name = this.userNameInput.getInputValue();
  const password = this.passwordInput.getInputValue();
 
  if(name == '' || name.trim().length == 0){
      showMessage('Enter User Name')
    
     return false
  }
 
  else if(password == '' || password.trim().length == 0){
    showMessage('Enter Password')
    return false

  }
  else{
    return true;
  }


}






handler = (id) =>{
  
    console.log("LIKE ",id)
  //  this.props.navigation.navigate("ModelsSwiper")
  this._onLogin()
 
 }

 _onLogin = () =>{
   if(this._isValid()){
    const name = this.userNameInput.getInputValue();
    const password = this.passwordInput.getInputValue();
    Keyboard.dismiss()
  
      this.setState({loading_status:true})
      var formData = new FormData();
    
     formData.append('user_name', name);
     formData.append('password', password);
     formData.append('device_token', this.state.deviceToken);
     Platform.OS =='android'
     ?  formData.append('device_type',1)
     : formData.append('device_type',2)
  
             let url = urls.BASE_URL +'api/login'
            //  console.log("FFF",JSON.stringify(url))
             fetch(url, {
             method: 'POST',
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'multipart/form-data',
             },
             body: formData
            }).then((response) => response.json())
                 .then(async (responseJson) => {
                     this.setState({loading_status:false})
  
                  console.log("FFF",JSON.stringify(responseJson))
                  
                  if (responseJson.status){
  
                    var  user_id = responseJson.user._id.toString()
                    var name = responseJson.user.display_name
                    var email = responseJson.user.email
                    var image = responseJson.user.user_image
  
                    if(name == null){
                        //means profile was not created
                        let tempObject = {
                          'user_id' : user_id,
                        }
                        this.props.navigation.navigate("SelectGender",{result:tempObject})
                        return
                    }
                    else{
  
                      AsyncStorage.multiSet([
                        ["user_id", user_id.toString()],
                        ["email", email.toString()],
                        ["name", name.toString()],
                        ["image", image.toString()],
                        ]);
  
                       await  this.props.add({ 
                          user_id: user_id, 
                          name : name,
                          image : image,
                          email:  email 
                        })
                        
  
                        this.props.navigation.navigate('Home');
  
  
                      }
                    
                     }else{
                       showMessage(responseJson.message)
                      
                      
                    }
                 }).catch((error) => {
                           this.setState({loading_status:false})
                        
                           showMessage(error.message)
                 });
   }
  

 }

 _forgotPassword = () =>{
  this.props.navigation.navigate("ForgotPassword")


 }



fetchAndUpdateCredentialState = async () => {
  if (this.user === null) {
    this.setState({ credentialStateForUser: 'N/A' });
  } else {
    const credentialState = await appleAuth.getCredentialStateForUser(this.user);
    if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
      this.setState({ credentialStateForUser: 'AUTHORIZED' });
    } else {
      this.setState({ credentialStateForUser: credentialState });
    }
  }
}



   
    render() {
        return (
          <SafeAreaView style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />

           

          

           <KeyboardAwareScrollView
             contentContainerStyle={{
              alignItems:'center',flexGrow:1,justifyContent:'center'}}
              style={styles.container}>

              <View style={{height:dimensions.SCREEN_HEIGHT * 0.2,
                width:'100%',alignItems:'center',justifyContent:'center',marginBottom:20}}>
              <Image 
              source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>
              </View>


                          {/* text fields */}

                        <CustomTextInput
                          placeholder={I18n.t('user_name')}
                          inputImage = {require('../assets/user.png')}
                          //focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.password.focus()}
                          inputRef={ref => this.name = ref}
                          ref={ref => this.userNameInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />

                        <CustomTextInput
                          placeholder={I18n.t('password')}
                          keyboardType="default"
                          secureTextEntry={true}
                          inputImage = {require('../assets/pass.png')}
                          //focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.handler()}
                          inputRef={ref => this.password = ref}
                          ref={ref => this.passwordInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="done"
                        />


                          {/* text fields end */}





                            {/* forgot password */}

                            <View style={{width:'77%',marginTop:10}}>
                              <Text onPress={()=> this._forgotPassword()}
                              style={{color:'white',alignSelf:'flex-end'}}>{I18n.t('forgot_password')}?</Text>
                            </View>

                             {/* forgot password  end*/}

                             <ButtonComponent 
                             style={{width : d.width * 0.8}}
                              handler={this.handler}
                             label ={I18n.t('login')}/>



                              <Text style={{color:'white'}}>{I18n.t('login_with_social_account')}</Text> 

                             {/* social icons */}

                              <View style={styles.socialContainer}>
                                <TouchableOpacity onPress={this._fbSignIn.bind(this)}>
                                  <Image
                                  style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/fb.png')}/>

                                </TouchableOpacity>

                                <TouchableOpacity  onPress={()=> {}}>
                                  <Image
                                  style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/insta.png')}/>

                                </TouchableOpacity>

                                <TouchableOpacity onPress={this._googleSignIn.bind(this)}>
                                  <Image
                                   style={styles.socialImage}
                                  resizeMode='contain'
                                  source={require('../assets/google.png')}/>

                                </TouchableOpacity>

                              </View>

                                {/* social icons end */}

                                {
                                  Platform.OS === 'ios'
                                  ?
                                
                                  <AppleButton
                                    style={{ height: 40, width: 180 ,marginBottom:10}}
                                    buttonStyle={AppleButton.Style.WHITE}
                                    buttonType={AppleButton.Type.SIGN_IN}
                                    onPress={() => this.appleSignIn()}
                                  />
                               
                                : null
                                }


                                <Text onPress={()=> this.props.navigation.navigate('ChooseLanguageRegister')}
                                style={{color:'white'}}>{I18n.t('dont_have_account')} ?    
                                <Text style={{color:'white',fontWeight:'bold',marginLeft:5}}>  {I18n.t('sign_up_now')}</Text>
                                </Text> 


                             


                            


           </KeyboardAwareScrollView>
         

           { this.state.loading_status && <ProgressBar/> }


			   

          
          </SafeAreaView>

        )
    }
}

const mapDispatchToProps = dispatch => {
  return {
      add: (userinfo) => dispatch(addUser(userinfo)),
  }
}
export default connect(null, mapDispatchToProps)(Login);



let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY
   
  },
  container:{
   
    backgroundColor:colors.BLACK,
    padding:10,
    height : d.height * 1,
   
   
    
    
  },
  logo_image:{
    height:"75%", 
    width:'40%',
    
  },
  socialImage:{
    height:40,
    width:40,
    flex:1,
    margin:6
  },
  socialContainer:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    margin:10
  }
 

}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';

import I18n from '../i18n';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls ,dimensions} from '../Constants';


const d = Dimensions.get("window")

export default class SelectGender extends React.Component {
    constructor(props) {
        super(props);

    }



componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}


handler = (post_id) =>{
 
  
 }
 _selectGender(gender){
  var result  = this.props.navigation.getParam('result')

  result["interested_gender"] = gender
  this.props.navigation.navigate('CreateProfile',{result : result});

}



   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
           <Header
           statusBarProps={{ barStyle: 'light-content' }}
           barStyle="light-content" // or directly

            leftComponent={
            <TouchableOpacity onPress={()=>{

              this.props.navigation.goBack()
             }}>
           <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

           </TouchableOpacity>
           }
           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
          centerComponent={{ text: I18n.t('interested_gender'), style: { color: colors.WHITE } }}
           //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{
             //to remove bottom border
             borderBottomColor: colors.COLOR_SECONDARY,
           backgroundColor: colors.COLOR_SECONDARY,
       
       
         }}
          />

              <View style={styles.container}>
            

           

              

                 <Text style={styles.headingText}>{ I18n.t('interested_gender')} </Text>


                 <Text style={styles.titleText}> {I18n.t('please_select_interested_gender')}</Text>


                          {/* selection */}

                          <View style={{flexDirection:'row',
                          alignItems:'center',justifyContent:'center'}}>
                          <TouchableOpacity onPress={() => this._selectGender('female')}>
                          <Image
                          source={Platform.OS =='android'? require('../assets/female.png') :  require('../assets/female-ios.png')} 
                          style={styles.genderImage} resizeMode='contain'/>
                          </TouchableOpacity>

                          <TouchableOpacity onPress={() => this._selectGender('male')}>
                          <Image
                          source={Platform.OS =='android'? require('../assets/male.png') :  require('../assets/male-ios.png')} 
                          style={styles.genderImage} resizeMode='contain'/>
                          </TouchableOpacity>

                          </View>

                          {/* <TouchableOpacity>
                          <Image
                            source={require('../assets/transgender.png')} 
                            style={[styles.genderImage,{marginTop:-30}]} resizeMode='contain'/>
                          </TouchableOpacity> */}


                            {/* selection end */}

                        



                             
                </View>

               

          
          </View>

        )
    }
}

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
    padding:10,
    flex:1,
    alignItems:'center',
    backgroundColor:colors.BLACK,
    justifyContent:'center'
    
    
  },
  headingText:{
   color:'white',
   fontWeight:'bold',
   fontSize:18,
   marginBottom:15
   },
   titleText :{
     color:'white',
     
     fontSize:14,
     marginBottom:5
     },
     genderImage :{
      height:d.height * 0.35, 
      width:d.width * 0.35,
     
      
    },
  
 

}
)



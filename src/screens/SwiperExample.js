import React, { Component } from 'react'
import Swiper from '@starodubenko/react-native-deck-swiper'
import { Button, StyleSheet, Text, View } from 'react-native'
import { colors } from '../Constants'


function * range (start, end) {
  for (let i = start; i <= end; i++) {
    yield i
  }
}


const pager = [
  {
    "text": "1this is page one",
    "image": require('../assets/welcome1.png')
  },
  {
    "text": "1this is page ontwwoooooooooooooe",
    "image": require('../assets/welcome2.png')
  },
  {
    "text": "lasstttttttttt",
    "image": require('../assets/logo.png')
  },


]
export default class SwiperExample extends Component {
  constructor (props) {
    super(props)
    this.state = {
      cards: [...range(1, 50)],
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0
    }
  }

  renderCard = (card, index) => {
    console.log('renderCard',card)
    return (
      <View style={styles.card}>
        <Text style={styles.text}>{card.text}</Text>
      </View>
    )
  };

  onSwiped = (type) => {
    console.log(`on swiped ${type}`)
  }

  onSwipedAllCards = () => {
    this.setState({
      swipedAllCards: true
    })
  };
 

  swipeLeft = () => {
    this.swiper.swipeLeft()
  };

  render () {
    return (
      <View style={styles.container}>
       {/*gallery */ }
       <View style={{flexDirection:'row',alignItems:'center',margin:10}}>
       {/*gallery one */}
      {
          user_details.gallery[0] == null 
          ? null
          :  <View>

          <View style={{height:70,width:70,borderRadius:35}}>

              {
              this.state.imageTwoPhoto == null
              ? <Image source={{uri:urls.BASE_URL+user_details.gallery[0].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden'}}  />
              : <Image source={this.state.imageTwoSource} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden'}}  />
              }


          </View>

          <View style={{position:'absolute',left:45}}>
          {
            this.state.imageTwoPhoto == null
            ? <TouchableOpacity onPress={()=> this._selectImageTwo()}>
            <Image style={{width: 25, height: 25,marginTop:0}} source={require('../assets/back-circle.png')} />
            </TouchableOpacity>
            : <TouchableOpacity onPress={()=> this.setState({imageTwoPhoto:null,imageTwoSource:null})}>
            <Image style={{width: 25, height: 25,marginTop:0}} source={require('../assets/cross.png')} />
            </TouchableOpacity>
            }
               

          </View>

      </View>
      }


       {/*gallery two */}
       {
        user_details.gallery[0] == null 
        ? null
        :  <View>

        <View style={{height:70,width:70,borderRadius:35}}>

            {
            this.state.imageThreePhoto == null
            ? <Image source={{uri:urls.BASE_URL+user_details.gallery[0].image}} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden'}}  />
            : <Image source={this.state.imageThreeSource} style={{alignSelf:'center',height:'100%',width:'100%',overflow:'hidden'}}  />
            }


        </View>

        <View style={{position:'absolute',left:45}}>
        {
          this.state.imageThreePhoto == null
          ? <TouchableOpacity onPress={()=> this._selectImageThree()}>
          <Image style={{width: 25, height: 25,marginTop:0}} source={require('../assets/back-circle.png')} />
          </TouchableOpacity>
          : <TouchableOpacity onPress={()=> this.setState({imageThreePhoto:null,imageThreeSource:null})}>
          <Image style={{width: 25, height: 25,marginTop:0}} source={require('../assets/cross.png')} />
          </TouchableOpacity>
          }
             

        </View>

    </View>
    }


      <TouchableOpacity onPress={()=> {
       if( this._checkImageUpload()){
        let res = this._checkImageCount()
        this._selectImageRunTime(res)
       }
      }}>
      <Image 
       source={require('../assets/story-add.png')} style={{width:40,height:40,marginLeft:15}} resizeMode='contain'/>
       </TouchableOpacity>

      </View>

       {/*gallery  end*/}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor:'white',
    justifyContent: 'center',
    backgroundColor: colors.COLOR_PRIMARY,
  },
  text: {
    textAlign: 'center',
    fontSize: 50,
    backgroundColor: 'transparent'
  },
  done: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    backgroundColor: 'transparent'
  }
})
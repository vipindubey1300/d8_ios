import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
import I18n from '../i18n';


import * as Animatable from 'react-native-animatable';

const d = Dimensions.get("window")

class UserProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          user_details:'',
          gallery:[],
          ethnicity:'',
          body_type:'',
          country:'',
          user_id:0
           
        }

    }

    _getProfile = async (id) =>{
         
      var formData = new FormData();
    
      formData.append('user_id',id);
     
        this.setState({loading_status:true})
    
        console.log("----",formData)
    
               let url = urls.BASE_URL +'api/user_detail'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                            console.log("----",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           // showMessage(responseJson.message,false)

                            //making details for the gallery
                            var temp = responseJson.user.gallery
                            temp.map(item=>{
                                Object.assign(item,{
                                    age:responseJson.user.age ,
                                     profession : responseJson.user.profession , 
                                     visibility : responseJson.user.visibility,
                                     verified : responseJson.user.verified,
                                     display_name : responseJson.user.display_name,

                                    })
                              })

                              this.setState({gallery:temp,user_details:responseJson.user,
                                country: responseJson.user.city.name  + ',' +
                                responseJson.user.state.name +  ',' +
                                responseJson.user.country.name,
                              ethnicity:responseJson.user.ethnicity.name,
                            body_type:responseJson.user.body_type.name
                          })


                          }
                          else{
                            showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });
    
    
    
    }


    componentWillMount(){
        let result = this.props.navigation.getParam('result')
        var user_id = result['user_id']
        this.setState({user_id:user_id})
        this._getProfile(user_id)


        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        });  
    }

    _checkSubscriptions = () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
               let url = urls.BASE_URL +'api/check_subscription'
                   return fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           
                           
                            return true
                          }
                          else{
                            //showMessage(responseJson.message)
                            return false
                          }
                         
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage('Try Again')
                        });
    
    }



    _winkUser =  () =>{
         
      var formData = new FormData();
    
      formData.append('from_id',this.props.user.user_id);
      formData.append('to_id',this.state.user_id);
      
      
      console.log("----",formData)
    
      this.setState({loading_status:true})
    
               let url = urls.BASE_URL +'api/wink'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("----",responseJson)
                          this.setState({loading_status:false})

                         
                          if(responseJson.status){
                           
                          
                            showMessage(responseJson.message,false)
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage(error.message)
                        });
    
    
    
    }

    _sendFriendRequest =  () =>{
         
      var formData = new FormData();
    
      formData.append('from_id',this.props.user.user_id);
      formData.append('to_id',this.state.user_id);
      
      
      console.log("----",formData)
    
      this.setState({loading_status:true})
    
               let url = urls.BASE_URL +'api/send_friend_request'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("----",responseJson)
                          this.setState({loading_status:false})

                         
                          if(responseJson.status){
                           
                            showMessage(responseJson.message,false)
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage(error.message)
                        });
    
    
    
    }

    _goToChats =() =>{
      if(this.props.user.user_id == this.state.user_id){
         showMessage('You cant chat with yourself');
         return
      }
      else{
       let user = this.state.user_details
       let obj={
         'to_id':user._id,
         'to_name':user.display_name,
         'to_image':user.user_image,
         'to_status':'offline',
         'to_typing':false
       }
       this.props.navigation.navigate("Chats",{result:obj})
      }
     }

    renderPage(item, index) {
        return (
            <View key={index} style={{backgroundColor:colors.COLOR_PRIMARY}}>
                <Image style={{ width: '100%', height: '100%' }} 
                resizeMode='contain'
                source={{uri:urls.BASE_URL +item.image}}/>





               
         
         
                            
                {
                    item.visibility == false?
            
                    <View style={{position:'absolute',top:0,
                    left:0,bottom:0,right:0, padding:10,backgroundColor:'rgba(0,0,0,0.9)',justifyContent:'center',alignItems:'center'
            }}>   
                
                <Image source={require('../assets/logo.png')} style={{height:150,width:150}} resizeMode='contain'/>


                </View>
                
                : null

                }
                

                <View style={{position:'absolute',bottom:30,
                left:10,padding:10,backgroundColor:'rgba(0,0,0,0.5)',borderRadius:11,elevation:10
                }}>  
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text style={{ textTransform: 'uppercase',color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:15}}>{item.display_name} </Text>
                        <Text style={{ textTransform: 'uppercase',color:colors.COLOR_PRIMARY,fontWeight:'bold'}}>, {item.age}</Text>

                        {
                            item.verified ?
                                <View>
                                <Image source={require('../assets/verify.png')} style={{height:20,width:20,marginLeft:10}} resizeMode='contain'/>
                                </View>
                                : null
                        
                      }
                
                        </View>

                
                
                        <Text style={{ color:colors.COLOR_PRIMARY,fontSize:12}}>{item.profession} </Text>

         
                </View>
                

            </View>
        );
      }

 

    render() {
        const { user_details } = this.state;


        return (
          <SafeAreaView style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />

          

                 <ScrollView style={{flex:1,backgroundColor:colors.BLACK,width:'100%',paddingBottom:40}}>
                            <View style={{height:dimensions.SCREEN_HEIGHT * 0.5,backgroundColor:'black'}}>
                                        <Carousel
                                        autoplay
                                        autoplayTimeout={2000}
                                        loop
                                        index={0}
                                        pageSize={Dimensions.get('window').width}
                                        >
                                        {this.state.gallery.map((image, index) => this.renderPage(image, index))}
                                    </Carousel>
                            </View>

                            <Animatable.Text 
                            style={{color:colors.WHITE,fontWeight:'bold',fontSize:17,alignSelf:'center',margin:10}} 
                            easing="ease-out"
                             iterationCount="infinite"
                            delay={500}
                            direction="alternate"
                            animation="pulse">{I18n.t('profile')}</Animatable.Text>




                            <View style={{padding:7}}>


                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('verified_user')} : </Text>
                            {
                                this.state.user_details.verified ?
                                <Text style={{color:colors.LIGHT_GREEN}}>{I18n.t('yes')}s</Text>
                                :  <Text style={{color:'red'}}>{I18n.t('no')}</Text>

                            }
                            </View>

                             <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('user_name')} : </Text>
                            <Text style={{color:colors.CRIMSON,fontWeight:'500',fontSize:13}}>{this.state.user_details.user_name}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('age')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.age} {I18n.t('years')}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('profession')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.profession}</Text>
                            </View>


                           
                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('gender')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.gender}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('interested_gender')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.interested_gender}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('height')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.height} {I18n.t('cms')}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('weight')} : </Text>
                            <Text style={styles.valueText}>{this.state.user_details.weight} {I18n.t('lbs')}</Text>
                            </View>

                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('ethnicity')} : </Text>
                            <Text style={styles.valueText}>{this.state.ethnicity} </Text>
                            </View>

                          
                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('body_type')} : </Text>
                            <Text style={styles.valueText}>{this.state.body_type} </Text>
                            </View>


                            <View style={styles.headingParent}>
                            <Text style={styles.headingText}>{I18n.t('location')} : </Text>
                            <Text style={styles.valueText}>{this.state.country}</Text>
                            </View>


                            <View style={{marginBottom:10}}></View>


                            <Text style={styles.headingText}> {I18n.t('about')} : 
                            <Text style={styles.valueText}>  {this.state.user_details.about}</Text>
                            </Text>

                            <View style={{height:60}}></View>


                            </View>



                           
                </ScrollView>


                <TouchableOpacity onPress={()=> this.props.navigation.pop()}
                style={{position:'absolute',top:Platform.OS == 'android' ? 30 : 50,left:20}}>
                <Image source={require('../assets/back-circle.png')} style={{width:40,height:40}} resizeMode='contain'/>
     
                </TouchableOpacity>

                <View style={{position:'absolute',
                bottom:10,right:10,left:10,height:60 ,backgroundColor:'rgba(128,128,128,0.4)',
                 flexDirection:'row',justifyContent:'space-between',alignItems:'center',
                paddingLeft:20,paddingRight:20,borderRadius:20}} >
              <TouchableOpacity onPress={()=>{
                this._checkSubscriptions().then(data => {
                  if(data) {
                    this._sendFriendRequest()
                  }
                  else  this.props.navigation.navigate('Subscriptions')
                })
              }}>
            <Image source={require('../assets/request-profile.png')} style={{width:50,height:50}} 
              resizeMode='contain'/>

            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{
              this._checkSubscriptions().then(data => {
                if(data) {
                  this._goToChats()
                }
                 else  this.props.navigation.navigate('Subscriptions')
              })
             }}>
            <Image source={require('../assets/chat-profile.png')} style={{width:50,height:50}} 
              resizeMode='contain'/>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>{
              this._winkUser()
             }}>
            <Image source={require('../assets/wink-profile.png')} style={{width:50,height:50}} 
              resizeMode='contain'/>

            </TouchableOpacity>
              
              
              </View>
                 
              
                  

             { this.state.loading_status && <ProgressBar/> }


           </SafeAreaView>
         
       

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
  valueText:{color:colors.CRIMSON,fontWeight:'500',fontSize:13,textTransform: 'capitalize'},
  headingText:{color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:16,textTransform:'uppercase'},
  headingParent:{flexDirection:'row',alignItems:'center',marginTop:10}
 
 
}
)



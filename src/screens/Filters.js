import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,
    StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,ScrollView,TouchableWithoutFeedback} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { StackActions, NavigationActions,DrawerActions} from 'react-navigation';
import {showMessage} from '../config/snackmsg';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import CustomDropDown from '../components/CustomDropDown';
import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements';
import { Chip } from 'react-native-paper';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import RBSheet from "react-native-raw-bottom-sheet";
import RNPickerSelect from 'react-native-picker-select';

import Icon from 'react-native-vector-icons/AntDesign';
const myIcon = <Icon name="caretdown" size={10} color={colors.COLOR_PRIMARY}  style={{marginTop:13}}/>;;
import FilterLocationComponent from '../components/FilterLocationComponent';



const d = Dimensions.get("window")


 class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          
          //weeight
          weight_values: [40, 150],
          //height
          height_values: [100, 220],
          //age
          age_values: [18, 90],

          date_status:true,
          dance_status:false,

          //gender
          male_checked:false,
          female_checked:true,


          transgender_check:false,
          location_status:false,
          selectedItems : [],
          cities:[],
          states:[],
          ethnicities:[],
          body_types:[],
          countries:[],
          country_id:'key0',
          ethnicity_boolean: [],
          ethnicity_id:[],
          ethnicity_names:[],
          bodytype_boolean: [],
          bodytype_id:[],
          bodytype_names:[],
          country_boolean: [],
          country_id:[],
          country_names:[],
          
        }

    }

    _fetchBodyTypes = async () =>{
        this.setState({loading_status:true})
    
                   let url = urls.BASE_URL +'api/body_types'
                        fetch(url, {
                        method: 'GET',
    
                        }).then((response) => response.json())
                            .then((responseJson) => {
    
                             
                              if(!responseJson.error){
                               
                                var temp_arr=[]
                                
                                var type = responseJson.result
                                type.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})

                                  let bodytype = [...this.state.bodytype_names];
                                
                                   bodytype.push( item.name );


                                    let bodytypes_ids = [...this.state.bodytype_id];  
                                    bodytypes_ids.push(item._id );


                                    let bodytypes_bool = [...this.state.bodytype_boolean];  
                                    bodytypes_bool.push( false );

                                    // Set state
                                  this.setState({ bodytype_id:bodytypes_ids,
                                    bodytype_names:bodytype,
                                    bodytype_boolean:bodytypes_bool });
                                    //ToastAndroid.show(responseJson.fuelid, ToastAndroid.LONG);
                                })
                                this.setState({body_types:type})
    
                              }
    
    
                            else{
                              showMessage(responseJson.message)
                            }


                            this.setState({loading_status:false})
    
                          }
                            ).catch((error) => {
    
                              this.setState({loading_status:false})
                              showMessage("Try Again.")
                            });
    
    
    
      }
  
      _fetchEthnicities = async () =>{
        this.setState({loading_status:true})
    
                   let url = urls.BASE_URL +'api/ethnicities'
                        fetch(url, {
                        method: 'GET',
    
                        }).then((response) => response.json())
                            .then((responseJson) => {
    
                              if(!responseJson.error){
                               
                                var temp_arr=[]
                                
                                var ethinicity = responseJson.result
                                ethinicity.map(item=>{
                                  Object.assign(item,{value:item._id,label:item.name,color:'black'})

                                  let ethnicity = [...this.state.ethnicity_names];
                                  ethnicity.push( item.name );
                                  let ethnicitys_ids = [...this.state.ethnicity_id];  
                                  ethnicitys_ids.push(item._id );
                                  let ethnicitys_bool = [...this.state.ethnicity_boolean];  
                                    ethnicitys_bool.push( false );

                                    this.setState({ ethnicity_id:ethnicitys_ids,
                                        ethnicity_names:ethnicity,
                                        ethnicity_boolean:ethnicitys_bool });


                                })
                                this.setState({ethnicities:ethinicity})


    
                              }
    
    
                            else{
                              showMessage(responseJson.message)
                            }
                            this.setState({loading_status:false})
    
                          }
                            ).catch((error) => {
    
                              this.setState({loading_status:false})
                              showMessage(error.message)
                            });
    
    
    
      }
  
  

 
    


    componentWillMount() {
        this._fetchBodyTypes()
        this._fetchEthnicities()

        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                }
                else {
                     I18n.locale = 'en'
                  }
        }); 
        
        
     let val = this.props.navigation.getParam('filters_data')

     console.log('filters-----',val)
     
   
     if(val == null ){
        //means filter wal called for first time 
        //so no need to set the values 
     }
     else{
      this.setState({
        weight_values : val['weight'],
        height_values : val['height'],
        male_checked : val['male'],
        female_checked : val['female'],
        age_values: val['age'],

      })

      }
    }

_isValid (){
  
  const language = this.languageDropDown.getSelectedValue()
 if(language.selected_value == null){
    showMessage('Select language')
    return false

  }
  
  else{
    return true;
  }


}

    _onSelect=()=>{
       if(this._isValid()){
        const language = this.languageDropDown.getSelectedValue()
        AsyncStorage.setItem('lang',language.selected_value);
        // this.props.navigation.navigate("Welcome")
 
        this.props.navigation.navigate('ModelsSwiper');
 
        //   const resetAction = StackActions.reset({
        //   index: 0,
        //   key: 'ModelsSwiper',
        //   actions: [NavigationActions.navigate({ routeName: 'ModelsSwiper' })],
        // });
 
        // this.props.navigation.dispatch(resetAction);
       }
 
 
        
    }

  multiWeightSliderValuesChange = ( weight_values) => {
      this.setState({
          weight_values,
      });
  }
  HeightMultiSliderValuesChange = (height_values) => {
    this.setState({
        height_values,
    });
 
}

  ageMultiSliderValuesChange = (age_values) => {
  this.setState({
      age_values,
  });

}


_submit =()=>{
 
            //making parameters for the ethnicity tag
            var length = this.state.ethnicity_boolean.length;
            var final= "";
            for(var i = 0 ; i < length ; i++){
              if(this.state.ethnicity_boolean[i] === false){
               //do nothing
              }
              else{
                let temp = this.state.ethnicity_id[i].toString()
                final = final + temp
                final = final + ","
              }
            }


              //making parameters for the body type tag
              var length = this.state.bodytype_boolean.length;
              var final_body= "";
              for(var i = 0 ; i < length ; i++){
                  if(this.state.bodytype_boolean[i] === false){
                       //do nothing
                  }
                  else{
                        let temp = this.state.bodytype_id[i].toString()
                        final_body = final_body + temp
                        final_body = final_body + ","
                    }
                }


                //making parameters for the location type tag
                var city = this.locationBottomSheet.getSelectedValue()
                var city_boolean = city['city_boolean']
                var city_id = city['city_id']
                  
                var length = city_boolean.length;
                var final_city= "";
                for(var i = 0 ; i < length ; i++){
                    if(city_boolean[i] === false){
                        //do nothing
                    }
                    else{
                          let temp = city_id[i].toString()
                          final_city = final_city + temp
                          final_city = final_city + ","
                      }
                  }


                    const { navigation } = this.props;
                    navigation.goBack();


                    navigation.state.params.onSelect({ 
                      weight: this.state.weight_values ,
                      height:this.state.height_values,
                      age:this.state.age_values,
                      male:this.state.male_checked,
                      female:this.state.female_checked,
                      location:final_city,
                      ethnicity:final.length > 0 ?final.substring(0, final.length-1) : '',
                      body_type:final_body.length > 0 ? final_body.substring(0, final_body.length-1) : '',
                    })
}

_close=()=>{
  var city = this.locationBottomSheet.getSelectedValue()
  //console.log('city------',city)

  this.locationSheet.close()
}



    render() {
        


         let chips = this.state.ethnicity_names.map((r, i) => {

            return (
                  <Chip 
                  style={{width:null,margin:4}}
                  mode={'outlined'}
                  selected={this.state.ethnicity_boolean[i]}
                
                  selectedColor={'black'}
                  onPress={() => {
                
                          let ids = [...this.state.ethnicity_boolean];    
                          ids[i] =!ids[i];                  
                          this.setState({ethnicity_boolean: ids });         
                  }}>
                  {r}
                  </Chip>
            );
         })
    
    
    
        let body_chips = this.state.bodytype_names.map((r, i) => {
        
        return (
                <Chip 
                style={{width:null,margin:4}}
                mode={'outlined'}
                selected={this.state.bodytype_boolean[i]}
                selectedColor={'black'}
                onPress={() => {
            
                        let ids = [...this.state.bodytype_boolean];    
                        ids[i] =!ids[i];                  
                        this.setState({bodytype_boolean: ids });         
                }}>
                {r}
                </Chip>
        );
        })

      

        
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly
               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }

              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            centerComponent={{ text:  I18n.t('filters'), style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
       <FilterLocationComponent
        inputRef={ref => this.locationSheet = ref}
        ref={ref => this.locationBottomSheet = ref}
        closeThis ={this._close}
     />
           
       {/*for main content*/}


       <View style={styles.body}>
       <ScrollView style={{margin:10,padding:0}} showsVerticalScrollIndicator={false}>

         {/*for*/}
      


        {/*loop*/}
        
          
      

       <View style={{marginBottom:10}}>

         {/*    ----location---  */}

         <View style={styles.parentLabelTwo}>
  
         <View style={{flexDirection:'row',alignItems:'center'}}>
         <Image style={styles.labelImage}  source={require('../assets/map-icon.png')} />
         <Text style={styles.labelText}>{
          I18n.t('location').toUpperCase()}</Text>       
           </View>
                    <TouchableOpacity onPress={()=> this.locationSheet.open()}>
                       <View style={{alignItems:'center',justifyContent:'center',
                     paddingBottom:3,paddingTop:3,paddingLeft:6,paddingRight:6,backgroundColor:'green',
                   borderRadius:15}}>
                        
                         <Text style={{color:'white',fontSize:12}}>{I18n.t('change')}</Text>
                         
                       </View>
                </TouchableOpacity>
             
           
         </View>
 
       
     {/*    ----location end---  */}
 
           {/*    ----gender----  */}
           <View style={styles.parentLabel}>
           <Image style={styles.labelImage}  source={require('../assets/gender-filter.png')} />
           <Text style={styles.labelText}>{
           I18n.t('gender').toUpperCase()}</Text>       
          </View>

          <View style={styles.genderBox}>
                <Text style={styles.genderText}> {I18n.t('female')}</Text>
                <CheckBox
                    checkedColor={colors.COLOR_PRIMARY}
                    onPress={() => this.setState({ female_checked: !this.state.female_checked })}
                    checked={this.state.female_checked}
                  />
            </View>
            
        <View style={styles.genderBox}>
        <Text style={styles.genderText}> {I18n.t('male')}</Text>
        <CheckBox
            checkedColor={colors.COLOR_PRIMARY}
            onPress={() => this.setState({ male_checked: !this.state.male_checked })}
            checked={this.state.male_checked}
          />
        </View>

         {/*    ----gender end----  */}
            <View style={{height:6}}/>

           {/*    ----weight----  */}
      
        <View style={styles.parentLabelTwo}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image style={styles.labelImage}  source={require('../assets/weight-filter.png')} />
                <Text style={styles.labelText}>{
                I18n.t('weight_kgs').toUpperCase()}</Text>       
             </View>
             <Text style={styles.valueText}>{this.state.weight_values[0]} - {this.state.weight_values[1]}</Text>
          
        </View>

        

       <View style={styles.sliderParent}>
            <MultiSlider
                values={[this.state.weight_values[0], this.state.weight_values[1]]}
                sliderLength={Platform.OS === 'android' ?
                 Dimensions.get('window').width *0.85 : Dimensions.get('window').width *0.85 }
                style={{alignSelf:'center'}}
                selectedStyle={{backgroundColor: colors.COLOR_PRIMARY}}
                markerStyle={{backgroundColor: colors.COLOR_PRIMARY}}
                onValuesChange={this.multiWeightSliderValuesChange}
                min={40}
                max={150}
                step={1}
            />
        </View> 

         {/*    ----weight end----  */}



          {/*    ----height----  */}

        <View style={styles.parentLabelTwo}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
                <Image style={styles.labelImage}  source={require('../assets/height-filter.png')} />
                <Text style={styles.labelText}>{
                I18n.t('height_cms').toUpperCase()}</Text>       
           </View>
                   <Text style={styles.valueText}>{this.state.height_values[0]} - {this.state.height_values[1]}</Text>
        </View>

       <View style={styles.sliderParent}>
            <MultiSlider
                values={[this.state.height_values[0], this.state.height_values[1]]}
                sliderLength={Platform.OS === 'android' ? Dimensions.get('window').width *0.85 : Dimensions.get('window').width *0.85 }
                style={{alignSelf:'center'}}
                selectedStyle={{backgroundColor: colors.COLOR_PRIMARY}}
                markerStyle={{backgroundColor: colors.COLOR_PRIMARY}}
                onValuesChange={this.HeightMultiSliderValuesChange}
                min={100}
                max={220}
                step={2}
            />
        </View> 

         {/*    ----height end----  */}


          {/*    ----age----  */}

        <View style={styles.parentLabelTwo}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <Image style={styles.labelImage}  source={require('../assets/age-filter.png')} />
        <Text style={styles.labelText}>{
         I18n.t('age').toUpperCase()}</Text>       
          </View>
               <Text style={styles.valueText}>{this.state.age_values[0]} - {this.state.age_values[1]}</Text>
      
       </View>

       <View style={styles.sliderParent}>
       <MultiSlider
           values={[this.state.age_values[0], this.state.age_values[1]]}
           sliderLength={Platform.OS === 'android' ? Dimensions.get('window').width *0.85 :
            Dimensions.get('window').width *0.85 }
           style={{alignSelf:'center'}}
           selectedStyle={{backgroundColor: colors.COLOR_PRIMARY}}
           markerStyle={{backgroundColor: colors.COLOR_PRIMARY}}
           onValuesChange={this.ageMultiSliderValuesChange}
           min={18}
           max={90}
           step={1}
       />
      
       </View> 
       {/*    ----age end----  */}



  






          {/*    ----ethnicity--  */}
        <View style={styles.parentLabel}>
        <Image style={styles.labelImage}  source={require('../assets/body-filter.png')} />
        <Text style={styles.labelText}>{
         I18n.t('ethnicity').toUpperCase()}</Text>       
          </View>
           <View style={{flexDirection:'row',flexWrap: 'wrap',margin:10}}>
         {chips}
       </View>
      {/*    ----ethnicity end---  */}

      {/*    ----body type---  */}

        <View style={styles.parentLabel}>
        <Image style={styles.labelImage}  source={require('../assets/body-filter.png')} />
        <Text style={styles.labelText}>{
        I18n.t('body_type').toUpperCase()}</Text>       
          </View>
          <View style={{flexDirection:'row',flexWrap: 'wrap',margin:10}}>
            {body_chips}
        </View>

          {/*    ----body type end---  */}
   
      
            <ButtonComponent 
            style={{width : d.width * 0.9,alignSelf:'center'}}
            handler={this._submit}
            label ={I18n.t('submit').toUpperCase()}/>
        

       </View>

       
           


</ScrollView>    


</View>
         

           

           { this.state.loading_status && <ProgressBar/> }


          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Filters);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.BLACK,
    flex:1
   
  },
  container:{
    alignItems: 'center',
    flex:1,
    backgroundColor: 'white'
    
  },
 
  input_text:{
    width:'100%',
    marginBottom:10,
    marginLeft:7,
    
    fontSize:18
  },
  labelText:{fontSize:16,marginBottom:0,fontWeight:'bold',color:colors.COLOR_PRIMARY},

  valueText:{fontSize:12,marginBottom:0,fontWeight:'700',color:'white',marginRight:10},

  parentLabel:{flexDirection:'row',alignItems:'center',
  backgroundColor:colors.MAROON,borderColor:'white',
 borderRadius:17,borderWidth:0.4,padding:2,marginTop:3,marginBottom:3},

 parentLabelTwo:{flexDirection:'row',alignItems:'center',
 backgroundColor:colors.MAROON,borderColor:'white',
borderRadius:17,borderWidth:0.4,justifyContent:'space-between',padding:2,marginTop:3,marginBottom:3},

 labelImage:{width: 20, height: 20,margin:5},

 genderBox:{ 
   flexDirection: 'row' ,justifyContent:'space-between',alignItems:'center',
 height:Platform.OS == 'android' ? 35 :55,
 marginBottom:0,marginTop:0,},

 genderText:{marginLeft:25,fontWeight:'500',color:colors.WHITE,},

 sliderParent:{width:'90%',marginLeft:Platform.OS === 'android' ? 0 : 15,marginRight:Platform.OS === 'android' ? 0 : 15,alignSelf:'center'},
 

  body:{
    width:'100%',
    height:'90%',
    paddingRight:5,
    margin:0
  },
 name_text:{
  width:'100%',
  marginBottom:10,
 
 },

 name_input: {
  width: "100%",
  height: 50,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
loginButton:{
  
  marginTop:50,
  marginBottom:15,
  width:"100%",
  height:50,
  backgroundColor:colors.COLOR_PRIMARY,
  borderRadius:10,
  borderWidth: 1,
  borderColor: 'black',

 
},
loginText:{
 color:'black',
 textAlign:'center',
 fontSize :20,
 paddingTop:10,
 textAlign:'center',
 color:'black'
},
about_text:{
  width:'100%',
  marginBottom:10
 },

 about_input: {
  width: "100%",
  height: 150,
  textAlign: 'center',
  borderRadius: 8,
  borderWidth: 1,
  borderColor: 'black',
  marginBottom: 15,
},
 
saveButton:{
    
    marginTop:20, 
     width:"100%",
     height:50,
     justifyContent:'center',
     alignItems:'center',
     backgroundColor:'#D2AC45',
     borderRadius:8,
     borderWidth: 1,
     borderColor: 'black',
   
    
  },
  saveText:{
    color:colors.COLOR_PRIMARY,
    textAlign:'center',
    fontSize :20,
    fontWeight : 'bold'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'rgba(128,128,128,0.5)'
  },
  dropDownParent:{flex:1,
    margin:3,
    borderRadius:20,borderWidth:0.7,
    borderColor:'white',
    padding:5
  }
 
 

}
)

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
     width:'90%',
    fontSize: 16,
    marginTop:5,
    color: colors.COLOR_PRIMARY,
  
  },
  inputAndroid: { height:40,
    width:'90%',
    fontSize: 13,
    marginTop:-5,
    color: colors.COLOR_PRIMARY,
  },
});

import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView,Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import BlackButton from '../components/BlackButton';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
import * as Animatable from 'react-native-animatable';

import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
  ProductPurchase,
  PurchaseError,
  Product
} from 'react-native-iap';

const d = Dimensions.get("window")

const itemSkusSubscriptions = Platform.select({
  ios: [
    'com.d8.1week','com.d8.1month','com.d8.3months','com.d8.12months'
  ],
  android: [
    'com.example.coins100' //android D8 app don't have any in app so dummy here
  ]
});


class Subscriptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          selectedItem:null,
          subscriptions:[],
          subscribed:null,
          products:null,
          subscription_price:0,
          subscription_id:0
           
        }

    }

    purchaseUpdateSubscription = null
    purchaseErrorSubscription = null

    _getSubscriptions = async () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
        this.setState({loading_status:true})
    
    
               let url = urls.BASE_URL +'api/get_subscriptions'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            //showMessage(responseJson.message,false)
                            this.setState({subscriptions:responseJson.result })
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });
    
    
    
    }



    _checkSubscriptions = async () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
      
    
    
               let url = urls.BASE_URL +'api/check_subscription'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          this.setState({loading_status:false})
                          
                          if(responseJson.status){
                           // showMessage(responseJson.message,false)
                            this.setState({subscribed: responseJson.subscription})
                            
                          }
                          else{
                           // showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });
    
    
    
    }
    

    handler = () =>{
  
    if(this.state.selectedItem == null) showMessage('Choose Subscription first')
    else {
      let obj = {
        'subscription_id' : this.state.selectedItem._id,
        'price':this.state.selectedItem.price
      }
      this.props.navigation.navigate("PaymentMode",{result:obj})
    }
   
   }


   buySubscription =(transactionId) =>{

    //0 means pay pal
    //1 means stripe
    //2 means in _app purchase
  
      
          this.setState({loading_status:true})
          var formData = new FormData();
          formData.append('user_id',this.props.user.user_id);
          formData.append('transaction_id',transactionId);
          formData.append('item_type', "Subscription");
          formData.append('payment_type',2);
          formData.append('subscription_id', this.state.subscription_id);
          formData.append('payment_price', this.state.subscription_price);
        
      
                 let url = urls.BASE_URL +'api/in_app_purchase'
                //  console.log("FFF",JSON.stringify(url))
                 fetch(url, {
                 method: 'POST',
                 headers: {
                   'Accept': 'application/json',
                   'Content-Type': 'multipart/form-data',
                 },
                 body: formData
                }).then((response) => response.json())
                     .then( (responseJson) => {
                         this.setState({loading_status:false})
                      if (responseJson.status){
      
                            showMessage(responseJson.message,false)
                            this.props.navigation.navigate("ModelsSwiper")
      
                         }else{
                           showMessage(responseJson.message)
              
                           }
                     }).catch((error) => {
                               this.setState({loading_status:false})
                               showMessage('Try Again.')
                              
      
                     });
   }
  


   _iap = async(sku) =>{
        try {
          let a = await RNIap.requestPurchase(sku, false);
         // Alert.alert('_iap function response....' + JSON.stringify(a))
          //console.log(JSON.stringify(a))
          

        } catch (err) {
          console.log("In App Purchase Error --->")
         // Alert.alert("In App Purchase Error --->"+err.message)
          showMessage(err.message)
          //console.warn(err.code, err.message);
        }
    }


  async componentWillMount(){
  
          try {

            const products: Product[] = await RNIap.getProducts(itemSkusSubscriptions);
            // Alert.alert(JSON.stringify(products))
             console.log("SubScriptions",JSON.stringify(products))

            this.setState({
              products:products
            })
            
          } catch(err) {
            //Alert.alert("Getting Products SKUS Error --->"+err.message)
            showMessage(err.message)
           // console.warn(err); // standardized err.code and err.message available
          }

      this._checkSubscriptions()
       this._getSubscriptions()



       AsyncStorage.getItem('lang')
       .then((item) => {
                 if (item) {
                   I18n.locale = item.toString()
               }
               else {
                    I18n.locale = 'en'
                 }
       });  
    }



  componentWillUnmount() {

      this.setState({
        products:null
      })
      if (this.purchaseUpdateSubscription) {
        this.purchaseUpdateSubscription.remove();
        this.purchaseUpdateSubscription = null;
      }
      if (this.purchaseErrorSubscription) {
        this.purchaseErrorSubscription.remove();
        this.purchaseErrorSubscription = null;
      }
    }

  componentDidMount() {
     
      this.purchaseUpdateSubscription = purchaseUpdatedListener((purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase ) => {
       console.log('purchaseUpdatedListener', purchase);
       const receipt = purchase.transactionReceipt;
       if (receipt) {
 
         RNIap.finishTransactionIOS(purchase.transactionId);
           //Alert.alert("Sucessfully purchased ----" +purchase.transactionId);
         //console.log("PURCHASEeeee",purchase.transactionId)

       
         //now hit api that purchase successfully....
         this.buySubscription(purchase.transactionId);
       }
     });
  
      this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
        // Alert.alert("purchaseErrorSubscriptionError --->"+error.message)
        //     console.warn('purchaseErrorListener', error);
            showMessage(error.message)
      });
 
   }

    _next = (item) =>{
      let obj ={
        'subscription_id' :item._id,
       'price':item.price
      }

      this.props.navigation.navigate("PaymentMode",{result:obj})
    }


    _onSelectPlan = (item) =>{
     
       let subscription_id = item._id
       let price  = item.price  

       this.setState({
        subscription_id:subscription_id,
        subscription_price: price
       },()=> console.log(this.state.subscription_id + '......' + this.state.subscription_price))

       let validity = item.validity  // 1 week,1 month,3 months,12 months

       var product = null

       if(validity == '1 Week'){
        product = this.state.products.filter(x => x.productId == 'com.d8.1week')
       }
       else if(validity == '1 Month'){
        product = this.state.products.filter(x => x.productId == 'com.d8.1month')
       }
       else if(validity == '3 Months'){
        product = this.state.products.filter(x => x.productId == 'com.d8.3months')
       }
       else if(validity == '12 Months'){
        product = this.state.products.filter(x => x.productId == 'com.d8.12months')
       }
     
       if(product.length == null){
        showMessage("Problem in fetching Product from Apple! Try Again")
        return
       }
       else{
       
        let sku = product[0].productId  //as filter function return data in array so [0]
        //showMessage("Going to purchase--" + sku)
       // console.log(sku)
          this._iap(sku)
        return 
       }
      
      
      
    }
    
     
     

    renderItem = ({item, index}) => {
      const isSelectedSubscription = this.state.selectedItem == null ? false : (this.state.selectedItem._id === item._id)
     // console.log(`Rendered item - ${item.id.value} for ${isSelectedUser}`);
      //const viewStyle = isSelectedUser ? styles.selectedButton : styles.normalButton;
        
      return(
        <TouchableOpacity onPress={()=>{
        this._onSelectPlan(item)
        }}
        style={{
          backgroundColor:colors.COLOR_PRIMARY,
          margin:10,
          width:'45%',
          alignItems:'center',
          justifyContent:'center',
          borderRadius:10,
          paddingTop:30
         
        }}>

        <View style={{backgroundColor:'black',width:'100%',
        borderTopLeftRadius:10,borderTopRightRadius:10,padding:5,justifyContent:'center',alignItems:'center',position:'absolute',
      top:0,left:0,right:0}}>
      <Animatable.Text 
      animation="pulse" easing="ease-out" iterationCount="infinite" 
      style={{color:'yellow',textAlign:'center'}}>{item.tag}</Animatable.Text>

        </View>

      

        <View style={{margin:5,justifyContent:'center',alignItems:'center'}}>

        <Image source={require('../assets/stars.png')} style={{width:50,height:50,margin:10}} resizeMode='contain'/>


          <Text style={{textAlign:'center',fontWeight:'bold',margin:2}}>Price : ${item.price}</Text>
          <Text style={{textAlign:'center',margin:2,fontSize:13}}>Validity : {item.validity}</Text>
        </View>
        
        
        </TouchableOpacity>
      )
     

    }

    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('subscriptions'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
             />

             <View style={{flex:1,backgroundColor:colors.WHITE,width:'100%',justifyContent:'center',alignItems:'center'}}>

             <View style={{flex:0.6,backgroundColor:colors.BLACK,width:'100%',alignItems:'center',justifyContent:'center'}}>
             <Text style={styles.headingText}>{I18n.t('choose_subscription')}</Text>
             {
               this.state.subscribed ?
               <Text 
               //animation="pulse" easing="ease-out" iterationCount="infinite" 
               style={{color:colors.COLOR_PRIMARY,textAlign:'center',margin:10,fontWeight:'bold'}}>{I18n.t('yes_subscription_one')} {this.state.subscribed.subscription_plan_id.validity } {' '} {I18n.t('yes_subscription_two')}
              </Text>
               : <Text
                style={{color:'red',textAlign:'center',margin:10,fontWeight:'bold'}}>{I18n.t('no_subscription_one')} {"\n"}
                </Text>
             }
            

             <Text style={styles.titleText}>{I18n.t('select_subscription')}.</Text>
             </View>

             <View style={{flex:1.4,backgroundColor:'grey',width:'100%',justifyContent:'center',alignItems:'center'}}>
                    
                  <FlatList
                  style={{marginBottom:10,width:'100%',padding:4}}
                  data={this.state.subscriptions}
                  numColumns={2}
                  extraData={this.state}

                  contentContainerStyle={{width:'100%',padding:4}}
                  // ItemSeparatorComponent = { this.FlatListItemSeparator }
                  showsVerticalScrollIndicator={false}
                  renderItem={(item,index) => this.renderItem(item,index)}
                  keyExtractor={item => item._id}
                    // Performance settings
                  removeClippedSubviews={true} // Unmount components when outside of window
                  initialNumToRender={2} // Reduce initial render amount
                  maxToRenderPerBatch={1} // Reduce number in each render batch
                  maxToRenderPerBatch={100} // Increase time between renders
                  windowSize={7} // Reduce the window size
                />


               



             </View>

            
            
             
              
                  

            


           </View>
         
       
           { this.state.loading_status && <ProgressBar/> }
             
           
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Subscriptions);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
  headingText:{
    color:'white',
    fontWeight:'bold',
    fontSize:18,
    marginBottom:15
    },
    titleText :{
      color:'white',
      textAlign:'center',
      fontSize:14,
      marginBottom:35
      },
 
}
)



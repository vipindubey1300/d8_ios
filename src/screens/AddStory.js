import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView,TextInput} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
import Video from 'react-native-video-controls';
import I18n from '../i18n';

import Carousel from 'react-native-snap-carousel';


const d = Dimensions.get("window")

class AddStory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
            'media_type' : 0,
             'media_source' : null,
            'media_object' : null,
             'caption':''
        
           
        }

    }


    componentWillMount(){
       // this._getMyStory()
       var result  = this.props.navigation.getParam('result')
       
        this.setState({media_type:result.media_type,
        media_source:result.media_source,
     media_object:result.media_object})

     AsyncStorage.getItem('lang')
     .then((item) => {
               if (item) {
                 I18n.locale = item.toString()
             }
             else {
                  I18n.locale = 'en'
               }
     });  

        
    }

    _addStory = () =>{
  
      
          this.setState({loading_status:true})
          var formData = new FormData();
      
      
         formData.append('user_id',this.props.user.user_id);
         formData.append('media_type',this.state.media_type);
         formData.append('media',this.state.media_object);
         formData.append('caption',this.state.caption);


         console.log("FFF",JSON.stringify(formData))



               let url = urls.BASE_URL +'api/add_story'
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then( (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))
    
    
                    if (responseJson.status){

                      const { navigation } = this.props;
                      navigation.goBack()
                    navigation.state.params.onAdd()
                      
                       //this.props.navigation.navigate("Stories")
    
                       }else{
                             showMessage(responseJson.message)
                      }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                             showMessage(error.message)
                        
                   });
    
    
     }

   

    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('add_story'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}/>
            <KeyboardAwareScrollView style={{backgroundColor:colors.BLACK,padding:20}}>


                <View style={{backgroundColor:colors.COLOR_PRIMARY,height:dimensions.SCREEN_HEIGHT * 0.7}}>
                {
                    this.state.media_type == 0
                    ?
                    <Image source={this.state.media_source}
                    resizeMode='contain'
                     style={{height:'100%',width:'100%'}}  />
                    :
                    <Video source={this.state.media_source}
                    disableBack
                      disableFullscreen
                      disableVolume
                      disableSeekbar
                      muted={true}
                      autoplay={true}
                      paused={false}
                      onPause={()=> {}}
                      onPlay={()=> {}}
      
                      showOnStart={false}
                      seekColor={colors.COLOR_PRIMARY}
      
                     style={{height:'100%',width:'100%'}}  />
                }
                </View>

                <View style={{backgroundColor:colors.BLACK,flex:2,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <TextInput
                    style={{  height:75,
                        textAlignVertical:'top',
                        padding:10,
                        fontSize: 14,marginTop:30,
                        color:colors.WHITE,borderRadius:10,borderColor:colors.COLOR_PRIMARY,borderWidth:3,flex:5}}
                    value={this.state.caption}
                    multiline={true}
                    placeholder={'Enter caption '}
                    placeholderTextColor={'grey'}
                    onChangeText={(caption) => this.setState({ caption})} />

                    <TouchableOpacity onPress={()=> this._addStory()}
                    style={{flex:1}}>
                    <Image
                     source={require('../assets/send.png')} 
                     style={{width:40,height:40,alignSelf:'flex-end'}} resizeMode='contain'/>

                    </TouchableOpacity>
                </View>
               
                      
           
            </KeyboardAwareScrollView>

           { this.state.loading_status && <ProgressBar/> }
             
           
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddStory);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
 
}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';

import {showMessage} from '../config/snackmsg';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';


const d = Dimensions.get("window")


export default class EnterOtp extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false
        }

    }



componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}


_isValid(){
  
  const otp = this.otpInput.getInputValue();
  
  if(otp.trim().length == 0){
    showMessage('Enter OTP')
    return false

  }
  else{
    return true;
  }


}


_onOTP = () =>{
  
    if(this._isValid()){
      const otp = this.otpInput.getInputValue();
      var result  = this.props.navigation.getParam('result')

     
      this.setState({loading_status:true})
      var formData = new FormData();
  
  
     formData.append('email',result['email']);
     formData.append('otp',otp);
  
           let url = urls.BASE_URL +'api/verify_email_otp'
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
          }).then((response) => response.json())
               .then( (responseJson) => {
                   this.setState({loading_status:false})

                   console.log("FFF",JSON.stringify(responseJson))


                if (responseJson.status){
                    let tempObject = {
                      'email' : result['email'],
                    }
                   this.props.navigation.navigate("EnterPassword",{result:tempObject})

                   }else{
                         showMessage(responseJson.message)
                  }
               }).catch((error) => {
                         this.setState({loading_status:false})
                         showMessage('Try Again.')
                    
               });
    }

 }



   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
             // enableAutomaticScroll={(Platform.OS === 'ios')}
           
             >



              <View style={styles.container}>
            

           

                 <Image source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>

                 <Text style={styles.headingText}>{I18n.t('enter_otp')}</Text>


                <Text style={styles.titleText}>{I18n.t('enter_otp_email')}</Text>


                          {/* text fields */}

                        <CustomTextInput
                          placeholder={I18n.t('otp')}
                          keyboardType="numeric"
                          maxLength={4}
                          secureTextEntry={true}
                          inputImage = {require('../assets/mail-text.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this._onOTP()}
                          inputRef={ref => this.otp = ref}
                          ref={ref => this.otpInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="done"
                        />

                             <ButtonComponent 
                             style={{width : d.width * 0.8,}}
                              handler={this._onOTP}
                             label ={I18n.t('verify_otp')}/>
                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           

           { this.state.loading_status && <ProgressBar/> }


          
          </View>

        )
    }
}

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 1,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
    
    
  },
  logo_image:{
    height:"25%", 
    width:'30%',
    
  },
 headingText:{
  color:'white',
  fontWeight:'bold',
  fontSize:18,
  marginBottom:15
  },
  titleText :{
    color:'white',
    
    fontSize:14,
    marginBottom:35
    }
 

}
)



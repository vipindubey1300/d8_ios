import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';

import { connect } from 'react-redux';

import I18n from '../i18n';
import ProgressBar from '../components/ProgressBar';


import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import {showMessage} from '../config/snackmsg';


import stripe from 'tipsi-stripe'
import PayPal from 'react-native-paypal-wrapper';
import { requestOneTimePayment } from 'react-native-paypal';


stripe.setOptions({
  // publishableKey: 'pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2',
  publishableKey:'pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB',
 //live : pk_live_j40fxUTzTRVqL0D8SWIIvdBy00NU75hBT2

 //androidPayMode: 'test',
    androidPayMode:'production'
})

const token = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpGVXpJMU5pSXNJbXRwWkNJNklqSXdNVGd3TkRJMk1UWXRjMkZ1WkdKdmVDSXNJbWx6Y3lJNklrRjFkR2g1SW4wLmV5SmxlSEFpT2pFMU9ESTNPRFk1T0Rnc0ltcDBhU0k2SWpBMFpESTRZemcyTFRCaE1HSXROR1psTXkxaE1HSmtMVGc1TlRnMk4yWXdNV1V3TlNJc0luTjFZaUk2SW1jNGFuQTNaRGsyZVROemNYSmtOMmNpTENKcGMzTWlPaUpCZFhSb2VTSXNJbTFsY21Ob1lXNTBJanA3SW5CMVlteHBZMTlwWkNJNkltYzRhbkEzWkRrMmVUTnpjWEprTjJjaUxDSjJaWEpwWm5sZlkyRnlaRjlpZVY5a1pXWmhkV3gwSWpwbVlXeHpaWDBzSW5KcFoyaDBjeUk2V3lKdFlXNWhaMlZmZG1GMWJIUWlYU3dpYjNCMGFXOXVjeUk2ZTMxOS54SHIzNVdGeV9TQUlHWVdGWjlaV0ZDRmFQbGpuNU0xclV2MW1PTzBjS1A0RmcwVkZ6WnplanRGbmFMUnBCVUpaMXdsU1c5U3Jwd0EyU2lGWHM5YlFDUSIsImNvbmZpZ1VybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy9nOGpwN2Q5Nnkzc3FyZDdnL2NsaWVudF9hcGkvdjEvY29uZmlndXJhdGlvbiIsImdyYXBoUUwiOnsidXJsIjoiaHR0cHM6Ly9wYXltZW50cy5zYW5kYm94LmJyYWludHJlZS1hcGkuY29tL2dyYXBocWwiLCJkYXRlIjoiMjAxOC0wNS0wOCJ9LCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2c4anA3ZDk2eTNzcXJkN2cvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vb3JpZ2luLWFuYWx5dGljcy1zYW5kLnNhbmRib3guYnJhaW50cmVlLWFwaS5jb20vZzhqcDdkOTZ5M3NxcmQ3ZyJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiSm9obiBEb2UncyBUZXN0IFN0b3JlIiwiY2xpZW50SWQiOiJBZEoyb3FwOWp1WUVGOGdQeVRCVnduRzkzRFh0RkNYVXRGTldGTjM4anpreDVtVUtqY2NRUTZpQTBPbmwyZWpJSVdXZ1cxYmpyY1liNmZNVCIsInByaXZhY3lVcmwiOiJodHRwczovL2V4YW1wbGUuY29tIiwidXNlckFncmVlbWVudFVybCI6Imh0dHBzOi8vZXhhbXBsZS5jb20iLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjpmYWxzZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJBVUQiLCJjdXJyZW5jeUlzb0NvZGUiOiJBVUQifSwibWVyY2hhbnRJZCI6Imc4anA3ZDk2eTNzcXJkN2ciLCJ2ZW5tbyI6Im9mZiJ9";


const d = Dimensions.get("window")

class PaymentMode extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          subscription_id:'',
          price:0.0,

        }

    }



 buySubscription =(transactionId,paymentType=1) =>{

  //0 means pay pal
  //1 means stripe
  
      var result  = this.props.navigation.getParam('result')
     
    
        this.setState({loading_status:true})
        var formData = new FormData();
        formData.append('user_id',this.props.user.user_id);
        formData.append('transaction_id',transactionId);
        formData.append('item_type', "subscription");
        formData.append('payment_type',paymentType);
        formData.append('subscription_id', this.state.subscription_id);
        formData.append('payment_price', this.state.price.toString());
      
    
               let url = urls.BASE_URL +'api/buy_subscription'
              //  console.log("FFF",JSON.stringify(url))
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))
    
    
                    if (responseJson.status){
    
                      showMessage(responseJson.message,false)
                      this.props.navigation.navigate("ModelsSwiper")
    
                       }else{
                         showMessage(responseJson.message)
            
                         }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                             showMessage('Try Again.')
                            
    
                   });
 }



 _stripePayment =(tokenId) =>{

      //0 means pay pal
      //1 means stripe
      
          var result  = this.props.navigation.getParam('result')
         
        
            this.setState({loading_status:true})
            var formData = new FormData();
            formData.append('token_id',tokenId);
            //multiply by 100 to avoid stripe cents error
            formData.append('amount', (this.state.price * 100).toString());
           
          
        
                   let url = urls.BASE_URL +'api/stripe'
                  //  console.log("FFF",JSON.stringify(url))
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data',
                   },
                   body: formData
                  }).then((response) => response.json())
                       .then(async (responseJson) => {
                           this.setState({loading_status:false})
        
                           //console.log("FFF",JSON.stringify(responseJson))
        
        
                        if (responseJson.status){
        
                            this.buySubscription(responseJson.result.balance_transaction,1)
        
                        }else{
                             showMessage(responseJson.message)
                
                             }
                       }).catch((error) => {
                                 this.setState({loading_status:false})
                                 showMessage('Try Again.')
                                
        
                       });
        }


componentWillMount(){
  var result  = this.props.navigation.getParam('result')
  var sid = result['subscription_id']
  var price = result.price

  this.setState({
    subscription_id:sid,
    price:price
  })

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}


async stripe(){
  const options = {
    requiredBillingAddressFields: 'full',
    prefilledInformation: {
      email:'abc@gmail.com',
      billingAddress: {
        name: 'Gunilla Haugeh',
        line1: 'Canary Place',
        line2: '3',
        city: 'Macon',
        state: 'Georgia',
        country: 'US',
        postalCode: '31217',
      },
    },
  }

  const token = await stripe.paymentRequestWithCardForm(options)
  var cardId = token.card.cardId
  var tokenId = token.tokenId


  this._stripePayment(tokenId)

  console.log("carddd",JSON.stringify(token))
}



paypal(){

  
    PayPal.initialize(PayPal.SANDBOX,"ARFiHtTEV2P17X4Hdei6M4vqoLx_UVGAvELcTpNprLC4hNklLmNq7cl_z5OqhRGoBzzVRmS9sG-RfhXx");
    //PayPal.initialize(PayPal.PRODUCTION,"AdCh6VdZcJ04UccfJHSWO7kraBNf2uukImABk5KC5uR8nFNaxl030c3ES92GVLSvJ1_NruLa_jgNlsxR");
    PayPal.pay({
      price: this.state.price.toString(),
      currency: 'USD',
      description: 'Payment for Purchase',
    }).then(confirm => {

      var pid = confirm.response.id

      this.buySubscription(pid,0)
     
      //console.log(JSON.stringify(confirm))
    })
      .catch(error =>{
        Platform.OS === 'android'
        ?  ToastAndroid.show("Transaction Cancelled", ToastAndroid.SHORT)
        : Alert.alert("Transaction Cancelled")
        

      })


}
// const {
//     nonce,
//     payerId,
//     email,
//     firstName,
//     lastName,
//     phone 
// }

_makePaypalTransaction(nonce){
  var result  = this.props.navigation.getParam('result')
     
    
  this.setState({loading_status:true})
  var formData = new FormData();
  formData.append('user_id',this.props.user.user_id);
  formData.append('nonce',nonce);
  formData.append('item_type', "subscription");
  formData.append('payment_type',0);
  formData.append('subscription_id', this.state.subscription_id);
  formData.append('payment_price', this.state.price.toString());


         let url = urls.BASE_URL +'api/buy_subscription'
        //  console.log("FFF",JSON.stringify(url))
         fetch(url, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'multipart/form-data',
         },
         body: formData
        }).then((response) => response.json())
             .then(async (responseJson) => {
                 this.setState({loading_status:false})

                 console.log("FFF",JSON.stringify(responseJson))


              if (responseJson.status){

                showMessage(responseJson.message,false)
                this.props.navigation.navigate("ModelsSwiper")

                 }else{
                   showMessage(responseJson.message)
      
                   }
             }).catch((error) => {
                       this.setState({loading_status:false})
                       showMessage('Try Again.')
                      

             });

}

_pay = async(token)=>{
  
  var result = await requestOneTimePayment(token,{
    amount: this.state.price.toString(), // required
    // any PayPal supported currency (see here: https://developer.paypal.com/docs/integration/direct/rest/currency-codes/#paypal-account-payments)
    currency: 'USD',
    // any PayPal supported locale (see here: https://braintree.github.io/braintree_ios/Classes/BTPayPalRequest.html#/c:objc(cs)BTPayPalRequest(py)localeCode)
    localeCode: 'en_USD', 
    shippingAddressRequired: false,
    userAction: 'commit', // display 'Pay Now' on the PayPal review page
    // one of 'authorize', 'sale', 'order'. defaults to 'authorize'. see details here: https://developer.paypal.com/docs/api/payments/v1/#payment-create-request-body
    intent: 'authorize', 
  });

  var nonce =  result.nonce
  this._makePaypalTransaction(nonce)
}


_getToken =  () =>{
  this.setState({loading_status:true})

             let url = urls.BASE_URL +'api/get_paypal_token'
                  fetch(url, {
                  method: 'GET',

                  }).then((response) => response.json())
                      .then((responseJson) => {

                        this.setState({loading_status:false})
                        if(responseJson.status){
                         
                          this._pay(responseJson.token)
                        }


                      else{
                        showMessage(responseJson.message)
                      }

                    }
                      ).catch((error) => {

                        this.setState({loading_status:false})
                        showMessage("Try Again.")
                      });



}


//
 // { email: 'seller@somedomain.com',
 //  firstName: 'Business',
 //  nonce: '2cefb82d-8d9e-09bb-5810-7fc7926d63f0',
 //  lastName: 'Account',
 //  phone: null,
 //  payerId: 'DZ3YWFQA2B3Q8' }

handler = (post_id) =>{
      var result  = this.props.navigation.getParam('result')

     result["gender"] = this.state.address
  
    this.props.navigation.navigate('InterestedGender',{result : result});
 
  
 }

 _selectPayment(gender){
    var result  = this.props.navigation.getParam('result')

    result["gender"] = gender
    this.props.navigation.navigate('InterestedGender',{result : result});
    //this.props.navigation.navigate('CreateProfile')
 }


   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />


           <Header
           statusBarProps={{ barStyle: 'light-content' }}
           barStyle="light-content" // or directly

            leftComponent={
            <TouchableOpacity onPress={()=>{

              this.props.navigation.goBack()
             }}>
           <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

           </TouchableOpacity>
           }
           statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
          centerComponent={{ text: I18n.t('payment'), style: { color: colors.WHITE } }}
           //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
           containerStyle={{
             //to remove bottom border
             borderBottomColor: colors.COLOR_SECONDARY,
           backgroundColor: colors.COLOR_SECONDARY,
       
       
         }}
          />


              
              <View style={styles.container}>
            

           

              

                 <Text style={styles.headingText}>{I18n.t('payment_mode')} </Text>


                 <Text style={styles.titleText}>{I18n.t('payment_type_choose')}</Text>


                          {/* selection */}

                          <View style={{flexDirection:'row',
                          alignItems:'center',justifyContent:'center'}}>
                          <TouchableOpacity onPress={()=> {this._getToken()}}>
                          <Image
                            source={require('../assets/paypal.png')} 
                            style={styles.genderImage} resizeMode='contain'/>
                          </TouchableOpacity>
                         

                          <TouchableOpacity onPress={()=> {this.stripe()}}>
                          <Image
                            source={require('../assets/stripe.png')} 
                            style={styles.genderImage} resizeMode='contain'/>
                          </TouchableOpacity>

                          </View>

                         


                            {/* selection end */}

                        



                             
                </View>
                { this.state.loading_status && <ProgressBar/> }



          
          </View>

        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(PaymentMode);



let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
    padding:10,
    flex:1,
    alignItems:'center',
    backgroundColor:colors.BLACK,
    justifyContent:'center'
   
    
    
  },
  headingText:{
   color:'white',
   fontWeight:'bold',
   fontSize:18,
   marginBottom:15
   },
   titleText :{
     color:'white',
     
     fontSize:14,
     marginBottom:25
     },
     genderImage :{
      height:d.height * 0.35, 
      width:d.width * 0.35,
     
      
    },
  
 

}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert,FlatList} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';
import I18n from '../i18n';

import {  colors,urls,dimensions } from '../Constants';
import firebase from 'react-native-firebase';

import {showMessage} from '../config/snackmsg';

const d = Dimensions.get("window")


class ChatLists extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          loading_status:false,
            chats:[]
        }

    }


 makeList(childData){
       
         let oldList = [...this.state.chats];
          oldList.push(childData);
         this.setState({chats:oldList},()=>{
           console.log("new list ",this.state.chat);
         })
 }

 initialize(id){
   this.setState({loading_status:true})

     //It will read data once from the `Users` object and print it on console. 
     //If we want to get data whenever there is any change in it, 
     //we can use on instead of once
   firebase.database().ref('recents/').orderByChild('timestamp').once('value',  (snapshot)=> {
   // console.log("SNAPSHOT",snapshot.val())
     snapshot.forEach((childSnapshot) =>{
       var childKey = childSnapshot.key;
       var childData = childSnapshot.val();
       // Display the data
     //  console.log("childKey",childKey)//5_22
     //   console.log("childData",childData)// {"last_message": "Scdscsdc", 
       //"timestamp": 1576000309991}


       var str = childKey;
       var res = str.split("_");//5,22

       //console.log("childKey-----------",res)


       if(res[0] == id || res[1] == id){
         console.log("YESSSSSS-----------",res)
         this.setState(previousState => ({
          chats: [...previousState.chats, childData]
         }));

             // this.setState({
             //   lists:childData
             // },()=>console.log("STATE-----------",JSON.stringify(this.state.lists)))

       }



       this.setState({loading_status:false})
       
     });
     console.log("STATE-----------",JSON.stringify(this.state.chats))



     
 }, (error) => {
         this.setState({loading_status:false})

   console.log("ERRROR-----------",error)
});





///

firebase.database().ref('recents/').orderByChild('timestamp').on('child_changed',  (snapshot)=> {
 
   //  console.log('Name of the field/node that was modified',snapshot.key); 
   // console.log('Value of the field/node that was modified',snapshot.val());
   // console.log("The updated " + 
   // snapshot.key + " is " + JSON.stringify(snapshot.val())); 

   var str = snapshot.key 
   var res = str.split("_");

   if(res[0] == this.props.user.user_id || res[1] == this.props.user.user_id){

       var index = this.state.chats.findIndex(x => (
    
       (x.receiver_id == res[0]  &&  x.sender_id == res[1])
       ||  (x.sender_id == res[0]  &&  x.receiver_id == res[1])
       ));

       //console.log("INDEX",index)

       const { chats } = this.state
       var tempArr = [...chats]
       tempArr[index] = snapshot.val()
       this.setState({chats:tempArr})

       snapshot.forEach((childSnapshot) =>{
   
       });

   }
  
}, (error) => {
 console.log(error)
});


 }

   




 componentWillMount(){
    this.setState({user_id:this.props.user.user_id},()=>{
      this.initialize(this.state.user_id)
    })

    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
 }

 componentWillUnmount() {
   firebase.database().ref('recents/').off('child_changed'); 
}


_updateReadStatus =(pairId)=>{
  //make sure to set last message as read as we clicked on message
  var key_from_read = `${this.props.user.user_id}_read`

  var ref1 =  firebase.database().
    ref('recents/' + pairId  )
    ref1.update({[key_from_read]:true});

}


  
_next =(item) =>{


  var to_id = this.props.user.user_id  == item.receiver_id ? item.sender_id : item.receiver_id
  var res = this.props.user.user_id.localeCompare(to_id)
  // -1 means user_id is less than to_id
  // 0 means user_id is equal to to_id
  // 1 means user_id is greater than to_id

  let pairId = ( res == -1 )
  ?  this.props.user.user_id + '_' + to_id
  : to_id + '_' + this.props.user.user_id

  this._updateReadStatus(pairId)


  
  let obj={
    'to_id':this.props.user.user_id  == item.receiver_id ? item.sender_id : item.receiver_id,
    'to_name':this.props.user.user_id == item.receiver_id? item.sender_name : item.receiver_name,
    'to_image':this.props.user.user_id  == item.receiver_id? item.sender_image : item.receiver_image,
    'to_status':'offline',
    'to_typing':false
  }
  this.props.navigation.navigate("Chats",{result:obj})
}


checkType(item){
  if(item.msg_type == 'text'){
    return (

      <Text style={{color:'grey'}}>{item.last_message.length > 5 ? 
        item.last_message.substring(0,6)   : 
        item.last_message }</Text>
    )
  }
  else if(item.msg_type == 'image'){
      return (
        <View style={{flexDirection:'row',
                                  alignItems:'center',marginTop:5}}>

                                    <Image source={require('../assets/attach.png')
                                    } style={{width:10,height:10,marginRight:10}} resizeMode='contain'/>

                                    <Text style={{color:'grey'}}>Image </Text>


                                  </View>
      )
  }
  else if(item.msg_type == 'location'){
    return (
      <View style={{flexDirection:'row',
      alignItems:'center',marginTop:5}}>

        <Image source={require('../assets/pin.png')
        } style={{width:20,height:20,marginRight:10}} resizeMode='contain'/>

        <Text style={{color:'grey'}}>Location </Text>


      </View>
    )
}
}
    renderItem = ({item, index}) => {
       // console.log(item)

        const a = urls.BASE_URL 
        const b = this.props.user.user_id  == item.receiver_id
        ? item.sender_image 
        : item.receiver_image
      
        //BELOW code is for typing features
        const rId = item.receiver_id
        const sId = item.sender_id
      
        let typeStatus = false
        let id = 0
        if(rId == this.props.user.user_id){
          //means receiver_id was my id 
            id = sId
      
        }
        else if(sId == this.props.user.user_id){
          //means sender id was my id 
            id = rId
        }
      
        let key = `${id}_typing`
        const imageUrl = a + b

        var foo = `${this.props.user.user_id}_read`
         var lastMessageRead = item[foo] ? true : false

        console.log("FOOO",lastMessageRead)





       // console.log(imageUrl)
        return(
          <TouchableOpacity onPress={()=> this._next(item)}>
          <View style={
              {padding:15,
              backgroundColor:lastMessageRead == false ? colors.PALE_GREEN : colors.LIGHT_YELLOW,
              width:dimensions.SCREEN_WIDTH * 0.93,
              margin:5,height:null,
              flexDirection:'row',justifyContent:'flex-start',alignItems:'center',borderRadius:5,alignSelf:'center'}}>
              
  
              <Image source={{uri:imageUrl}} 
              overflow='hidden'
              style={{width:40,height:40,borderRadius:20,overflow:'hidden'}} 
               resizeMode='cover'/>
                
              <View style={{marginLeft:10}}>
              <Text style={{marginLeft:0,fontWeight:'bold',fontSize:16,textTransform:'capitalize'}}>{
                this.props.user.user_id == item.receiver_id
                ? item.sender_name
                : item.receiver_name
                }</Text>

                {
                  item[key] ?
                  <Text style={{fontWeight:'700',color:'green'}}>{I18n.t('typing')}...</Text>
                  :

                  this.checkType(item)
                 


                }
              
              </View>

              
              <Text style={{color:'grey',
              marginLeft:3,
              position:'absolute',bottom:10,right:10,fontSize:11}}>{item.time}</Text>
         </View>
          </TouchableOpacity>
        )
       
  
      }
   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }

              centerComponent={{ text: I18n.t('chat_lists'), style: { color: colors.WHITE } }}

              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView style={{backgroundColor:colors.BLACK}}>
                              
         

           {
             this.state.chats.length > 0
             ?

          <FlatList
                   style={{marginBottom:10,width:'100%',padding:4}}
                   data={this.state.chats}
                  // ItemSeparatorComponent = { this.FlatListItemSeparator }
                   showsVerticalScrollIndicator={false}
                   renderItem={(item,index) => this.renderItem(item,index)}
                   keyExtractor={item => item._id}
                    // Performance settings
                  removeClippedSubviews={true} // Unmount components when outside of window
                  initialNumToRender={2} // Reduce initial render amount
                  maxToRenderPerBatch={1} // Reduce number in each render batch
                  maxToRenderPerBatch={100} // Increase time between renders
                  windowSize={7} // Reduce the window size
                 />

            :
            this.state.loading_status ?
            null
            :

                <View style={{justifyContent:'center',alignItems:'center',height:'90%'}}>
            <Image source={require('../assets/no-chat.png')}
              style={{width:200,height:200,alignSelf:'center',marginTop:Dimensions.get('window').height * 0.4}} resizeMode='contain'/>
              </View>


           }
                               
           </KeyboardAwareScrollView>
         

                      { this.state.loading_status && <ProgressBar/> }

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatLists);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 0.95,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
  },
 
}
)



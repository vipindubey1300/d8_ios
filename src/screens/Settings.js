import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';

import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';

import {showMessage} from '../config/snackmsg';
import { VinChandText } from '../components/CustomText';

const d = Dimensions.get("window")


class Settings extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          visibilityStatus:true
        }

    }





componentWillMount(){
  
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
    //I18n.locale = 'en'
}


  


   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
                 centerComponent={{ text:I18n.t('settings'), style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
          



              <View style={styles.container}>

                <View style={{alignItems:'center'}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ResetPassword")}
                     style={styles.parent}>
                        <Text style={styles.text}>{I18n.t('reset_password')}</Text>
                    </TouchableOpacity>
                    <View style={styles.border}></View>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("ChangeLanguage")}
                    style={styles.parent}>
                        <Text style={styles.text}>{I18n.t('change_language')} </Text>
                    </TouchableOpacity>
                    <View style={styles.border}></View>

                    <TouchableOpacity onPress={()=> this.props.navigation.navigate("DeleteAccount")}
                    style={styles.parent}>
                        <Text style={styles.deletetext}>{I18n.t('delete_account')}</Text>
                    </TouchableOpacity>
                </View>
             
                </View>

                              

                               
         

           

			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Settings);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
    flex:1,
    
    padding:10,
    alignItems:'center',
    backgroundColor:colors.BLACK,
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:0,
    justifyContent:"center"
   
  },
  parent:{flexDirection:'row'},
  text:{color:colors.COLOR_PRIMARY,
fontSize:19},
deletetext:{color:'red',
    fontSize:19},
border:{width:dimensions.SCREEN_WIDTH * 0.8,height:1,backgroundColor:'grey',marginTop:10,marginBottom:10}

 
}
)



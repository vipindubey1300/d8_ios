import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {  colors,urls ,dimensions} from '../Constants';
import I18n from '../i18n';

import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

class Splash extends React.Component {
    constructor(props) {
        super(props);

    }








    home = async () =>{

      await AsyncStorage.multiGet(["user_id", "name","image","email"]).then(response => {
        console.log(response[0][0]) // Key1
        console.log(response[0][1]) // Value1
        console.log(response[1][0]) // Key2
        console.log(response[1][1])// Value2
  
        let user_id = response[0][1]
        let name = response[1][1]
        let image = response[2][1]
        let email = response[3][1]
       
  
        this.props.add({ 
          user_id: user_id, 
          name : name,
          image : image,
          email:  email 
        })
    })
  
     
     this.props.navigation.navigate('Home')
    }
  
  
  xyz = async () =>{
   
   
              AsyncStorage.getItem('user_id')
              .then((value) => {
                if(value){
                    this.home()
                }
  
                else{
                  this.props.navigation.navigate('Welcome')
  
                }
              });
  
  
  }


  abc = async () =>{
    await AsyncStorage.getItem('privacy_status')
    .then((item) => {
      if (item) {
        AsyncStorage.getItem('user_id')
              .then((value) => {
                if(value){
                    this.home()
                }
  
                else{
                  this.props.navigation.navigate('Welcome')
  
                }
              });
  }
  else {
    //do something else means go to login
   // this.next()
    this.props.navigation.navigate('PrivacyPolicy')
  }
  });
  
  }
  
  
  
  
  check = async () =>{
     setTimeout(() => {
             
        this.abc()
        // this.props.navigation.navigate('HomePage')
  
  
          }, 1000)
  
  
    }
  
    componentDidMount() {
      StatusBar.setBackgroundColor(colors.status_bar_color)
      this.check()
  }




    render() {
        return (
          <View
            style={styles.container}>

			     <Image source={require('../assets/logo.png')} style={styles.splash_image} resizeMode='contain'/>

          </View>

        )
    }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}


export default connect(null, mapDispatchToProps)(Splash);

let styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height:"100%",
    backgroundColor:colors.COLOR_SECONDARY
  },
  splash_image:{
    height:"35%", 
    width:'50%',
    marginTop:-20
  }

}
)



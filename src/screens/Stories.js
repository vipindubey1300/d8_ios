import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar
  ,StyleSheet, TouchableOpacity, SafeAreaView,FlatList,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ProgressBar from '../components/ProgressBar';
import I18n from '../i18n';

import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';

import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import {showMessage} from '../config/snackmsg';
import ImagePicker from 'react-native-image-picker';

import CustomModalOptions from '../components/CustomModalOptions';

const d = Dimensions.get("window")

class Stories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          loading_status:false,
          stories:[], //my_status
          friends_stories:[]//friend status
           
        }

    }

    _getMyStory = async () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
        this.setState({loading_status:true})
    
    
               let url = urls.BASE_URL +'api/my_story'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("------",responseJson)
                          
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           // showMessage(responseJson.message,false)
                            this.setState({stories:responseJson.my_status,
                              friends_stories:responseJson.friend_status})
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage(error.message)
                        });
    
    
    
    }


    componentWillMount(){
       this._getMyStory()
       AsyncStorage.getItem('lang')
       .then((item) => {
                 if (item) {
                   I18n.locale = item.toString()
               }
               else {
                    I18n.locale = 'en'
                 }
       });  
    }

    _next(stories,index){
     
    
      let obj={
       'stories':stories
       }
       this.props.navigation.navigate("FriendStory",{result:obj})

 }

    _goToMyStory = ()=>{
      let obj={
        'stories':this.state.stories
        }
        this.props.navigation.navigate("MyStory",{result:obj})
      
    }



 


 renderItem = ({item, index}) => {
  console.log(item)
  const length = item.status.length
  return(
    <TouchableOpacity onPress={()=> this._next(item.status)}>
    <View style={
        {padding:15,
        backgroundColor:colors.LIGHT_YELLOW,
        width:dimensions.SCREEN_WIDTH * 0.93,
        margin:5,height:null,
        flexDirection:'row',justifyContent:'flex-start',alignItems:'center',borderRadius:5,alignSelf:'center'}}>

        {
          item.status[0].media_type == 0
          ?
          <Image source={{uri:urls.BASE_URL + item.status[0].media_link}} 
          overflow='hidden'
          style={styles.user_image} resizeMode='cover'/>
          :
          <Image source={{uri:urls.BASE_URL + item.user_two.user_image}} 
          overflow='hidden'
          style={styles.user_image} resizeMode='cover'/>

        }


        <View style={{marginLeft:10}}>
        <Text style={{color:'black',fontSize:14,fontWeight:'900',textTransform:'capitalize'}}>{item.user_two.display_name}</Text>
        <Text style={{color:'grey',fontSize:11,textTransform:'capitalize'}}>{item.status[length-1].posted_ago}</Text>

        </View>





   </View>
    </TouchableOpacity>
  )
 

}

onAddStory = data => {
  this._getMyStory()
};


selectVideo() {
  const options = {
    title: 'Video Picker',
    mediaType: 'video',
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.launchCamera(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
   }else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }else {
      let source = { uri: response.uri};
     // this.setState({videoSource: source});
     this.chooseSourceModal.setState({visibility:false})


      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }

      let obj ={
        //0 image 1 video
        'media_type' : 1,
        'media_source' : source,
        'media_object' : video
      }


      this.props.navigation.navigate('AddStory',{result: obj,onAdd: this.onAddStory})


    }
  });

  
}


addVideoCamera() {
  const options = {
    title: 'Video Picker',
    mediaType: 'video',
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.launchCamera(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
   }else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }else {
      let source = { uri: response.uri};
     // this.setState({videoSource: source});
     this.chooseVideoModal.setState({visibility:false})


      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }

      let obj ={
        //0 image 1 video
        'media_type' : 1,
        'media_source' : source,
        'media_object' : video
      }


      this.props.navigation.navigate('AddStory',{result: obj,onAdd: this.onAddStory})


    }
  });
}
addVideoGallery() {
  const options = {
    title: 'Video Picker',
    mediaType: 'video',
    videoQuality: 'medium',
    durationLimit: 30,
    takePhotoButtonTitle: 'Take Video...',
    allowsEditing: true,
    storageOptions: {
      skipBackup: true
    }
  };

  ImagePicker.launchImageLibrary(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
   }else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }else {
      let source = { uri: response.uri};
     // this.setState({videoSource: source});
     this.chooseVideoModal.setState({visibility:false})


      var video={
        name: 'name.mp4',
        uri: source.uri,
        type: 'video/mp4'
      }

      let obj ={
        //0 image 1 video
        'media_type' : 1,
        'media_source' : source,
        'media_object' : video
      }


      this.props.navigation.navigate('AddStory',{result: obj,onAdd: this.onAddStory})


    }
  });
}


addPhotoCamera() {
  const options = {  maxWidth: 500,maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }else {
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };

      this.choosePhotoModal.setState({visibility:false})

      let obj ={
        //0 image 1 video
        'media_type' : 0,
        'media_source' : source,
        'media_object' : photo
      }

      this.props.navigation.navigate('AddStory',{result: obj,onAdd: this.onAddStory})

    }
  });
}
addPhotoGallery() {
  const options = {  maxWidth: 500,maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }else {
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };

      this.choosePhotoModal.setState({visibility:false})

     
      let obj ={
        //0 image 1 video
        'media_type' : 0,
        'media_source' : source,
        'media_object' : photo
      }

      this.props.navigation.navigate('AddStory',{result: obj,onAdd: this.onAddStory})
    }
  });
}


rightButtonClick= () =>{
  this.addPhotoCamera()
}
leftButtonClick= () =>{
  this.addPhotoGallery()

}

rightVideoButtonClick= () =>{
  this.addVideoCamera()
}
leftVideoButtonClick= () =>{
  this.addVideoGallery()

}

rightButtonClickChoose= () =>{
 this.choosePhotoModal.setState({visibility:true})
 this.chooseSourceModal.setState({visibility:false})
}
leftButtonClickChoose= () =>{
  //this.selectVideo()
  this.chooseVideoModal.setState({visibility:true})
  this.chooseSourceModal.setState({visibility:false})
 }



    render() {
      const {stories} = this.state
      let isStatus = stories.length > 0 ? true : false
      let storiesLength = stories.length
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />

           <CustomModalOptions 
           rightButtonClick={this.rightButtonClickChoose}
           leftButtonClick={this.leftButtonClickChoose}
           title={I18n.t('choose_story_source')}
           rightButton={I18n.t('image')}
           leftButton={I18n.t('video')}
           description={I18n.t('create_story_to_share')}
           ref={ref => this.chooseSourceModal = ref}/>


           <CustomModalOptions 
      
           rightButtonClick={this.rightButtonClick}
           leftButtonClick={this.leftButtonClick}
           title={I18n.t('choose_image_source')}
           rightButton={I18n.t('camera')}
           leftButton={I18n.t('gallery')}
           description={I18n.t('choose_story_image')}
           ref={ref => this.choosePhotoModal = ref}/>


           <CustomModalOptions 
      
           rightButtonClick={this.rightVideoButtonClick}
           leftButtonClick={this.leftVideoButtonClick}
           title={I18n.t('choose_video_source')}
           rightButton={I18n.t('camera')}
           leftButton={I18n.t('gallery')}
           description={I18n.t('choose_story_video')}
           ref={ref => this.chooseVideoModal = ref}/>


              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }
              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
             centerComponent={{ text: I18n.t('stories'), style: { color: colors.WHITE } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}/>
            <ScrollView style={{flex:1,backgroundColor:colors.BLACK,padding:20}}>
                {/* my story */}
                <View>

                <Text style={{textTransform:'uppercase',
                color:colors.BLACK,fontWeight:'bold',width:'100%',
                backgroundColor:colors.COLOR_PRIMARY,marginBottom:10,padding:5}}>{I18n.t('my_story')}</Text>


                <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
                {
                  isStatus ?
                  <TouchableOpacity onPress={()=> this._goToMyStory()}>
                  {
                    stories[storiesLength - 1].media_type == 0 ?
                    <Image source={{uri:urls.BASE_URL + stories[storiesLength - 1].media_link}} 
                    overflow='hidden'
                    style={{height:60,width:60,borderRadius:30,borderWidth:4,borderColor:colors.COLOR_PRIMARY}} resizeMode='cover'/>
                    :
                    <Image source={require('../assets/video-story.png')} 
                    overflow='hidden'
                    style={{height:60,width:60,borderRadius:30,borderWidth:4,borderColor:colors.COLOR_PRIMARY}} resizeMode='cover'/>
                  }
  
                   </TouchableOpacity>
                   :
                   <Image source={{uri:urls.BASE_URL + this.props.user.image}} 
                            overflow='hidden'
                            style={{height:60,width:60,borderRadius:30}} resizeMode='cover'/>


                }

                {
                  isStatus ?
                  <Text style={{marginLeft:20,color:'green',fontWeight:'bold',fontSize:15}}>{I18n.t('story_posted')} {stories[storiesLength - 1].posted_ago}</Text>
                  :
                  <Text style={{marginLeft:20,color:colors.WHITE}}>{I18n.t('no_story_found')}</Text>


                }
                
                 
                </View>



                </View>
                 {/* my story end */}



                 {/* friend story */}
                <View style={{marginTop:30}}>

                <Text style={{textTransform:'uppercase',
                color:colors.BLACK,fontWeight:'bold',width:'100%',
                backgroundColor:colors.COLOR_PRIMARY,marginBottom:10,padding:5}}>{I18n.t('friends_story')}</Text>

                      
                        <FlatList
                        style={{marginBottom:10,width:'100%',padding:4}}
                        data={this.state.friends_stories}
                        showsVerticalScrollIndicator={false}
                        renderItem={(item,index) => this.renderItem(item,index)}
                        keyExtractor={item => item._id}
                      
                      />



                </View>
                 {/* friend story end */}
           
            </ScrollView>

              {/* floating action button*/}
            <View style={{
              position:'absolute',bottom:30,right:20,elevation:10,shadowOpacity:0.7
            }}>
            <TouchableOpacity onPress={()=>  this.chooseSourceModal.setState({visibility:true})
          }>
            <Image 
            source={require('../assets/story-add.png')} style={{width:60,height:60}} resizeMode='contain'/>

            </TouchableOpacity>
            </View>
               {/* floating action button end */}

            
         
       
           { this.state.loading_status && <ProgressBar/> }
             
           
         
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Stories);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
   
    flex:1
   
  },
  container:{
   
   
    height: '100%',
    
    
  },
  user_image:{
    height:50, 
    width:50,
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:2,
    borderRadius:25,
    marginRight:15
    
  },
 
}
)



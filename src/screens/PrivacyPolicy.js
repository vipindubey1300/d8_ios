import React, {Component} from 'react';
import {Text, View,Platform,Alert,Image,Dimensions,ToastAndroid,ScrollView,StatusBar,StyleSheet,ImageBackground,TouchableOpacity,TextInput,TouchableWithoutFeedback,ActivityIndicator} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Chip } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { CheckBox } from 'react-native-elements'
import {  colors,urls,dimensions } from '../Constants';
import WebView  from 'react-native-webview';
import ButtonComponent from '../components/ButtonComponent';

import {showMessage} from '../config/snackmsg';

export default class PrivacyPolicy extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
        checked:false,
    };
}

  next=()=>{
    if(this.state.checked){
      AsyncStorage.setItem('privacy_status',"false");
      this.props.navigation.navigate("Welcome")
    }
    else{
        showMessage('You must agree to privacy policy !')
    
    }
    
  }

  render() {
    return (
       <View style = {styles.container}>
         <WebView
            style={{height:'100%'}}
           source = {{ uri: urls.BASE_URL + 'api/terms' }}>
         
           </WebView>

         <View style={{width:'100%',backgroundColor:'black',borderColor:'white',borderWidth:0.4,}}>
           <View style={{ flexDirection: 'row',marginLeft:-10 ,alignSelf:'center'}}>
                <CheckBox
                center
                checkedColor={colors.COLOR_PRIMARY}
                onPress={() => this.setState({ checked: !this.state.checked })}
                checked={this.state.checked}
              />
                <Text style={{marginTop: 18,marginLeft:-15,color:'white'}}>Are you above 18+ ? </Text>
           </View>
  
         <ButtonComponent 
         style={{width : '80%',alignSelf:'center'}}
          handler={this.next}
         label ={'Accept'}/>

         </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
   container: {
      flex:1,
   },
   privacyButton:{
  alignSelf:'center',
    marginBottom:15,
    marginTop:10,
    width:"90%",
    height:50,
    backgroundColor:colors.COLOR_PRIMARY,
    borderRadius:10,
    borderWidth: 1,
    borderColor: 'black',
  
   
 },
 privacyText:{
   color:'black',
   textAlign:'center',
   fontSize :18,
   paddingTop:10
   
 },
})

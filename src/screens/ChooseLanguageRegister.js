import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { StackActions, NavigationActions,DrawerActions} from 'react-navigation';
import {showMessage} from '../config/snackmsg';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import CustomDropDown from '../components/CustomDropDown';
import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';
import { connect } from 'react-redux';


const d = Dimensions.get("window")

                        


const languages =[
    {
        label:'English',
        value:'en'
    },
    {
        label:'Indian',
        value:'hi'
    },
    {
        label:'Thai',
        value:'th'
    },
    {
        label:'Chinese',
        value:'zh'
    },
    {
        label:'Spanish',
        value:'es'
    },
    {
        label:'Russian',
        value:'ru'
    },
    {
        label:'German',
        value:'de'
    },
    {
        label:'French',
        value:'fr'
    },
      {
        label:'Italian',
        value:'it'
    },


]
 class ChooseLanguageRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          countries:[],
          loading_status:false
        }

    }

 
    


    componentWillMount() {
        AsyncStorage.getItem('lang')
        .then((item) => {
                  if (item) {
                    I18n.locale = item.toString()
                    this.languageDropDown.setState({selectedValue:item})
                }
                else {
                     I18n.locale = 'en'
                  }
        }); 
        
      
      }


_isValid (){
  
  const language = this.languageDropDown.getSelectedValue()
 if(language.selected_value == null){
    showMessage('Select language')
    return false

  }
  
  else{
    return true;
  }


}

    _onSelect=()=>{
       if(this._isValid()){
        const language = this.languageDropDown.getSelectedValue()
        AsyncStorage.setItem('lang',language.selected_value);
        // this.props.navigation.navigate("Welcome")
 
        this.props.navigation.navigate('Register');
 
       
       }
 
 
        
    }



    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.pop()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


        


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
             // enableAutomaticScroll={(Platform.OS === 'ios')}
           
             >



              <View style={styles.container}>
            

           

                 <Image source={require('../assets/logo.png')} style={styles.logo_image} resizeMode='contain'/>

                 <Text style={styles.headingText}>{I18n.t('change_language')}</Text>


             

                          {/* text fields */}

                          <CustomDropDown
                          data={languages}
                          ref={ref => this.languageDropDown = ref}
                          inputImage={require('../assets/lang-edit.png')}
                          placeholder={{
                            label: 'Language',
                            value: null,
                            color: colors.COLOR_PRIMARY,
                            
                          }}
                          />

                             <ButtonComponent 
                             style={{width : d.width * 0.8,}}
                              handler={this._onSelect}
                             label ={I18n.t('save_language')}/>
                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           

           { this.state.loading_status && <ProgressBar/> }


          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChooseLanguageRegister);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 1,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
    
    
  },
  logo_image:{
    height:"25%", 
    width:'30%',
    
  },
 headingText:{
  color:'white',
  fontWeight:'bold',
  fontSize:18,
  marginBottom:15
  },
  titleText :{
    color:'white',
    
    fontSize:14,
    marginBottom:35
    }
 

}
)



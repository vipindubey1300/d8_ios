import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView, TextInput, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import BigTextInput from '../components/BigTextInput';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';

import CustomDropDown from '../components/CustomDropDown';

import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import {  colors,urls,dimensions } from '../Constants';
import {showMessage} from '../config/snackmsg';
import CustomModalOptions from '../components/CustomModalOptions';
import I18n from '../i18n';

const d = Dimensions.get("window")

const age = [{label:'18',value:18},
            {label:'19',value:19},
            {label:'20',value:20},
            {label:'21',value:21},
            {label:'22',value:22},
            {label:'23',value:23},
            {label:'24',value:24},
            {label:'25',value:25},
            {label:'26',value:26},
            {label:'27',value:27},
            {label:'28',value:28},
            {label:'29',value:29},
            {label:'30',value:30},
            {label:'31',value:31},
            {label:'32',value:32},
            {label:'33',value:33},
            {label:'34',value:34},
            {label:'35',value:35},
            {label:'36',value:36},
            {label:'37',value:37},
            {label:'38',value:38},
            {label:'39',value:39},
            {label:'40',value:40},

          ]

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class CreateProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          imageSource: null ,
          imagePhoto: null,
          display_name:'',
          about:'', 
          body_types:[],
          ethnicities:[]         
        }

    }

    _fetchBodyTypes = async () =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/body_types'
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              var temp_arr=[]
                              
                              var type = responseJson.result
                              type.map(item=>{
                                Object.assign(item,{value:item._id,label:item.name,color:'black'})
                              })
                              this.setState({body_types:type})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }

    _fetchEthnicities = async () =>{
      this.setState({loading_status:true})
  
                 let url = urls.BASE_URL +'api/ethnicities'
                      fetch(url, {
                      method: 'GET',
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
  
                            this.setState({loading_status:false})
                            if(!responseJson.error){
                             
                              var temp_arr=[]
                              
                              var ethnicity = responseJson.result
                              ethnicity.map(item=>{
                                Object.assign(item,{value:item._id,label:item.name,color:'black'})
                              })
                              this.setState({ethnicities:ethnicity})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({loading_status:false})
                            showMessage("Try Again.")
                          });
  
  
  
    }



componentWillMount(){
  this._fetchBodyTypes()
  this._fetchEthnicities()
 
  var result  = this.props.navigation.getParam('result')
  var user_id = result.user_id
  var gender = result.gender
  var interested_gender = result.interested_gender

  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}

handler = ()=>{

}

addPhotoCamera() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchCamera(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
      this.choosePhotoModal.setState({visibility:false})

      this.setState({
        imageSource: source ,
        imagePhoto:photo,

      }); 
      
     

     
    }
  });
}
addPhotoGallery() {
  const options = {
    maxWidth: 500,
    maxHeight: 500,
    storageOptions: {
      skipBackup: true
    }
  };

  //launchCamera
  //showImagePicker
  //launchImageLibrary
  ImagePicker.launchImageLibrary(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled photo picker');
    }
    else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    }
    else {
      let source = { uri: response.uri};

      var photo = {
        uri: response.uri,
        type:"image/jpeg",
        name: Platform.OS == 'android'  ? response.fileName : "asdfsdfasdfsdf.jpeg"
      };
     
      this.choosePhotoModal.setState({visibility:false})
      this.setState({
        imageSource: source ,
        imagePhoto:photo,

      }); 
      
    }
  });
}


_isValid = () =>{
  const name = this.nameInput.getInputValue();
  const about = this.aboutInput.getInputValue();
  const profession = this.professionInput.getInputValue();
  const weight = this.weightInput.getInputValue();
  const height = this.heightInput.getInputValue();

  const age = this.ageDropDown.getSelectedValue()
  const body_type = this.bodyTypesDropDown.getSelectedValue()
  const ethnicities = this.ethnicitiesDropDown.getSelectedValue()

  let height_valid = /^\d*\.?(?:\d{1,2})?$/.test(height.toString())
  let weight_valid = /^\d*\.?(?:\d{1,2})?$/.test(weight.toString())
  console.log(weight)

  
  if(this.state.imagePhoto == null || this.state.imagePhoto == ''){
    showMessage('Choose Display Image')
  
   return false
  }
  else if(name.trim() == ''){
      showMessage('Enter Display Name')
    
     return false
  }
  else if(profession.trim() == ''){
    showMessage('Enter Profession')
  
   return false
}
  else if(weight.trim() == ''){
    showMessage('Enter Weight')

    return false
  }
  else if(!weight_valid){
    showMessage('Enter Valid Weight')

    return false
  }
  
  else if(parseInt(weight) < 40 ||parseInt(weight) == 0){
    showMessage('Enter weight greater than 40')
     return false;
   }
  else if(parseInt(weight) > 150 ){
    showMessage('Enter weight less than 150')
      return false;
  }
  else if(height.trim() == ''){
    showMessage('Enter height')

    return false
  }
  else if(!height_valid){
    showMessage('Enter Valid Height')

    return false
  }
  else if(height < 100 ||height == 0){
    showMessage('Enter height greater than 100')
     return false;
   }
  else if(height > 220 ){
    showMessage('Enter height less than 220')
      return false;
    }
  else if(age.selected_value == null){
    showMessage('Select age')
    return false

  }
  else if(body_type.selected_value == null){
    showMessage('Select body type')
    return false

  }
  else if(ethnicities.selected_value == null){
    showMessage('Select ethnicity')
    return false

  }
  else{
    return true;
  }


}

_onCreateProfile = () =>{

if(this._isValid()){
  const name = this.nameInput.getInputValue();
  const about = this.aboutInput.getInputValue();
  const profession = this.professionInput.getInputValue();
  const weight = this.weightInput.getInputValue();
  const height = this.heightInput.getInputValue();

  const age = this.ageDropDown.getSelectedValue()
  const body_type = this.bodyTypesDropDown.getSelectedValue()
  const ethnicities = this.ethnicitiesDropDown.getSelectedValue() 
  var result  = this.props.navigation.getParam('result')
  var user_id = result.user_id
  var gender = result.gender
  var interested_gender = result.interested_gender

    this.setState({loading_status:true})
    var formData = new FormData();
    formData.append('user_id',user_id);
    formData.append('gender',gender);
    formData.append('interested_gender', interested_gender);
    formData.append('display_name',name);
    formData.append('about',about.trim());
    formData.append('profession',profession.trim());
    formData.append('age',age.selected_value);
    formData.append('user_image', this.state.imagePhoto);
    formData.append('weight',weight);
    formData.append('height',height);
    formData.append('body_type_id',body_type.selected_value);
    formData.append('ethnicity_id', ethnicities.selected_value);
  

           let url = urls.BASE_URL +'api/create_profile'
          //  console.log("FFF",JSON.stringify(url))
           fetch(url, {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'multipart/form-data',
           },
           body: formData
          }).then((response) => response.json())
               .then(async (responseJson) => {
                   this.setState({loading_status:false})

                   console.log("FFF",JSON.stringify(responseJson))


                if (responseJson.status){

                  var  user_id = responseJson.user._id
                  var name = responseJson.user.display_name
                  var email = responseJson.user.email
                  var image = responseJson.user.user_image

                  AsyncStorage.multiSet([
                    ["user_id", user_id.toString()],
                    ["email", email.toString()],
                    ["name", name.toString()],
                    ["image", image.toString()],
                    

                  ]);
                   await this.props.add({ 
                      user_id: user_id, 
                      name : name,
                      image : image,
                      email:  email 
                    })

                    this.props.navigation.navigate('Home');
  
                 
              

                   }else{
                     showMessage(responseJson.message)
        
                     }
               }).catch((error) => {
                         this.setState({loading_status:false})
                         showMessage('Try Again.')
                        

               });
 }

 }


 handler= (post_id,like_id) =>{
  //  console.log("RECT ON POSTS",post_id)
    console.log("LIKE ",like_id)
  
 }
 rightButtonClick= () =>{
   this.addPhotoCamera()
  //this.choosePhotoModal.setState({visibility:false})
 }
 leftButtonClick= () =>{
   this.addPhotoGallery()

 }

   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


        


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
              centerComponent={{ text:  I18n.t('create_profile'), style: { color: colors.WHITE } }}
             
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
              enableOnAndroid={true} 
             // enableAutomaticScroll={(Platform.OS === 'ios')}
           
             >




              <View style={styles.container}>

              <CustomModalOptions 
              handler={this.handler}
              rightButtonClick={this.rightButtonClick}
              leftButtonClick={this.leftButtonClick}
              title={ I18n.t('choose_image_source')}
              rightButton={ I18n.t('camera')}
              leftButton={ I18n.t('gallery')}
              description={ I18n.t('choose_display_picture_making')}
              ref={ref => this.choosePhotoModal = ref}/>


              {
                this.state.imagePhoto == null
                ?
                <TouchableOpacity onPress={()=> this.choosePhotoModal.setState({visibility:true})}>
                <Image
                source={require('../assets/avatar.png')}  
                              style={styles.avatarImage} resizeMode='contain'/>
              </TouchableOpacity>

              :
              <TouchableOpacity onPress={()=> this.choosePhotoModal.setState({visibility:true})}>
              <Image
             
              source={this.state.imageSource} 
              style={styles.avatarImage} />
            </TouchableOpacity>


              }


                     <CustomTextInput
                          placeholder= {I18n.t('display_name')}
                          keyboardType="default"
                          inputImage = {require('../assets/user.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.profession.focus()}
                          inputRef={ref => this.name = ref}
                          ref={ref => this.nameInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />

                        <CustomTextInput
                          placeholder={ I18n.t('profession')}
                          keyboardType="default"
                          inputImage = {require('../assets/user.png')}
                          focus={this.onChangeInputFocus}
                          onSubmitEditing={()=> this.about.focus()}
                          inputRef={ref => this.profession = ref}
                          ref={ref => this.professionInput = ref}
                          style={{width:'90%'}}
                          //onChangeText={(text) => this.handleNameChange(text)}
                          returnKeyType="next"
                        />
                        <CustomTextInput
                        placeholder={ I18n.t('weight')}
                        keyboardType="numeric"
                        maxLength={3}
                        textContentType='telephoneNumber'
                        inputImage = {require('../assets/weight-edit.png')}
                        inputRef={ref => this.weight = ref}
                        ref={ref => this.weightInput = ref}
                        style={{width:'90%',paddingLeft:  Platform.OS == 'ios' ?  15 : 0}}
                        
                      />
                      <CustomTextInput
                      placeholder={ I18n.t('height')}
                      keyboardType="numeric"
                      maxLength={3}
                      textContentType='telephoneNumber'
                      inputImage = {require('../assets/height-edit.png')}
                      inputRef={ref => this.height = ref}
                      ref={ref => this.heightInput = ref}
                      style={{width:'90%',paddingLeft:  Platform.OS == 'ios' ?  15 : 0}}
                      
                    />
                    
                        <CustomDropDown
                        style={{marginBottom:10,marginTop:10, paddingLeft:  Platform.OS == 'ios' ?  -20 : 20}}
                         data={age}
                          placeholder={{
                            label:  I18n.t('age'),
                            value: null,
                            color: colors.COLOR_PRIMARY,
                        
                        }}
                        ref={ref => this.ageDropDown = ref}
                        inputImage={require('../assets/age-edit.png')}
                        />

                        <CustomDropDown
                        style={{marginBottom:10,marginTop:10}}

                        data={this.state.body_types}
                         placeholder={{
                           label: I18n.t('body_types'),
                           value: null,
                           color: colors.COLOR_PRIMARY,
                       
                       }}
                       ref={ref => this.bodyTypesDropDown = ref}
                       inputImage={require('../assets/body-type-edit.png')}
                       />
                       <CustomDropDown
                       style={{marginBottom:10,marginTop:10}}

                        data={this.state.ethnicities}
                         placeholder={{
                           label:I18n.t('ethnicities'),
                           value: null,
                           color: colors.COLOR_PRIMARY,
                       
                       }}
                       ref={ref => this.ethnicitiesDropDown = ref}
                       inputImage={require('../assets/body-type-edit.png')}
                       />

                        <BigTextInput
                          placeholder={I18n.t('about')}
                          keyboardType="default"
                         
                          focus={this.onChangeInputFocus}
                          inputRef={ref => this.about = ref}
                          ref={ref => this.aboutInput = ref}
                          style={{width:'90%',marginTop:10}}
                         
                          returnKeyType="done"
                        />

                        {/* <TextInput
                          placeholder='About Us'
                          placeholderTextColor={colors.WHITE}
                          style={styles.bigInput}
                        /> */}


                              <ButtonComponent 
                             style={{width : d.width * 0.8}}
                              handler={this._onCreateProfile}
                             label ={I18n.t('save')}/>

                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           
           { this.state.loading_status && <ProgressBar/> }

			   

          
          </View>

        )
    }
}

const mapDispatchToProps = dispatch => {
  return {
      add: (userinfo) => dispatch(addUser(userinfo)),
  }
}
export default connect(null, mapDispatchToProps)(CreateProfile);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height :Platform.OS =='android' ?  d.height * 1.2 :  d.height * 1.3,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
    
    
  },
  bigInput:{
    height:90,
    width:d.width * 0.85,
    backgroundColor:colors.LIGHT_MAROON,
    borderWidth:2,
    borderRadius:15,
    borderColor:colors.DARK_MAROON,
    textAlignVertical:'top',
    padding:10,
    marginTop:15
  },
  avatarImage :{
    height:150, 
    width:150,
    borderRadius:75,
    overflow:'hidden',
    marginBottom:40,
    marginTop:20
    
   
    
  },
 

}
)



import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, 
  TouchableOpacity, SafeAreaView, Platform,Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header} from 'react-native-elements';
import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import ButtonComponent from '../components/ButtonComponent';
import CustomTextInput from '../components/CustomTextInput';
import CustomModalOptions from '../components/CustomModalOptions';
import SwitchToggle from "react-native-switch-toggle";
import * as Animatable from 'react-native-animatable';

import {  colors,urls,dimensions } from '../Constants';
import I18n from '../i18n';

import {showMessage} from '../config/snackmsg';
import { VinChandText } from '../components/CustomText';

const d = Dimensions.get("window")


class ProfileMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          visibilityStatus:true
        }

    }


    _myProfile =() =>{

           
            var formData = new FormData();
            formData.append('user_id',this.props.user.user_id);
          
          
        
                   let url = urls.BASE_URL +'api/my_profile'
                  //  console.log("FFF",JSON.stringify(url))
                   fetch(url, {
                   method: 'POST',
                   headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'multipart/form-data',
                   },
                   body: formData
                  }).then((response) => response.json())
                       .then(async (responseJson) => {
                          
                        if (responseJson.status){
        
                         var visibility = responseJson.user.visibility
               
                         this.setState({visibilityStatus:visibility})
        
                           }else{
                             //showMessage(responseJson.message)
                
                             }
                       }).catch((error) => {
                                 this.setState({loading_status:false})
                                 showMessage('Error')
                                
        
                       });
        }


componentWillMount(){
  console.log(JSON.stringify(this.props.user))
    this._myProfile()
    AsyncStorage.getItem('lang')
    .then((item) => {
              if (item) {
                I18n.locale = item.toString()
            }
            else {
                 I18n.locale = 'en'
              }
    });  
    //I18n.locale = 'en'
}

_updateVisibility = async (visibility_status) =>{
         
  var formData = new FormData();

  formData.append('user_id',this.props.user.user_id);
  formData.append('visibility_status',visibility_status);
  

  console.log("----",formData)

           let url = urls.BASE_URL +'api/change_visibility'
                fetch(url, {
                method: 'POST',
                body:formData

                }).then((response) => response.json())
                    .then((responseJson) => {
                      console.log("----",responseJson)
                      //showMessage(responseJson.message,false)
                     
                  }
                    ).catch((error) => {

                      showMessage(error.message)
                    });



}



logout = async() =>{
  this.logoutModal.setState({visibility:false})
    try {
     
  
      this.props.remove({});
  
  
  
      await AsyncStorage.removeItem("user_id");
      await AsyncStorage.removeItem("name");
      await AsyncStorage.removeItem("image");
      await AsyncStorage.removeItem("email");
      await AsyncStorage.removeItem("request");
      await AsyncStorage.removeItem("accept");
      //await AsyncStorage.removeItem("lang");
      
      this.props.navigation.navigate("Welcome")
     showMessage("Logged Out Successfully.")
      return true;
    }
    catch(exception) {
      
      return false;
    }
  }
  
  
  remove = async() =>{
  
    Alert.alert(
      'Logout',
      'Are you sure to logout ?',
      [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      },
      {
        text: 'OK',
        onPress: () => this.logout()
      }
      ],
      {
      cancelable: false
      }
    );
  
   
    
  }
  handler= (post_id,like_id) =>{
   
    
   }
   leftButtonClick= () =>{
     this.logout()
    //this.logoutModal.setState({visibility:false})
   }
   rightButtonClick= () =>{
    this.logoutModal.setState({visibility:false})
  
   }


  


   
    render() {
        return (
          <View style={styles.safeAreaContainer}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{

                 this.props.navigation.goBack()
                }}>
              <Image source={require('../assets/left-arrow-white.png')} style={{width:25,height:25}} resizeMode='contain'/>

              </TouchableOpacity>
              }


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
           <KeyboardAwareScrollView
           
             >



              <View style={styles.container}>
              <CustomModalOptions 
              handler={this.handler}
              rightButtonClick={this.rightButtonClick}
              leftButtonClick={this.leftButtonClick}
              title={I18n.t('logout')}
              rightButton={I18n.t('no')}
              leftButton={I18n.t('yes')}
              description={I18n.t('are_sure_to_logout')}
              ref={ref => this.logoutModal = ref}/>

            

                <View style={styles.topContainer}>

                        
                          <TouchableOpacity onPress={()=> this.props.navigation.navigate('MyProfile')}
                          style={styles.imageParentView}>
                          <Image source={{uri:urls.BASE_URL + this.props.user.image}} 
                            overflow='hidden'
                            style={styles.logo_image} resizeMode='cover'/>
                          </TouchableOpacity>


                        

                           <VinChandText style={{
                            color:'white',
                           textTransform:'capitalize',fontSize:19,
                            }}>{this.props.user.name}</VinChandText>


                              {/**
                          <View style={styles.editImage}>

                                <TouchableOpacity onPress={()=>{

                                   
                                    }}>
                                    <Image source={require('../assets/user-menu.png')}
                                     style={{width:30,height:30}} resizeMode='contain'/>

                                </TouchableOpacity>

                          </View>

                           */}

                         
                        
                  </View>

                  <Text style={{
                    color:this.state.visibilityStatus ? 'green' : 'red',marginBottom:-5
                  }}>{ I18n.t('discreet_mode') }</Text>


                  <SwitchToggle
                     buttonText={()=>{ return this.state.switchOn4 ? "Hour" : "Day";}}
                     backTextRight={()=>{return this.state.switchOn4 ? "" : "Hour";}}
                     backTextLeft={()=>{return this.state.switchOn4 ? "Day" : "";}}
                      type={1}
                      buttonStyle={{
                        alignItems: "center",
                        justifyContent: "center",
                        position: "absolute"
                      }}
                      rightContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                      leftContainerStyle={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "flex-start"
                      }}
                      buttonTextStyle={{ fontSize: 20 }}
                      textRightStyle={{ fontSize: 20 }}
                      textLeftStyle={{ fontSize: 20 }}
                      containerStyle={{
                        marginTop: 16,
                        width: 50,
                        height: 25,
                        borderRadius: 30,
                        padding: 2
                      }}
                      backgroundColorOn={colors.COLOR_PRIMARY}
                      backgroundColorOff={colors.COLOR_PRIMARY}
                      circleStyle={{
                        width: 20,
                        height: 20,
                        borderRadius: 10,
                        backgroundColor: "blue" // rgb(102,134,205)
                      }}
                      switchOn={this.state.visibilityStatus}
                      onPress={()=> this.setState({ visibilityStatus: !this.state.visibilityStatus },()=> this._updateVisibility(this.state.visibilityStatus))}
                      circleColorOff="red"
                      circleColorOn='green'
                      duration={500}
        />

                        <View style ={styles.bottomContainer}>
                        <TouchableOpacity  onPress={()=> this.props.navigation.navigate("MyProfile")}>
                        <View style={styles.menuParent}>

                            <Image 
                            source={require('../assets/profile-user.png')}
                            style={styles.menuImage} 
                            resizeMode='contain'/>


                            <Text style={styles.menuLabel}>{I18n.t('my_profile')}</Text>

                        </View>
                      </TouchableOpacity>

                      <View style={styles.menuDivider}/>
                                    <TouchableOpacity  onPress={()=> this.props.navigation.navigate("Stories")}>
                                    <View style={styles.menuParent}>

                                        <Image 
                                        source={require('../assets/story.png')}
                                        style={styles.menuImage} 
                                        resizeMode='contain'/>


                                        <Text style={styles.menuLabel}>{I18n.t('stories')}</Text>

                                    </View>
                                  </TouchableOpacity>

                                  <View style={styles.menuDivider}/>

                                        <TouchableOpacity  onPress={()=> this.props.navigation.navigate("Friends")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/friends.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('friends')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                        <View style={styles.menuDivider}/>




                                        <TouchableOpacity  onPress={()=> this.props.navigation.navigate("ChatLists")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/chat-menu.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('chats')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                        <View style={styles.menuDivider}/>


                                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("Subscriptions")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/buy-coins.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('buy_subscriptions')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                        {/** 

                                        <View style={styles.menuDivider}/>
                                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("ChangeLanguage")}>
                                        <View style={styles.menuParent}>

                                            <Image 
                                            source={require('../assets/lang.png')}
                                            style={styles.menuImage} 
                                            resizeMode='contain'/>


                                            <Text style={styles.menuLabel}>{I18n.t('change_language')}</Text>

                                        </View>
                                      </TouchableOpacity>

                                    */}

                                      <View style={styles.menuDivider}/>


                                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("Notifications")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/notifications.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('notifications')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                        <View style={styles.menuDivider}/>

                                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("Settings")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/settings.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('settings')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                        <View style={styles.menuDivider}/>

                                        <TouchableOpacity onPress={()=>  this.props.navigation.navigate("ContactUs")}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/contact-us.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('contact_us')}</Text>

                                          </View>
                                        </TouchableOpacity>


                                        <View style={styles.menuDivider}/>

                                        <TouchableOpacity onPress={()=>  this.logoutModal.setState({visibility:true})}>
                                          <View style={styles.menuParent}>

                                              <Image 
                                              source={require('../assets/logout.png')}
                                              style={styles.menuImage} 
                                              resizeMode='contain'/>


                                              <Text style={styles.menuLabel}>{I18n.t('logout')}</Text>

                                          </View>
                                        </TouchableOpacity>

                                      



                                 

                         </View>

               

                


               


                             

                                


                             
                </View>

                              

                               
           </KeyboardAwareScrollView>
         

           

			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (userinfo) => dispatch(removeUser(userinfo)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ProfileMenu);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:colors.COLOR_PRIMARY,
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : d.height * 0.95,
    alignItems:'center',
    backgroundColor:colors.BLACK
   
  },
  topContainer:{
    height:dimensions.SCREEN_HEIGHT * 0.3,
    width:dimensions.SCREEN_WIDTH * 0.9,
    justifyContent:'center',
    alignItems:'center',
   

  },
  imageParentView:{
                            
    height:dimensions.SCREEN_HEIGHT * 0.2,
    width:dimensions.SCREEN_HEIGHT * 0.2,
  },
  editImage:{
    position:'absolute',
    bottom:Platform.OS === 'android' ? 45 :55,
    right:dimensions.SCREEN_WIDTH * 0.33
  },
  bottomContainer:{
    width:'100%',
    marginTop:30
  },
  menuParent:{
    flexDirection:'row',
    alignItems:'center'
  },
  menuImage:{width:25,height:25,marginRight:20},
  menuLabel:{
    color:colors.WHITE,
    fontSize:15
   
  },
  menuDivider:{
    height:0.5,
    width:'100%',
    backgroundColor:'grey',
    marginBottom:5,
    marginTop:5
  },
  logo_image:{
    height:"95%", 
    width:'100%',
   
    borderColor:colors.COLOR_PRIMARY,
    borderWidth:3,
    borderRadius:dimensions.SCREEN_HEIGHT * 0.14,
    
  },
 
}
)



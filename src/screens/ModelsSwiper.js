import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,
  StatusBar,StyleSheet,
   TouchableOpacity, SafeAreaView,Animated, Easing,
  Modal,Alert,PermissionsAndroid ,AppState} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Header,Overlay} from 'react-native-elements';
import Swiper from '@starodubenko/react-native-deck-swiper'
import Geolocation from '@react-native-community/geolocation';

import { connect } from 'react-redux';
import { addUser, changeUser } from '../actions/actions';
import I18n from '../i18n';
import LocationRangeSheet from '../components/LocationRangeSheet';

import SwiperComponent from '../components/SwiperComponent';
import CustomModalOptions from '../components/CustomModalOptions';
import ProgressBar from '../components/ProgressBar';
import LogoProgressBar from '../components/LogoProgressBar';
import {  colors,urls,dimensions } from '../Constants';
import {showMessage} from '../config/snackmsg';
import RBSheet from "react-native-raw-bottom-sheet";
import CustomModalOk from '../components/CustomModalOk';
import {getFCMToken} from '../config/DeviceTokenUtils';


import firebase from 'react-native-firebase';
import firebaseSDK from '../config/firebaseSDK';

const d = Dimensions.get("window")

class ModelsSwiper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
         // cards: [...range(1, 50)],
          users:[],
          latitude: 0.00,
          longitude: 0.00,
          user_loading_status:false,
          loading_status:false,
          winked_modal_visibility:false,
          swipedAllCards: false,
          swipeDirection: '',
          cardIndex: 0,
          selectedModel:0,
          isSubscribed:false,
          chats:[],
          chatCount:0,
          filters_data:null

        }


    }

    _getNearByUsers =  (range) =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
      formData.append('lat',this.state.latitude);
      formData.append('long',this.state.longitude);
      formData.append('range',range);
      
      console.log("NEARBY USERS----",formData)
    
      this.setState({loading_status:true})
    
               let url = urls.BASE_URL +'api/nearby_users'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("NEARBY USERS----",responseJson)
                       
                          this.setState({loading_status:false})

                         
                          if(responseJson.status){
                            var users = responseJson.results
                           
                            this.setState({users:users})

                            this.setState({swipedAllCards:false,selectedModel:0})
                            showMessage(responseJson.message,false)
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage(error.message)
                        });
    
    
    
    }

    _updateUserLocation = async () =>{
         
        var formData = new FormData();

        formData.append('user_id',this.props.user.user_id);
        formData.append('lat',this.state.latitude);
        formData.append('long',this.state.longitude);

       
  
                 let url = urls.BASE_URL +'api/update_location'
                      fetch(url, {
                      method: 'POST',
                      body:formData
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            
                           
                        }
                          ).catch((error) => {
  
                            showMessage(error.message)
                          });
  
  
  
    }


    _checkSubscriptions = () =>{
         
      var formData = new FormData();
    
      formData.append('user_id',this.props.user.user_id);
     
               let url = urls.BASE_URL +'api/check_subscription'
                   return fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           
                            this.setState({isSubscribed:true})
                            return true
                          }
                          else{
                            //showMessage(responseJson.message)
                            return false
                          }
                         
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                          showMessage('Try Again')
                        });
    
    }



    _winkUser =  (toId) =>{
         
      var formData = new FormData();
    
      formData.append('from_id',this.props.user.user_id);
      formData.append('to_id',toId);
      
      
      console.log("----",formData)
    
      this.setState({loading_status:true})
    
               let url = urls.BASE_URL +'api/wink'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("----",responseJson)
                          this.setState({loading_status:false})

                         
                          if(responseJson.status){
                           
                            this.winkModal.setState({visibility:true})
                            //showMessage(responseJson.message,false)
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage(error.message)
                        });
    
    
    
    }

    _sendFriendRequest =  () =>{
         
      var formData = new FormData();
    
      formData.append('from_id',this.props.user.user_id);
      formData.append('to_id',this.state.users[this.state.selectedModel]._id);
      
      
      console.log("----",formData)
    
      this.setState({loading_status:true})
    
               let url = urls.BASE_URL +'api/send_friend_request'
                    fetch(url, {
                    method: 'POST',
                    body:formData
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("----",responseJson)
                          this.setState({loading_status:false})

                         
                          if(responseJson.status){
                           
                            showMessage(responseJson.message,false)
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          showMessage(error.message)
                        });
    
    
    
    }

    async requestLocationPermission(){
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'D8 App',
            'message': 'D8 App access to your location '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    
    
          Geolocation.getCurrentPosition(
            position => {
              //ToastAndroid.show(JSON.stringify(position), ToastAndroid.SHORT)
              this.setState({
    
               latitude: position.coords.latitude,
               longitude: position.coords.longitude,
    
    
              }, () => this._updateUserLocation());
             
    
            },
            error => {
            //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);
    
              // Platform.OS === 'android'
              // ? ToastAndroid.show("Check your internet speed connection", ToastAndroid.SHORT)
              // : Alert.alert("Check your internet speed connection")
    
                console.log("ERROR LOCATION----",error)
              this.setState({loading_status:false})
            },
            { enableHighAccuracy: false, timeout: 10000}
          );
    
    
        } else {
         // console.log("location permission denied")
         Alert.alert("location permission denied");
          //alert("Location permission denied");
        }
      } catch (err) {
        //console.warn(err)
          Alert.alert(JSON.stringify(err.message));
      }
    }
    
    
    async requestLocationPermissionIOS(){
    
            Geolocation.getCurrentPosition(
                position => {
                  this.setState({
    
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
    
    
                  } , () =>   this._updateUserLocation()) ;
    
                },
                error => {
                //  ToastAndroid.show("Check your internet speed connection",ToastAndroid.SHORT);
    
                console.log("ERROR LOCATION----",error)
    
    
                  this.setState({loading_status:false})
                },
                { enableHighAccuracy: false, timeout: 10000}
              );
        }


    _fetchUsers = async () =>{
      this.setState({user_loading_status:true})
        var formData = new FormData();

        formData.append('user_id',this.props.user.user_id);
        formData.append('lat',this.state.latitude);
        formData.append('long',this.state.longitude);

        console.log("----",formData)
  
                 let url = urls.BASE_URL +'api/get_all_users'
                      fetch(url, {
                      method: 'POST',
                      body:formData
  
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            //console.log("----",responseJson)
                           
                            this.setState({user_loading_status:false})
                            if(responseJson.status){
                            
                              var users = responseJson.result
                             
                              this.setState({users:users})

                              this.setState({swipedAllCards:false,selectedModel:0})
  
                            }
  
  
                          else{
                            showMessage(responseJson.message)
                          }
  
                        }
                          ).catch((error) => {
  
                            this.setState({user_loading_status:false})
                            showMessage(error.message)
                          });
  
  
  
    }
    _applyFilters =(formData) =>{
      console.log("formData------",JSON.stringify(formData))

      this.setState({loading_status:'true'})

      let url = urls.BASE_URL +'api/filters'
      fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
        //'Content-Type': 'application/json'
      },
      body: formData

      }).then((response) => response.json())
          .then((responseJson) => {
            console.log("RESSSSSSS",responseJson)
            this.setState({loading_status:false})
            if(responseJson.status && responseJson.result.length> 0){
              var users = responseJson.result
             
              this.setState({users:users})

              this.setState({swipedAllCards:false,selectedModel:0})
              //showMessage(responseJson.message,false)
            }
            else{
              showMessage(responseJson.message)
            }
           

        }
          ).catch((error) => {
            this.setState({loading_status:false})

            console.log("Errror",JSON.stringify(error.message))
            Platform.OS === 'android' 
            ?  ToastAndroid.show("Connection Error!", ToastAndroid.SHORT)
            : Alert.alert("Connection Error!")
          });

    }
    _filters = async (data) =>{

       //DATA {"weight":[40,150],"height":[100,220],"age":[18,90],"male":false,"female":true,
      //"location":"133165,133236,134012,","ethnicity":"","body_type":""}
     
     

      //weight
      var weight = data['weight']
      //height
      var height = data['height']
      //age
      var age = data['age']

      //location
      var location = data['location'].split(',')
      //ethnicity
      var ethnicity = data['ethnicity'].split(',')
      //body_type
      var body_type = data['body_type'].split(',')

      //gender 
      var arr=[];
      var genderString= ''
      var male = data['male']
      var female = data['female']
    
      if(male == true && female == true){
          arr=['male','female']
          genderString = 'male,female'
      }
      else if(male == true && female == false){
        arr=['male']
        genderString = 'male'
      }
    
      else if(male == false && female == true ){
        arr=['female']
        genderString = 'female'
      }

      var formData = new FormData();

      
    //   for(var i = 0 ; i < arr.length ; i++){
    //     formData.append('gender[' +i+']',arr[i]);
    //    }

    // for(var i = 0 ; i < data['height'].length ; i++){
    //   formData.append('height[' +i+']',data['height'][i]);
    // }

    // for(var i = 0 ; i < data['weight'].length ; i++){
    //   formData.append('weight[' +i+']',data['weight'][i]);
    // }
    // for(var i = 0 ; i < location.length ; i++){
    //   formData.append('cities[' +i+']',data['cities'][i]);
    // }
    // for(var i = 0 ; i < data['age'].length ; i++){
    //   formData.append('age[' +i+']',data['age'][i]);
    // }

    formData.append("user_id",this.props.user.user_id)
      formData.append("weight",weight[0] + ',' + weight[1])
      formData.append("height",height[0] + ',' + height[1])
      formData.append("age",age[0] + ',' + age[1])
      formData.append("gender",genderString)
      {
        data['body_type'] == "" ?
        null
        :
        formData.append("body_type",data['body_type'])
      }
      {
        data['location'] == "" ?
        null
        :
        formData.append("location",data['location'])
      }
      {
        data['ethnicity'] == "" ?
        null
        :
        formData.append("ethnicity",data['ethnicity'])
      }


      //formData.append("age",age)

        
     

      // var formData = {
      //   ethnicity:ethnicity.toString(),
      //   weight:weight.toString()

      // }

      
 
        //  console.log("formData------",JSON.stringify(formData))
    
        this._applyFilters(formData)

        
    
                    
    
    
    }
    onSelectFilters = data => {
      //console.log("DATA",JSON.stringify(data))
     
      this.setState({filters_data:data})
      this._filters(data)
    };


    writeUserData( user_id, name,image,email,deviceToken){
      
        firebase.database().ref('Users/'+ user_id).set({
          user_id,
          name,
          image,
          email ,
          device_token:deviceToken,
          'active':'online'
        }).then((data)=>{
            //success callback
            //console.log('data written---' , JSON.stringify(data))
        }).catch((error)=>{
            //error callback
         console.log('error ' , error)
        })
      
    
      }
    changeStatus(status){
        //status == 0 ..offline
        //status == 1 .. online
        firebase.database().ref('Users/'+ this.props.user.user_id).update({
          'active' : status == 1 ? 'online' :'offline',
        });
    }

    _handleAppStateChange = (nextAppState) => {
        if (
          //this.state.appState.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          //console.log('App has come to the foreground!');
          this.changeStatus(1)
        }
        else{
         // console.log('App has come to the background!');
          this.changeStatus(0)
        }
        //this.setState({appState: nextAppState});
      };

    initialize(id){
        //this.setState({chatCount:0})
     
          //It will read data once from the `Users` object and print it on console. 
          //If we want to get data whenever there is any change in it, 
          //we can use on instead of once
        firebase.database().ref('recents/').orderByChild('timestamp').once('value',  (snapshot)=> {
        //console.log("SNAPSHOT",snapshot.val())

           var count = 0 
          snapshot.forEach((childSnapshot) =>{
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();
            var str = childKey;
            var res = str.split("_");

            if(res[0] == id || res[1] == id){


              var foo = `${this.props.user.user_id}_read`
              if(childData[foo]) {
                //means already read last message
              }
              else{
                // this.setState(previousState => ({
                //   chatCount: previousState.chatCount +1
                //  }));
                count = count + 1
              }

              if(count  === this.state.chatCount){
                  //to avoid unneccesary re render
              }
              else{
                //console.log('hahahh',this.state.chatCount)
                this.setState(previousState => ({
                  chats: [...previousState.chats, childData],
                  chatCount:count
                 
                 }));
              }
             
            
             
     
            }
     
            
          });
          
      }, (error) => {
        console.log("ERRROR-----------",error)
     });
     
   
     
      }

    componentWillMount = async () =>{
      AppState.addEventListener('change', this._handleAppStateChange);

      this._fetchUsers()
      
      AsyncStorage.getItem('lang')
      .then((item) => {
                if (item) {
                  I18n.locale = item.toString()
              }
              else {
                   I18n.locale = 'en'
                }
      });  


      Platform.OS === 'android'
      ?  await this.requestLocationPermission()
      :  await this.requestLocationPermissionIOS()
      console.log("USER LOGGED IN ---->",JSON.stringify(this.props.user))
  
      getFCMToken().then(response => {
  
        this.setState({
          deviceToken : response
        },()=>{
         
           //save user data to database for firebase
       this.writeUserData(this.props.user.user_id,
        this.props.user.name,
        this.props.user.image,
        this.props.user.email,
        response)
        })
        //console.log("HOME FCM TOKEN----------",JSON.stringify(response))
    });

    //this.initialize(this.props.user.user_id)
    this.interval = setInterval(() => this.initialize(this.props.user.user_id), 1000);

  
  }
  

  componentWillUnmount(){
    AppState.removeEventListener('change', this._handleAppStateChange);
    clearInterval(this.interval);
  }




    renderCard = (card, index) => {
     // console.log('renderCard',card)
      //below condition check is necessary as setstate was not workign after fetching data
      if(card){
        return (
          <SwiperComponent
          card ={card}
          index = {index}
          />
        )
      }
    };
  
    onSwiped = (type,index) => {
      //console.log(`on swiped ${index}`)
      // this.setState({
      //   selectedModel : index
      // })
      this.setState(prevState => ({ selectedModel: index + 1 }));
    }
  
    onSwipedAllCards = () => {
      this.setState({
        swipedAllCards: true
      })
    };
   
  
    swipeLeft = () => {
      this.swiper.swipeLeft()
    };

  

    componentDidMount = async () => {
      await this._checkSubscriptions()
     
      
     
    }


    buttonClick=()=>{
      this.winkModal.setState({visibility:false})
    }

    _goToChats =() =>{
     if(this.props.user.user_id == this.state.users[this.state.selectedModel]._id){
        showMessage('You cant chat with yourself');
        return
     }
     else{
      let user = this.state.users[this.state.selectedModel]
      let obj={
        'to_id':user._id,
        'to_name':user.display_name,
        'to_image':user.user_image,
        'to_status':'offline',
        'to_typing':false
      }
      this.props.navigation.navigate("Chats",{result:obj})
     }
    }

    _cancel =()=>{
        this.locationRangeSheet.close()
    }

    _searchRange =(locationRange)=>{
      console.log('locationRange---',parseInt(locationRange))
      var range = parseInt(locationRange)
      this.locationRangeSheet.close()
      this._getNearByUsers(range)
    }


    _chatWithUser =() =>{
      this.RBSheet.close()
      let obj={
        'to_id':this.state.users[this.state.selectedModel]._id,
        'to_name':this.state.users[this.state.selectedModel].display_name,
        'to_image':this.state.users[this.state.selectedModel].user_image,
        'to_status':'offline',
        'to_typing':false
      }
      this.props.navigation.navigate("Chats",{result:obj})
    }


   
    render() {
      
     
      const RequestSheet = () =>{
        return(
          <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={230}
          closeOnDragDown
          duration={240}
          customStyles={{
            container: {
            
              alignItems: "center",
              backgroundColor:colors.BLACK,
              padding:10,
              borderTopLeftRadius:15,
            borderTopRightRadius:15
            }
          }}
          animationType='fade'
          minClosingHeight={10}
        >
         <Text 
         style={{color:colors.COLOR_PRIMARY,textAlign:'center',fontWeight:'bold'}}>
         {I18n.t('request_text')}</Text>

          {/** bottom three buttons */}

          <View style={{flexDirection:'row',justifyContent:'space-evenly',alignItems:'center',width:'100%',marginTop:20}}>
            
          <TouchableOpacity onPress={()=> {
              this._chatWithUser()
            // console.log(JSON.stringify(this.state))
            }}>
            <View style={{justifyContent:'center',alignItems:'center'}}>
            <Text style={{color:colors.COLOR_PRIMARY,fontWeight:'bold',fontSize:9}}>{I18n.t('chat_first')}</Text>
            <Image source={require('../assets/chat-heart.png')} style={{width:50,height:50}} 
                resizeMode='contain'/>
            </View>
            </TouchableOpacity>


              <TouchableOpacity  onPress={()=> {
                AsyncStorage.setItem('request','true'),
                this._sendFriendRequest()
              }}>
              <View style={{justifyContent:'center',alignItems:'center'}}>
              <Text style={{color:'#38ac2c',fontWeight:'bold',fontSize:9,width:90,textAlign:'center'}}>  {I18n.t('accept_and_dont_show')}</Text>
              <Image source={require('../assets/d8-green.png')} style={{width:35,height:35}} 
                  resizeMode='contain'/>
              </View>
              </TouchableOpacity>
          </View>

           {/** bottom three buttons  end*/}


        </RBSheet>
        )
      }


      if(this.state.user_loading_status){
        return(
        <LogoProgressBar
        title ={I18n.t('please_wait_loading')}
        />
        )
      }
        return (
          <View style={styles.safeAreaContainer}>
          <RequestSheet/>
          <LocationRangeSheet
            inputRef={ref => this.locationRangeSheet = ref}
            ref={ref => this.locationBottomSheet = ref}
            cancel ={this._cancel}
            search={this._searchRange}
        />

          <CustomModalOk
          handler={this.handler}
          buttonClick={this.buttonClick}
         
          title={this.state.users[this.state.selectedModel] == undefined ? '' : this.state.users[this.state.selectedModel].display_name}
          buttonText={I18n.t('ok')}

          description={I18n.t('you_have_winked')}
          ref={ref => this.winkModal = ref}/>

          <CustomModalOptions 
           ref={ref => this.winkedModal = ref}/>

           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
              <Header
              statusBarProps={{ barStyle: 'light-content' }}
              barStyle="light-content" // or directly

               leftComponent={
               <TouchableOpacity onPress={()=>{
                    this.props.navigation.navigate("ProfileMenu")
                
                }}>
              <Image source={require('../assets/user-menu.png')} style={{width:30,height:30}} 
              resizeMode='contain'/>

              </TouchableOpacity>
              }
              rightComponent={
                <View style={{flexDirection:'row'}}>
                <TouchableOpacity onPress={()=>{
                  this.props.navigation.navigate("Filters",
                   { onSelect: this.onSelectFilters,
                    filters_data:this.state.filters_data
                   })
              
              }}>
            <Image source={require('../assets/filter.png')} style={{width:25,height:25,marginRight:10}} 
            resizeMode='contain'/>



            </TouchableOpacity>

                    <TouchableOpacity
                      onPress={()=>{
                      this.props.navigation.navigate("ChatLists")
                      
                      }}>
                    <Image source={require('../assets/chat.png')} style={{width:30,height:30}} 
                    resizeMode='contain'/>

                    <View style={{padding:3,borderRadius:20,
                    backgroundColor:'red',position:'absolute',bottom:14,left:15,fontSize:11,paddingLeft:6,paddingRight:6}}>
                                <Text style={{color:'white',fontSize:11}}>{this.state.chatCount}</Text>


                    </View>

      
                    </TouchableOpacity>

              </View>
              
              }


        


              statusBarProps={{ translucent: Platform.OS == 'android' ? true : false}}
            // centerComponent={{ text: "Add Property", style: { color: '#fff' } }}
              //outerContainerStyles={{height: Platform.OS === 'ios' ? 70 :  70 - 24}}
              containerStyle={{
                //to remove bottom border
                borderBottomColor: colors.COLOR_SECONDARY,
              backgroundColor: colors.COLOR_SECONDARY,
          
          
            }}
       />
            <View>
              {/* <Text style={{color:'white'}}>TOOOOOPPPP</Text> */}

           


              <View style={styles.container}>
            
            {
              this.state.swipedAllCards ?
              <View>
                  <Image source={require('../assets/heart-eyes.png')} style={{width:150,height:150,marginBottom:20}} 
                  resizeMode='contain'/>

                  <TouchableOpacity onPress={()=> {this._fetchUsers()}}
                  style={{paddingBottom:7,paddingTop:7,
                  paddingLeft:14,paddingRight:14,backgroundColor:colors.COLOR_PRIMARY,borderRadius:15}}>
                  <Text style={{
                    alignSelf:'center',
                    fontWeight:'bold'
                  }}>{I18n.t('reset')}</Text>

                  </TouchableOpacity>

              
              </View>

            
              :
              <Swiper
                ref={swiper => {
                  this.swiper = swiper
                  }}
                  backgroundColor={colors.BLACK}
                 // childrenOnTop={true}
                  //backgroundColor='red'
                  showSecondCard={true}
                  verticalSwipe={true}
                  horizontalSwipe={false}
                  containerStyle={{height:'100%',
                  justifyContent:'center',alignItems:'center'}}
                  style={{backgroundColor:'blue',height:20}}
                  onSwiped={(index) => this.onSwiped('general',index)}
                  onSwipedLeft={(index) => this.onSwiped('left',index)}
                  onSwipedRight={(index) => this.onSwiped('right',index)}
                  onSwipedTop={(index) => this.onSwiped('top',index)}
                  onSwipedBottom={(index) => this.onSwiped('bottom',index)}
                  //onTapCard={this.swipeLeft}
                  //cards={pager}
                  cards={this.state.users}
                  cardIndex={this.state.cardIndex}
                  cardVerticalMargin={80}
                  renderCard={this.renderCard}
                  onSwipedAll={this.onSwipedAllCards}
                  stackSize={3}
                  stackSeparation={-25}
                  overlayLabels={{
                 
                    left: {
                      title: 'NOPE',
                      style: {
                        label: {
                          backgroundColor: 'black',
                          borderColor: 'black',
                          color: 'white',
                          borderWidth: 1
                        },
                        wrapper: {
                          flexDirection: 'column',
                          alignItems: 'flex-end',
                          justifyContent: 'flex-start',
                          marginTop: 30,
                          marginLeft: -30
                        }
                      }
                    },
                    right: {
                      title: 'LIKE',
                      style: {
                        label: {
                          backgroundColor: 'black',
                          borderColor: 'black',
                          color: 'white',
                          borderWidth: 1
                        },
                        wrapper: {
                          flexDirection: 'column',
                          alignItems: 'flex-start',
                          justifyContent: 'flex-start',
                          marginTop: 30,
                          marginLeft: 30
                        }
                      }
                    },
                 
                  }}
                  animateOverlayLabelsOpacity
                  animateCardOpacity
                  swipeBackCard
        >
          {/* <Button onPress={() => this.swiper.swipeBack()} title='Swipe Back' /> */}
        </Swiper>

            }
           
             
       
             </View>

            <View style={{width:dimensions.SCREEN_WIDTH * 0.9,
            alignSelf:'center',
            flexDirection:'row',
               justifyContent:'space-between',
            alignItems:'center',
           }}>

            <TouchableOpacity onPress={()=>{this.locationRangeSheet.open()}}>
            <Image source={require('../assets/map-icon.png')} style={{width:40,height:40}} 
              resizeMode='contain'/>

            </TouchableOpacity>

            <TouchableOpacity onPress={()=>{

             
                if(this.state.users[this.state.selectedModel]){

                  this._checkSubscriptions().then(data => {
                  
                    if(data) {

                      AsyncStorage.getItem('request').
                      then(value => {
                        console.log('_________________________',value)
                        if(value) this._sendFriendRequest()
                        else this.RBSheet.open();
                      })
                  
                    }
                    else  this.props.navigation.navigate('Subscriptions')

                  })
                }
           
            
             
                 // ToastAndroid.show(JSON.stringify(this.state.users[this.state.selectedModel]),ToastAndroid.SHORT)
            }}>
            <Image source={require('../assets/icon2.png')} style={{width:55,height:55}} 
              resizeMode='contain'/>

            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{

              if(this.state.users[this.state.selectedModel]){

              this._checkSubscriptions().then(data => {
               
                if(data) {
                  this._goToChats()
                }
                 else  this.props.navigation.navigate('Subscriptions')

              })
              }



              
             }}>
            <Image source={require('../assets/icon3.png')} style={{width:55,height:55}} 
              resizeMode='contain'/>

            </TouchableOpacity>

            <TouchableOpacity onPress={()=>{
              //console.log('//////',this.state.users[this.state.selectedModel]._id)
              if(this.state.users[this.state.selectedModel]){
                this._winkUser(this.state.users[this.state.selectedModel]._id)

              }
             }}>
            <Image source={require('../assets/wink-golden.png')} style={{width:40,height:40}} 
              resizeMode='contain'/>

            </TouchableOpacity>

            </View>

            
                     
           </View>
         

           
           { this.state.loading_status && <ProgressBar/> }

			   

          
          </View>

        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,

	};
};

const mapDispatchToProps = dispatch => {
  return {

    change: (userinfo) => dispatch(changeUser(userinfo)),
  


  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ModelsSwiper);

let styles = StyleSheet.create({
  safeAreaContainer:{
   
    backgroundColor:'black',
    flex:1
   
  },
  container:{
   
   
    padding:10,
    height : dimensions.SCREEN_HEIGHT  * 0.77,
    alignItems:'center',
    backgroundColor:colors.BLACK,
    justifyContent:'center'
   
    
    
  },
  card: {
      height:dimensions.SCREEN_HEIGHT * 0.6,
      borderRadius: 4,
      borderWidth: 2,
      borderColor:'white',
      justifyContent: 'center',
      backgroundColor: colors.COLOR_PRIMARY,
    },
    text: {
      textAlign: 'center',
      fontSize: 50,
      backgroundColor: 'transparent'
    },
    done: {
      textAlign: 'center',
      fontSize: 30,
      color: 'white',
      backgroundColor: 'transparent'
    },
    userLoadingContainer:{
      height:dimensions.SCREEN_HEIGHT * 1,
      width:dimensions.SCREEN_WIDTH * 1,
      backgroundColor:colors.BLACK,
      justifyContent:'center',
      alignItems:'center'
    }
 

}
)



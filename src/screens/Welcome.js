import React, {Component} from 'react';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel from 'react-native-banner-carousel';
import I18n from '../i18n';

import BannerComponent from '../components/BannerComponent';

import {  colors,urls,dimensions } from '../Constants';



export default class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          pager : [
            {
              "text":I18n.t('our_speedd8'),
              "image": require('../assets/welcome1.png')
            },
            {
              "text": I18n.t('our_speedd8'),
              "image": require('../assets/welcome2.png')
            },
          
          ]

        }

    }


   



componentWillMount(){
  AsyncStorage.getItem('lang')
  .then((item) => {
            if (item) {
              I18n.locale = item.toString()
          }
          else {
               I18n.locale = 'en'
            }
  });  
  
}
_goToLogin(){
  this.props.navigation.navigate("Login")
}

renderPage(data, index) {
  return (
     <BannerComponent data={data} index={index} />
  );
}

   
    render() {
        return (
          <SafeAreaView style={styles.container}>
           <StatusBar
               barStyle="dark-content" //dark-content //light-content
           />
         

            {/* top container */}

                        <View style={styles.topContainer}>

                        <Carousel
                        style={{justifyContent:'center'}}
                        autoplay
                        autoplayTimeout={4000}
                        loop
                        index={0}
                        pageSize={Dimensions.get('window').width}
                       
                        pageIndicatorStyle={{backgroundColor:'black'}}
                        activePageIndicatorStyle={{backgroundColor:'yellow'}}
                        
                        >

                          {this.state.pager.map((data, index) => this.renderPage(data, index))}
                      
                      
                        </Carousel>



                        </View>

                         {/* top container end */}



                          {/* bottom container  */}


                   <View style={styles.bottomContainer}>

                    <View style={{
                      flexDirection:'row',
                      marginBottom:10,
                      alignItems:'flex-end'
                    }}>
                      <Text style={{
                        fontSize:25,
                        color:colors.COLOR_PRIMARY,
                        fontWeight:'bold'
                      }}>{I18n.t('hi')},</Text>
                      <Text style={{
                        marginBottom:4,
                        marginLeft:4,
                        fontWeight:'bold'
                      }}></Text>
                    </View>


                    <View style={{
                      flexDirection:'row',
                      alignItems:'center'
                      
                    }}>
                      <Text style={{flex:5,fontWeight:'bold'}}>{I18n.t('welcome_to_d8')}.</Text>
                    
                      <TouchableOpacity onPress={()=> this._goToLogin()}
                      style={{flex:2,elevation:10}}>
                      <Image source={require('../assets/button.png')} 
                      style={{height:100,width:100}} resizeMode='contain'/>

                      </TouchableOpacity>
                    </View>


                     

              

            </View>
             {/* bottom container end */}
           

			   

          
          </SafeAreaView>

        )
    }
}

let styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor:colors.COLOR_PRIMARY
   
  },
  topContainer:{
    flex:5,
    backgroundColor:colors.COLOR_PRIMARY,
    justifyContent:'center',
    alignItems:'center',padding:20
  },
  bottomContainer:{
    flex:1.5,
    backgroundColor:'white',
    padding:20
  }

}
)



//HINDI LANGUAGE

export default {
  //add story
  add_story: 'कहानी जोड़ें',

//block user
  block_user: 'ब्लॉक का उपयोग करेंr',
  block: 'खंड मैथा ',

//change language
  change_language:'भाषा बदलो',
  select_a_language:'एक भाषा का चयन करें और इसे पूरे ऐप के लिए डिफ़ॉल्ट भाषा के रूप में बनाएं',
  save_language:'भाषा बचाओ',

  //Chat Lists
  chat_lists: 'चैट सूची',
  typing:'टाइपिंग',

  //create profile
    save:'सहेजें',
    choose_image_source:'छवि स्रोत चुनें',
    choose_video_source:'वीडियो स्रोत चुनें',
    camera:'कैमरा',
    gallery:'गेलरी',
    choose_display_picture_making:"आप के बारे में अधिक जानने के लिए अन्य D8 बनाने वाली अपनी डिस्प्ले पिक्चर चुनें",
    display_name:'प्रदर्शित होने वाला नाम',
    profession:'व्यवसाय',
    age:'आयु',
    height:'ऊंचाई',
    weight:'वजन',
    about:'के बारे में',
    ethnicities:'जातियों',
    body_types:'शरीर के प्रकार',
    create_profile:'प्रोफ़ाइल बनाने',

     //edit profile
     save:'सहेजें',
     name:'नाम',
     edit_profile:'प्रोफ़ाइल संपादित करें',
     email:'ईमेल',
     ethnicity:'जातीयता',
     gender:'लिंग',
     interested_gender:"इच्छुक लिंग",
     country:'देश',
     state:'राज्य',
     city:'शहर',
     add_document:'D8 गोल्ड टिक के लिए वेरिफिकेशन डॉक्यूमेंट अपलोड करें',
     document_added:'दस्तावेज़ जोड़ा गया',
     add_video:'वीडियो जोड़ें',
     video_added:'वीडियो जोड़ा गया',

  

  //enter OTP
  otp:'OTP',
  send_otp:'संदेश OTP',
  enter_otp:'दर्ज OTP',
  enter_otp_email:'अपने ईमेल पर भेजे गए ओटीपी को दर्ज करें',
  verify_otp:'सत्यापित करें OTP',




  //enter password
  forgot_password:'पासवर्ड भूल गए',
  please_update_your_password:'कृपया अपना पासवर्ड अपडेट करें',
  new_password:'नया पासवर्ड',
  confirm_password:'पुष्टि पासवर्ड',
  update:'अपडेट करें',


  //Forgot password
  enter_email_address_used_to_register:'आपके द्वारा पंजीकृत ईमेल पता दर्ज करें',


    //filters
    choose_desired_location_for_d8:"उपलब्ध D8 की सूची प्राप्त करने के लिए वांछित शहर चुनें",
    cities:'शहरों',
    filters:'फिल्टर',
    change:'परिवर्तन',
    states:'राज्य',
    countries:'देश',
    age:'आयु',
    close:'बंद करे',
    male:'पुरुष',
    female:'महिला',
    submit:'प्रस्तुत',
    height_cms:'लम्बाई (सेंटीमीटर मे)',
    weight_kgs:'वजन (किलोग्राम में)',
    about_bio:'जैव के बारे में (वैकल्पिक)',
    ethnicity:'जातीयता',
    body_type:'शरीर के प्रकार',

    
    //friends
    friends:'दोस्त',
    see_pending_requests:'लंबित स्पीड D8 अनुरोध देखें',

    //friend story
    story:'कहानी',

    //interested gender
    please_select_interested_gender:'कृपया अपने इच्छुक लिंग का चयन करें',
    //gender
    please_select_your_gender:'कृपया अपना लिंग चुनें',

    //login
    user_name:'उपयोगकर्ता नाम',
    password:'कुंजिका',
    login_with_social_account:'सोशल अकाउंट से लॉगिन करें',
    login:'लॉग इन करें',
    dont_have_account:"खाता नहीं है",
    sign_up_now:"अभी साइनअप करें",

     //register
     register:'रजिस्टर करें',

    //models swiper
    reset:'रीसेट',
    request_text:"स्पीड D8 का अनुरोध करके आप इस व्यक्ति को बता रहे हैं कि आपका तैयार और मिलने को तैयार है। यदि आप मिलने के लिए तैयार नहीं हैं, तो पहले उन्हें जानने के लिए चैट बटन का उपयोग करें",
    accept_and_dont_show:"स्वीकार करें और इसे फिर से न दिखाएं",
    please_wait_loading:"कृपया प्रतीक्षा करें जब हम आपके लिए D8 लोड कर रहे हैं ...",
    you_have_winked:"आपने विंक किया है।",

    //
    my_profile:' मेरी प्रोफाइल',
    verified_user:'सत्यापित उपयोगकर्ता',
    yes:'हाँ',
    no:'नहीं',
    years:'वर्षों',
    cms:'सेमी',
    lbs:'एलबीएस',
    location:'स्थान',

    //notifiactoin
    notifications:'सूचनाएं',

    //payment-mode
    payment:'भुगतान',
    payment_mode:'भुगतान का प्रकार',
    payment_type_choose:"लगभग वहाँ, अपने D8 को पूरा करने के लिए भुगतान प्रकार चुनें",

    //pending friend
    accept_text:" 'स्पीड डी 8' के अनुरोध को स्वीकार करके, आप इस व्यक्ति को बता रहे हैं कि आपका मिलने के लिए तैयार है। यदि आप मिलने के लिए तैयार नहीं हैं, तो आप नीचे दिए गए 'चैट फर्स्ट' बटन को देख सकते हैं, या उन्हें 'नोप' मार सकते हैं। अलविदा कहने का बटन।",
    accept:'स्वीकार करना',
    chat_first:'पहले चैट करें',
    nope:'नहीं',
    speed_d8_requests:'स्पीड D8 अनुरोध',


    //profile menu
    stories:'कहानियों',
    profile:'प्रोफ़ाइल',
    chats:'चैट',
    buy_subscriptions:'सदस्यता खरीदें',
    change_language:'भाषा बदलो',
    reset_password:'पासवर्ड रीसेट',
    logout:'लॉग आउट',
    indiscreet_mode:'Indiscreet मोड',
    discreet_mode:'विवेकशील विधा',
    logout:'लॉग आउट',
    are_sure_to_logout:'क्या आप लॉगआउट करना सुनिश्चित कर रहे हैं?',


    //register 
    app_user_name:'ऐप उपयोगकर्ता नाम',
    accept_terms_and_conditions:'नियम और शर्त स्वीकार करें',
    already_have_an_account :'पहले से ही एक खाता है ?',
    sign_in_now:'अब साइन इन करो',
    register_with_social_account:'सोशल अकाउंट से रजिस्टर करें',


   //reset password
  reset_password:'पासवर्ड रीसेट',
  please_update_your_password:'कृपया अपना पासवर्ड अपडेट करें',
  old_password:'पुराना पासवर्ड',
  new_password:'नया पासवर्ड',
  confirm_password:'पुष्टि पासवर्ड',
  reset:'रीसेट',


  //stories
  my_story:'मेरी कहानी',
  friends_story:'दोस्तों कहानी',
  stories:'कहानियों',
  story_posted:'कहानी पोस्ट की गई',
  no_story_found:'कोई कहानी नहीं मिली',


  ///subscripti
  subscriptions:'सदस्यता',
  choose_subscription:'सदस्यता चुनें',
  yes_subscription_one:"आपने एक लोकप्रिय विकल्प प्रदान किया है",
  yes_subscription_two:"अवसरों की",
  no_subscription_one:"ओह, आपके पास सदस्यता नहीं है जल्दी करो और तुरंत D8ing शुरू करने के लिए एक हो जाओ",
  no_subscription_two:"जल्दी पहुंचने के लिए अपनी पसंद करें और तुरंत D8ing शुरू करें",
  select_subscription:"कृपया सदस्यता का चयन करें ",

  //update ountry
  choose_country:'देश चुनें',

  //welcome
lets_get_started:"आएँ शुरू करें",
our_speedd8:"हमारा SpeedD8 बटन साहसिक सदस्यों को \n अब मिलने की अनुमति देता है।",
hi:"नमस्ते",
welcome_to_d8:"नमस्ते, D8 में आपका स्वागत है। सभी के लिए सहायक और प्रगतिशील D8ing ऐप",



//filterlocationsheet
choose_location_search:"D8 की खोज के लिए स्थान चुनें",
ok:'ठीक है',

//location sheet chat
choose_location_chat:"वह स्थान भेजें जहां आप मिलना चाहते हैं।",
choose_range:"अपने पास D8 के लिए सीमा का चयन करें ",
distance:"दूरी",
search:'खोज',
cancel:'रद्द करना',

//story dialoge
video:'वीडियो',
create_story_to_share:'अपने स्वीकृत मित्रों के साथ साझा करने के लिए एक कहानी बनाएँ।',
choose_story_image:'अन्य D8s के साथ अपनी छवि कहानी ans साझा चुनें',
choose_story_video:'अपनी वीडियो कहानी चुनें और अन्य D8s के साथ साझा करें',
choose_story_source:'कहानी स्रोत चुनें',
image:'छवि',

//settings
settings:'समायोजन',
delete_account:'खाता हटा दो',
delete_account_text:"आपका खाता हटाना स्थायी है और मित्र, अनुरोध, चैट और प्रोफ़ाइल सेटिंग्स सहित सभी सामग्री को हटा देगा। क्या आप इस खाते को हटाने के लिए सुनिश्चित हैं?"







  
};

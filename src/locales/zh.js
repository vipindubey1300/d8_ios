//MANDARIN/CHINESE LANGUAGE

export default {
  //add story
  add_story: '添加',

//block user
  block_user: '阻止用户',
  block: '块 ',

//change language
  change_language:'改变语言',
  select_a_language:'选择一种语言并将其设置为整个应用程序的默认语言',
  save_language:'储存语言',

  //Chat Lists
  chat_lists: '聊天清单',
  typing:'打字',

  //create profile
    save:'保存',
    choose_image_source:'选择图像来源',
    choose_video_source:'选择视频源',
    camera:'相机',
    gallery:'画廊',
    choose_display_picture_making:"选择制作其他D8的显示器图片以了解更多信息",
    display_name:'显示名称',
    profession:'职业',
    age:'年龄',
    height:'高度',
    weight:'重量',
    about:'关于',
    ethnicities:'种族',
    body_types:'身体类型',
    create_profile:'创建个人资料',

     //edit profile
     save:'保存',
     name:'名称',
     edit_profile:'编辑个人资料',
     email:'电子邮件',
     ethnicity:'种族',
     gender:'性别',
     interested_gender:"感兴趣的性别",
     country:'国家',
     state:'州',
     city:'市',
     add_document:'上传D8 Gold Tick的验证文件',
     document_added:'文件已新增',
     add_video:'新增影片',
     video_added:'视频已添加',

  

  //enter OTP
  otp:'一次性密码',
  send_otp:'发送OTP',
  enter_otp:'输入一次性密码',
  enter_otp_email:'输入通过电子邮件发送的OTP',
  verify_otp:'验证OTP',




  //enter password
  forgot_password:'忘记密码',
  please_update_your_password:'请更新您的密码',
  new_password:'新密码',
  confirm_password:'确认密码',
  update:'更新资料',


  //Forgot password
  enter_email_address_used_to_register:'输入您用来注册的电子邮件地址',


    //filters
    choose_desired_location_for_d8:"选择所需的城市以获取可用D8的列表",
    cities:'城市',
    filters:'筛选器',
    change:'更改',
    states:'状态',
    countries:'国别',
    age:'年龄',
    close:'关',
    male:'男',
    female:'女',
    submit:'提交',
    height_cms:'身高（厘米）',
    weight_kgs:'重量（公斤）',
    about_bio:'关于生物（可选）',
    ethnicity:'种族',
    body_type:'体型',

    
    //friends
    friends:'友人',
    see_pending_requests:'查看待处理的Speed D8请求',

    //friend story
    story:'故事',

    //interested gender
    please_select_interested_gender:'请选择您感兴趣的性别',
    //gender
    please_select_your_gender:'请选择您的性别',

    //login
    user_name:'用户名',
    password:'密码',
    login_with_social_account:'使用社交帐户登录',
    login:'登录',
    dont_have_account:"没有帐号",
    sign_up_now:"立即注册",

    //register
    register:'寄存器',

    //models swiper
    reset:'重启',
    request_text:"通过请求Speed D8，您告诉此人您已经准备好并愿意见面。 如果您还没准备好见面，请改用“聊天”按钮先了解他们",
    accept_and_dont_show:"接受，不再显示",
    please_wait_loading:"请稍候，我们正在为您加载D8 ...",
    you_have_winked:"你眨了眨眼。",

    //my profile
    my_profile:'我的简历',
    verified_user:'验证用户',
    yes:'是',
    no:'没有',
    years:'年份',
    cms:'厘米',
    lbs:'磅',
    location:'位置',

    //notifiactoin
    notifications:'通知事项',

    //payment-mode
    payment:'付款',
    payment_mode:'付款方式',
    payment_type_choose:"快到了，选择付款方式以满足您的D8",

    //pending friend
    accept_text:" 通过接受“ Speed D8”请求，您告诉此人您已经准备好见面。如果您还没准备好见面，则可以单击下面的“聊天优先”按钮将其签出，或者单击“否” 按钮说再见。",
    accept:'接受',
    chat_first:'首先聊天',
    nope:'不',
    speed_d8_requests:'速度D8请求',


    //profile menu
    stories:'故事',
    profile:'轮廓',
    chats:'聊天室',
    buy_subscriptions:'购买订阅',
    change_language:'改变语言',
    reset_password:'重设密码',
    logout:'登出',
    indiscreet_mode:'不谨慎模式',
    discreet_mode:'谨慎模式',
    logout:'登出',
    are_sure_to_logout:'您确定要注销吗？',


    //register 
    app_user_name:'应用程序用户名',
    accept_terms_and_conditions:'接受条款和条件',
    already_have_an_account :'已经有帐号了？',
    sign_in_now:'现在登入',
    register_with_social_account:'用社交帐号注册',


   //reset password
  reset_password:'重设密码',
  please_update_your_password:'请更新您的密码',
  old_password:'旧密码',
  new_password:'新密码',
  confirm_password:'确认密码',
  reset:'重启',


  //stories
  my_story:'我的故事',
  friends_story:'朋友故事',
  stories:'故事',
  story_posted:'故事发布',
  no_story_found:'找不到故事',


  ///subscripti
  subscriptions:'订阅内容',
  choose_subscription:'选择订阅',
  yes_subscription_one:"您抓住了一个受欢迎的选择",
  yes_subscription_two:"机会",
  no_subscription_one:"糟糕，您没有订阅。快点，让它立即开始D8ing",
  no_subscription_two:"赶紧做出选择，以获得完全访问权限并立即开始D8ing",
  select_subscription:"在下面进行选择。",

  //update ountry
  choose_country:'选择国家',

  //welcome
lets_get_started:"让我们开始吧",
our_speedd8:"我们的SpeedD8按钮允许冒险的成员现在开会。",
hi:"你好",
welcome_to_d8:"嗨，欢迎来到D8面向所有人的支持性渐进式D8ing应用",



//filterlocationsheet
choose_location_search:"选择要搜索D8的位置",
ok:'好',


//location sheet chat
choose_location_chat:"发送一个您想见面的地方",
choose_range:" 选择D8在您附近的范围",
distance:"距离",
search:'搜索',
cancel:'取消',

//story dialoge
video:'视频',
create_story_to_share:'创建一个故事，与您接受的朋友分享。',
choose_story_image:'选择您的形象故事并与其他D8分享',
choose_story_video:'选择您的视频故事并与其他D8分享',
choose_story_source:'选择故事来源',
image:'图片',

//settings
settings:'设定值',
delete_account:'删除帐户',
delete_account_text:"删除您的帐户是永久性的，将删除所有内容，包括朋友，请求，聊天和个人资料设置。您确定要删除您的帐户吗？"









  
};

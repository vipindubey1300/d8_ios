//ITALIAN LANGUAGE

export default {
  //add story
  add_story: 'Aggiungi storia',

//block user
  block_user: 'Blocca utente',
  block: 'Bloccare ',

//change language
  change_language:'Cambia lingua',
  select_a_language:"Seleziona una lingua e impostala come lingua predefinita per l'intera app",
  save_language:'Salva lingua',

  //Chat Lists
  chat_lists: 'Elenchi di chat',
  typing:'digitando',

  //create profile
    save:'Salva',
    choose_image_source:'Scegli Fonte immagine',
    choose_video_source:'Scegli la sorgente video',
    camera:'telecamera',
    gallery:'Galleria',
    choose_display_picture_making:"Scegli la tua immagine del display facendo conoscere altri D8 per saperne di più",
    display_name:'Nome da visualizzare',
    profession:'Professione',
    age:'Età',
    height:'Altezza',
    weight:'Peso',
    about:'Di',
    ethnicities:'Etnie',
    body_types:'Tipi di corpo',
    create_profile:'Crea un profilo',

     //edit profile
     save:'Salva',
     name:'Nome',
     edit_profile:'Modifica Profilo',
     email:'E-mail',
     ethnicity:'Razza',
     gender:'Genere',
     interested_gender:"Genere interessato",
     country:'Nazione',
     state:'Stato',
     city:'Città',
     add_document:'Carica il documento di verifica per D8 Gold Tick',
     document_added:'Documento aggiunto',
     add_video:'Aggiungi video',
     video_added:'Video aggiunto',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Invia OTP',
  enter_otp:'Inserisci OTP',
  enter_otp_email:"Inserisci l'OTP inviato sulla tua email",
  verify_otp:'Verifica OTP',




  //enter password
  forgot_password:'Ha dimenticato la password',
  please_update_your_password:'Si prega di aggiornare la password',
  new_password:'nuova password',
  confirm_password:'conferma password',
  update:'Aggiornare',


  //Forgot password
  enter_email_address_used_to_register:"Inserisci l'indirizzo email che hai usato per registrarti",


    //filters
    choose_desired_location_for_d8:"Scegli le città desiderate per ottenere l'elenco dei D8 disponibili",
    cities:'Città',
    filters:'filtri',
    change:'Modificare',
    states:'stati',
    countries:'paesi',
    age:'Età',
    close:'Vicina',
    male:'Maschio',
    female:'femmina',
    submit:'Invia',
    height_cms:'Altezza (in cm)',
    weight_kgs:'Peso (in kg)',
    about_bio:'Informazioni sulla biografia (opzionale)',
    ethnicity:'Razza',
    body_type:'Tipo di corpo',

    
    //friends
    friends:'Amiche',
    see_pending_requests:'Vedi richieste Speed D8 in sospeso',

    //friend story
    story:'Story',

    //interested gender
    please_select_interested_gender:'Seleziona il tuo genere interessato',
    //gender
    please_select_your_gender:'Per favore seleziona il tuo genere',

    //login
    user_name:'Nome utente',
    password:"Parola d'ordine",
    login_with_social_account:'Accedi con un account social',
    login:'Accesso',
    dont_have_account:"Non ho un account",
    sign_up_now:"Iscriviti ora",

    //register
    register:'Registrati',

    //models swiper
    reset:'Ripristina',
    request_text:"Richiedendo una Speed D8 stai dicendo a questa persona che sei pronto e disposto a incontrarti. Se non sei pronto per incontrarti, usa invece il pulsante Chat per conoscerli prima",
    accept_and_dont_show:"Accetta e non mostrarlo più",
    please_wait_loading:"Attendi mentre stiamo caricando D8 per te ...",
    you_have_winked:"Hai fatto l'occhiolino.",

    //my profile
    my_profile:'Il mio profilo',
    verified_user:'Utente verificato',
    yes:'sì',
    no:'No',
    years:'Anni',
    cms:'cms',
    lbs:'lbs',
    location:'Posizione',

    //notifiactoin
    notifications:'notifiche',

    //payment-mode
    payment:'Pagamento',
    payment_mode:'Metodo di pagamento',
    payment_type_choose:"Quasi lì, seleziona il tipo di pagamento per soddisfare il tuo D8",

    //pending friend
    accept_text:" Accettando una richiesta 'Speed D8', stai dicendo a questa persona che sei pronto a incontrarti. Se non sei pronto a incontrarti, puoi premere il pulsante 'Chat First' qui sotto per controllarli o premere 'no' pulsante per dire addio.",
    accept:'Accettare',
    chat_first:'Chatta prima',
    nope:'no',
    speed_d8_requests:'Richieste Speed D8',


    //profile menu
    stories:'Storie',
    profile:'Profilo',
    chats:'Chiacchierare',
    buy_subscriptions:'Acquista abbonamenti',
    change_language:'Cambia lingua',
    reset_password:'Resetta la password',
    logout:'Disconnettersi',
    indiscreet_mode:'Indiscreet Mode',
    discreet_mode:'Modalità discreta',
    logout:'Disconnettersi',
    are_sure_to_logout:'Sei sicuro di uscire?',


    //register 
    app_user_name:"Nome utente dell'app",
    accept_terms_and_conditions:'Accetta termini e condizioni',
    already_have_an_account :'Hai già un account ?',
    sign_in_now:'Registrati ora',
    register_with_social_account:'Registrati con un account social',


   //reset password
  reset_password:'Resetta la password',
  please_update_your_password:'Si prega di aggiornare la password',
  old_password:'vecchia password',
  new_password:'nuova password',
  confirm_password:'conferma password',
  reset:'Ripristina',


  //stories
  my_story:'La mia storia',
  friends_story:'Storia degli amici',
  stories:'Storie',
  story_posted:'Storia pubblicata',
  no_story_found:'Nessuna storia trovata',


  ///subscripti
  subscriptions:'Sottoscrizioni',
  choose_subscription:'Scegli abbonamenti',
  yes_subscription_one:"Hai afferrato una scelta popolare fornendo",
  yes_subscription_two:"di opportunità",
  no_subscription_one:"Spiacenti, non hai un abbonamento. Sbrigati e prendine uno per iniziare subito D8ing",
  no_subscription_two:"Sbrigati, fai la tua scelta per ottenere l'accesso completo e iniziare immediatamente D8ing",
  select_subscription:"Fai la tua selezione qui sotto.",

  //update ountry
  choose_country:'Scegli la nazione',

  //welcome
lets_get_started:"Iniziamo",
our_speedd8:"Il nostro pulsante SpeedD8 consente ai membri  \n avventurosi di incontrarsi ora.",
hi:"Ciao",
welcome_to_d8:"Ciao, benvenuto in D8. L'app D8ing di supporto e progressiva per tutti",



//filterlocationsheet
choose_location_search:"Scegli le posizioni in cui cercare D8",
ok:'Ok',


//location sheet chat
choose_location_chat:"Invia uno a cui vorresti incontrare",
choose_range:"Seleziona l'intervallo per D8 più vicino a te",
distance:"Distanza",
search:'Ricerca',
cancel:'Annulla',

//story dialoge
video:'video',
create_story_to_share:'Crea una storia da condividere con i tuoi amici accettati.',
choose_story_image:'Scegli la tua storia di immagini e condividi con altri D8',
choose_story_video:'Scegli la tua storia video e condividila con altri D8',
choose_story_source:'Scegli Story Story',
image:'Immagine',

//settings
settings:'impostazioni',
delete_account:"Eliminare l'account",
delete_account_text:"L'eliminazione del tuo account è permanente e rimuoverà tutti i contenuti inclusi amici, richieste, chat e impostazioni del profilo. Sei sicuro di voler cancellare il tuo account?"









  
};

//GERMAN LANGUAGE

export default {
  //add story
  add_story: 'Story hinzufügen',

//block user
  block_user: 'Benutzer blockieren',
  block: 'Block ',

//change language
  change_language:'Sprache ändern',
  select_a_language:'Wählen Sie eine Sprache aus und legen Sie sie als Standardsprache für die gesamte App fest',
  save_language:'Sprache speichern',

  //Chat Lists
  chat_lists: 'Chat-Listen',
  typing:'Tippen',

  //create profile
    save:'speichern',
    choose_image_source:'Wählen Sie Bildquelle',
    choose_video_source:'Wählen Sie Videoquelle',
    camera:'Kamera',
    gallery:'Galerie',
    choose_display_picture_making:"Wählen Sie Ihr Anzeigebild, damit andere D8 Sie über weitere Informationen informieren können",
    display_name:'Anzeigename',
    profession:'Beruf',
    age:'Alter',
    height:'Höhe',
    weight:'Gewicht',
    about:'Über',
    ethnicities:'Ethnien',
    body_types:'Körpertypen',
    create_profile:'Profil erstellen',

     //edit profile
     save:'speichern',
     name:'Name',
     edit_profile:'Profil bearbeiten',
     email:'Email',
     ethnicity:'Ethnizität',
     gender:'Geschlecht',
     interested_gender:"Interessiertes Geschlecht",
     country:'Land',
     state:'Zustand',
     city:'Stadt',
     add_document:'Bestätigungsdokument für D8 Gold Tick hochladen',
     document_added:'Dokument hinzugefügt',
     add_video:'Video hinzufügen',
     video_added:'Video hinzugefügt',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Senden OTP',
  enter_otp:'Eingeben OTP',
  enter_otp_email:'Geben Sie das OTP ein, das in Ihrer E-Mail gesendet wurde',
  verify_otp:'Überprüfen Sie OTP',




  //enter password
  forgot_password:'Passwort vergessen',
  please_update_your_password:'Bitte aktualisieren Sie Ihr Passwort',
  new_password:'Neues Kennwort',
  confirm_password:'Kennwort bestätigen',
  update:'Aktualisieren',


  //Forgot password
  enter_email_address_used_to_register:'Geben Sie die E-Mail-Adresse ein, mit der Sie sich registriert haben',


    //filters
    choose_desired_location_for_d8:"Wählen Sie die gewünschten Städte aus, um eine Liste der verfügbaren D8 zu erhalten",
    cities:'Städte',
    filters:'Filter',
    change:'Veränderung',
    states:'Zustände',
    countries:'Länder',
    age:'Alter',
    close:'Schließen',
    male:'Männlich',
    female:'Weiblich',
    submit:'einreichen',
    height_cms:'Höhe (in cm)',
    weight_kgs:'Gewicht (in kg)',
    about_bio:'Über Bio (optional)',
    ethnicity:'Ethnizität',
    body_type:'Körpertyp',

    
    //friends
    friends:'Freunde',
    see_pending_requests:'Siehe ausstehende Speed ​​D8-Anforderungen',

    //friend story
    story:'Geschichte',

    //interested gender
    please_select_interested_gender:'Bitte wählen Sie Ihr interessiertes Geschlecht',
    //gender
    please_select_your_gender:'Bitte wählen Sie Ihr Geschlecht',

    //login
    user_name:'Nutzername',
    password:'Passwort',
    login_with_social_account:'Mit sozialem Konto anmelden',
    login:'Einloggen',
    dont_have_account:"Ich habe kein Konto",
    sign_up_now:"Jetzt registrieren",

    //register
    register:'Registrieren',

    //models swiper
    reset:'Zurücksetzen',
    request_text:"Wenn Sie einen Speed ​​D8 anfordern, teilen Sie dieser Person mit, dass Sie bereit und bereit sind, sich zu treffen. Wenn Sie nicht bereit sind, sich zu treffen, verwenden Sie stattdessen die Schaltfläche Chat, um sie zuerst kennenzulernen",
    accept_and_dont_show:"Akzeptiere und zeige das nicht noch einmal",
    please_wait_loading:"Bitte warten Sie, während wir D8 für Sie laden ...",
    you_have_winked:"Du hast geblinzelt.",

    //my profile
    my_profile:'Mein Profil',
    verified_user:'Verifizierter Benutzer',
    yes:'Ja',
    no:'Nein',
    years:'Jahre',
    cms:'cms',
    lbs:'lbs',
    location:'Ort',

    //notifiactoin
    notifications:'Benachrichtigungen',

    //payment-mode
    payment:'Zahlung',
    payment_mode:'Zahlungsart',
    payment_type_choose:"Wählen Sie fast dort die Zahlungsart aus, um Ihren D8 zu erfüllen",

    //pending friend
    accept_text:" Wenn Sie eine 'Speed ​​D8'-Anfrage annehmen, teilen Sie dieser Person mit, dass Sie bereit sind, sich zu treffen. Wenn Sie nicht bereit sind, sich zu treffen, können Sie unten auf die Schaltfläche' Chat First 'klicken, um sie zu überprüfen, oder auf' Nope 'klicken. Knopf, um sich zu verabschieden.",
    accept:'Akzeptieren',
    chat_first:'Zuerst chatten',
    nope:'Nee',
    speed_d8_requests:'Speed ​​D8-Anfragen',


    //profile menu
    stories:'Geschichten',
    profile:'Profil',
    chats:'Chats',
    buy_subscriptions:'Abonnements kaufen',
    change_language:'Sprache ändern',
    reset_password:'Passwort zurücksetzen',
    logout:'Ausloggen',
    indiscreet_mode:'Indiskreter Modus',
    discreet_mode:'Diskreter Modus',
    logout:'Ausloggen',
    are_sure_to_logout:'Sind Sie sicher, sich abzumelden?',


    //register 
    app_user_name:'App-Benutzername',
    accept_terms_and_conditions:'Allgemeine Geschäftsbedingungen akzeptieren',
    already_have_an_account :'Sie haben bereits ein Konto ?',
    sign_in_now:'Schreib dich jetzt ein',
    register_with_social_account:'Registrieren Sie sich mit dem sozialen Konto',


   //reset password
  reset_password:'Passwort zurücksetzen',
  please_update_your_password:'Bitte aktualisieren Sie Ihr Passwort',
  old_password:'Altes Passwort',
  new_password:'Neues Kennwort',
  confirm_password:'Kennwort bestätigen',
  reset:'Zurücksetzen',


  //stories
  my_story:'Meine Geschichte',
  friends_story:'Freunde Geschichte',
  stories:'Geschichten',
  story_posted:'Geschichte gepostet',
  no_story_found:'Keine Geschichte gefunden',


  ///subscripti
  subscriptions:'Abonnements',
  choose_subscription:'Wählen Sie Abonnements',
  yes_subscription_one:"Sie haben eine beliebte Auswahl getroffen",
  yes_subscription_two:"von Möglichkeiten",
  no_subscription_one:"Hoppla, Sie haben kein Abonnement. Beeilen Sie sich und lassen Sie einen sofort mit D8ing beginnen",
  no_subscription_two:"Beeilen Sie sich, treffen Sie Ihre Wahl, um vollen Zugriff zu erhalten, und starten Sie D8ing sofort",
  select_subscription:"Treffen Sie unten Ihre Auswahl",

  //update ountry
  choose_country:'Land auswählen',

  //welcome
lets_get_started:"Lass uns anfangen",
our_speedd8:"Mit unserer SpeedD8-Taste können sich abenteuerlustige \n Mitglieder jetzt treffen.",
hi:"Hallo",
welcome_to_d8:"Hallo, willkommen bei D8. Die unterstützende und fortschrittliche D8ing-App für alle",



//filterlocationsheet
choose_location_search:"Wählen Sie Standorte für die Suche nach D8",
ok:'In Ordnung',


//location sheet chat
choose_location_chat:"Senden Sie eine, wo Sie sich treffen möchten.",
choose_range:"Wählen Sie den Bereich für D8 in Ihrer Nähe ",
distance:"Entfernung",
search:'Suche',
cancel:'Stornieren',

//story dialoge
video:'Video',
create_story_to_share:'Erstellen Sie eine Geschichte, die Sie mit Ihren akzeptierten Freunden teilen möchten.',
choose_story_image:'Wählen Sie Ihre Image-Story und teilen Sie sie mit anderen D8s',
choose_story_video:'Wählen Sie Ihre Video-Story und teilen Sie sie mit anderen D8s',
choose_story_source:'Wählen Sie Story Source',
image:'Bild',

//settings
settings:'die Einstellungen',
delete_account:'Konto löschen',
delete_account_text:"Das Löschen Ihres Kontos ist dauerhaft und entfernt alle Inhalte, einschließlich Freunde, Anfragen, Chats und Profileinstellungen. Möchten Sie Ihr Konto wirklich löschen?"









  
};
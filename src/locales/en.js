//ENGLISH LANGUAGE

export default {
  //add story
  add_story: 'Add Story',

//block user
  block_user: 'Block User',
  block: 'Block ',

//change language
  change_language:'Change Language',
  select_a_language:'Select a language and make it as default language for whole app',
  save_language:'Save language',

  //Chat Lists
  chat_lists: 'Chat Lists',
  typing:'typing',

  //create profile
    save:'Save',
    choose_image_source:'Choose Image Source',
    choose_video_source:'Choose Video Source',
    camera:'Camera',
    gallery:'Gallery',
    choose_display_picture_making:"Choose Your Display Picture making other D8's to know you about more",
    display_name:'Display Name',
    profession:'Profession',
    age:'Age',
    height:'Height',
    weight:'Weight',
    about:'About',
    ethnicities:'Ethnicities',
    body_types:'Body Types',
    create_profile:'Create Profile',

     //edit profile
     save:'Save',
     name:'Name',
     edit_profile:'Edit Profile',
     email:'Email',
     ethnicity:'Ethnicity',
     gender:'Gender',
     interested_gender:"Interested Gender",
     country:'Country',
     state:'State',
     city:'City',
     add_document:'Upload Verification Document for D8 Gold Tick',
     document_added:'Document Added',
     add_video:'Add Video',
     video_added:'Video Added',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Send OTP',
  enter_otp:'Enter OTP',
  enter_otp_email:'Enter the OTP sent on your email',
  verify_otp:'Verify OTP',




  //enter password
  forgot_password:'Forgot Password',
  please_update_your_password:'Please update your password',
  new_password:'New Password',
  confirm_password:'Confirm Password',
  update:'Update',


  //Forgot password
  enter_email_address_used_to_register:'Enter the Email Address You Used to Register',


    //filters
    choose_desired_location_for_d8:"Choose desired cities to get list of available D8's",
    cities:'Cities',
    filters:'Filters',
    change:'Change',
    states:'States',
    countries:'Countries',
    age:'Age',
    close:'Close',
    male:'Male',
    female:'Female',
    submit:'Submit',
    height_cms:'Height (in cms)',
    weight_kgs:'Weight (in kgs)',
    about_bio:'About Bio (Optional)',
    ethnicity:'Ethnicity',
    body_type:'Body type',

    
    //friends
    friends:'Friends',
    see_pending_requests:'See pending Speed D8 requests',

    //friend story
    story:'Story',

    //interested gender
    please_select_interested_gender:'Please select your interested gender',
    //gender
    please_select_your_gender:'Please select your gender',

    //login
    user_name:'Username',
    password:'Password',
    login_with_social_account:'Login With Social Account',
    login:'Login',
    dont_have_account:"Don't have an account",
    sign_up_now:"Sign Up Now",

    //register
    register:'Register',

    //models swiper
    reset:'Reset',
    request_text:"By Requesting a Speed D8 you’re telling this person that your ready and willing to meet. If you’re not ready to meet,use the Chat button instead to get to know them first",
    accept_and_dont_show:"Accept and don't show this again",
    please_wait_loading:"Please wait while we are loading D8's for you...",
    you_have_winked:"You have Winked.",

    //my profile
    my_profile:'My Profile',
    verified_user:'Verified User',
    yes:'Yes',
    no:'No',
    years:'Years',
    cms:'cms',
    lbs:'lbs',
    location:'Location',

    //notifiactoin
    notifications:'Notifications',

    //payment-mode
    payment:'Payment',
    payment_mode:'Payment Mode',
    payment_type_choose:"Almost there, select payment type to meet your D8",

    //pending friend
    accept_text:" By accepting a 'Speed D8' request, you’re telling this person that your ready to meet.If you are not ready to meet,you can hit the 'Chat First' button below to check them out, or hit the 'nope' button to say goodbye.",
    accept:'Accept',
    chat_first:'Chat First',
    nope:'Nope',
    speed_d8_requests:'Speed D8 requests',


    //profile menu
    stories:'Stories',
    profile:'Profile',
    chats:'Chats',
    buy_subscriptions:'Buy Subscriptions',
    change_language:'Change Language',
    reset_password:'Reset Password',
    logout:'Logout',
    indiscreet_mode:'Indiscreet Mode',
    discreet_mode:'Discreet Mode',
    logout:'Logout',
    are_sure_to_logout:'Are you sure to logout?',


    //register 
    app_user_name:'App Username',
    accept_terms_and_conditions:'Accept terms & Condition',
    already_have_an_account :'Already have an account ?',
    sign_in_now:'Sign In now',
    register_with_social_account:'Register with social Account',


   //reset password
  reset_password:'Reset Password',
  please_update_your_password:'Please update your password',
  old_password:'Old Password',
  new_password:'New Password',
  confirm_password:'Confirm Password',
  reset:'Reset',


  //stories
  my_story:'My Story',
  friends_story:'Friends Story',
  stories:'Stories',
  story_posted:'Story Posted',
  no_story_found:'No Story Found',


  ///subscripti
  subscriptions:'Subscriptions',
  choose_subscription:'Choose Subscriptions',
  yes_subscription_one:"You’ve grabbed a popular choice providing",
  yes_subscription_two:"of opportunities",
  no_subscription_one:"Oops, you don’t have subscription. Hurry up and get one to start D8ing immediately",
  no_subscription_two:"Hurry make your choice to get full access and start D8ing immediately",
  select_subscription:"Make your selection below",

  //update ountry
  choose_country:'Choose Country',

  //welcome
lets_get_started:"Let's get Started",
our_speedd8:"Our SpeedD8 button allows adventurous \n members to meet now.",
hi:"Hi",
welcome_to_d8:"Hi, welcome to D8. The supportive and progressive D8ing app for everyone",






//filterlocationsheet
choose_location_search:"Choose locations to search D8's",
ok:'Ok',


//location sheet chat
choose_location_chat:"Send a one where you’d like to meet.",
choose_range:" Select range for D8’s near you",
distance:"Distance",
search:'Search',
cancel:'Cancel',

//story dialoge
video:'Video',
create_story_to_share:'Create a story to share with your accepted friends.',
choose_story_image:'Choose your image story and share with other D8s',
choose_story_video:'Choose your video story and share with other D8s',
choose_story_source:'Choose Story Source',
image:'Image',

//settings
settings:'Settings',
delete_account:'Delete Account',
delete_account_text:"Deleting your account is permanent and will remove all content including friends, requests, chats and profile settings. Are you sure you want to delete your account?",


 //contact us
 contact_us:'Contact Us',
 message_title:'Message Title',
 description:'Description',
 d8_complain:'D8 Complaint',
 username:'Username',
 suggestions:'Suggestions',
 reason_one:'My D8 sent Objectional Content ',
 reason_two:'D8 Abused',
 reason_three:'Bad Behaviour or Violence',
 please_detail_complain:'Please detail your complaint',
 objectional_content:'Objectional Content ,Abuse or Violence',


  
};

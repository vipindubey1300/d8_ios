//RUSSIAN LANGUAGE

export default {
  //add story
  add_story: 'Добавить историю',

//block user
  block_user: 'Заблокировать пользователя',
  block: 'блок ',

//change language
  change_language:'Изменить язык',
  select_a_language:'Выберите язык и сделайте его языком по умолчанию для всего приложения',
  save_language:'Сохранить язык',

  //Chat Lists
  chat_lists: 'Списки чатов',
  typing:'типирование',

  //create profile
    save:'Сохранить',
    choose_image_source:'Выберите источник изображения',
    choose_video_source:'Выберите источник видео',
    camera:'камера',
    gallery:'Галерея',
    choose_display_picture_making:"Выберите свой дисплей, чтобы другие D8 знали о вас больше",
    display_name:'Показать имя',
    profession:'профессия',
    age:'Возраст',
    height:'Высота',
    weight:'Вес',
    about:'Около',
    ethnicities:'Этносы',
    body_types:'Типы телосложения',
    create_profile:'Создать профиль',

     //edit profile
     save:'Сохранить',
     name:'имя',
     edit_profile:'Редактировать профиль',
     email:'Эл. адрес',
     ethnicity:'Этнос',
     gender:'Пол',
     interested_gender:"Интересует пол",
     country:'Страна',
     state:'состояние',
     city:'город',
     add_document:'Загрузить подтверждающий документ для D8 Gold Tick',
     document_added:'Документ добавлен',
     add_video:'Добавить видео',
     video_added:'Добавлено видео',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Отправить OTP',
  enter_otp:'Введите OTP',
  enter_otp_email:'Введите OTP, отправленный на вашу электронную почту',
  verify_otp:'Проверьте OTP',




  //enter password
  forgot_password:'Забыл пароль',
  please_update_your_password:'Пожалуйста, обновите ваш пароль',
  new_password:'новый пароль',
  confirm_password:'Подтвердите Пароль',
  update:'Обновить',


  //Forgot password
  enter_email_address_used_to_register:'Введите адрес электронной почты, который вы использовали для регистрации',


    //filters
    choose_desired_location_for_d8:"Выберите нужные города, чтобы получить список доступных D8",
    cities:'Города',
    filters:'фильтры',
    change:'+ Изменить',
    states:'состояния',
    countries:'страны',
    age:'Возраст',
    close:'близко',
    male:'мужчина',
    female:'женский',
    submit:'Разместить',
    height_cms:'Высота (в смс)',
    weight_kgs:'Вес (в кг)',
    about_bio:'О био (опционально)',
    ethnicity:'Этнос',
    body_type:'Телосложение',

    
    //friends
    friends:'друзья',
    see_pending_requests:'Смотрите ожидающие запросы Speed D8',

    //friend story
    story:'История',

    //interested gender
    please_select_interested_gender:'Пожалуйста, выберите интересующий вас пол',
    //gender
    please_select_your_gender:'Пожалуйста, выберите Ваш пол',

    //login
    user_name:'имя пользователя',
    password:'пароль',
    login_with_social_account:'Войти через социальную учетную запись',
    login:'Авторизоваться',
    dont_have_account:"Нет аккаунта",
    sign_up_now:"Зарегистрироваться сейчас",

    //register
    register:'регистр',

    //models swiper
    reset:'Сброс настроек',
    request_text:"Запрашивая скорость D8, вы говорите этому человеку, что вы готовы и хотите встретиться. Если вы не готовы встретиться, используйте вместо этого кнопку Чат, чтобы узнать их в первую очередь",
    accept_and_dont_show:"Принять и не показывать это снова",
    please_wait_loading:"Пожалуйста, подождите, пока мы загружаем D8 для вас ...",
    you_have_winked:"Вы подмигнули.",

    //my profile
    my_profile:'Мой профайл',
    verified_user:'Проверенный пользователь',
    yes:'да',
    no:'нет',
    years:'лет',
    cms:'КМВ',
    lbs:'фунтов',
    location:'Место расположения',

    //notifiactoin
    notifications:'Уведомления',

    //payment-mode
    payment:'Оплата',
    payment_mode:'Режим оплаты',
    payment_type_choose:"Почти там, выберите тип оплаты, чтобы удовлетворить ваши D8",

    //pending friend
    accept_text:" Принимая запрос «Speed D8», вы сообщаете этому человеку, что вы готовы встретиться. Если вы не готовы встретиться, вы можете нажать кнопку «Сначала поговорить», чтобы проверить их, или нажать «Нет». Кнопка, чтобы сказать до свидания.",
    accept:'принимать',
    chat_first:'Первый чат',
    nope:'нет',
    speed_d8_requests:'Скорость D8 запросов',


    //profile menu
    stories:'Истории',
    profile:'Профиль',
    chats:'Чаты',
    buy_subscriptions:'Купить подписки',
    change_language:'Изменить язык',
    reset_password:'Сброс пароля',
    logout:'Выйти',
    indiscreet_mode:'Нескромный режим',
    discreet_mode:'Discreet Mode',
    logout:'Выйти',
    are_sure_to_logout:'Вы уверены, что хотите выйти?',


    //register 
    app_user_name:'Имя пользователя приложения',
    accept_terms_and_conditions:'Принять условия и условия',
    already_have_an_account :'Уже есть аккаунт?',
    sign_in_now:'Войти сейчас',
    register_with_social_account:'Зарегистрироваться с помощью социальной учетной записи',


   //reset password
  reset_password:'Сброс пароля',
  please_update_your_password:'Пожалуйста, обновите ваш пароль',
  old_password:'Старый пароль',
  new_password:'новый пароль',
  confirm_password:'Подтвердите Пароль',
  reset:'Сброс настроек',


  //stories
  my_story:'Моя история',
  friends_story:'История друзей',
  stories:'Истории',
  story_posted:'История опубликована',
  no_story_found:'История не найдена',


  ///subscripti
  subscriptions:'Подписки',
  choose_subscription:'Выберите Подписки',
  yes_subscription_one:"Вы воспользовались популярным выбором",
  yes_subscription_two:"возможностей",
  no_subscription_one:"К сожалению, у вас нет подписки. Спешите и получите один, чтобы начать D8ing немедленно",
  no_subscription_two:"Спешите сделать свой выбор, чтобы получить полный доступ и сразу же начать D8ing",
  select_subscription:"Сделайте свой выбор ниже.",

  //update ountry
  choose_country:'Выберите страну',

  //welcome
lets_get_started:"Давайте начнем",
our_speedd8:"Наша кнопка SpeedD8 позволяет авантюрным  \n участникам встретиться сейчас.",
hi:"Здравствуй",
welcome_to_d8:"Привет, добро пожаловать в D8. Поддерживающее и прогрессивное приложение D8ing для всех",



//filterlocationsheet
choose_location_search:"Выберите места для поиска D8",
ok:'Ok',


//location sheet chat
choose_location_chat:"Отправить, где вы хотели бы встретиться.",
choose_range:" Выберите диапазон для D8 рядом с вами",
distance:"Расстояние",
search:'Поиск',
cancel:'Отмена',

//story dialoge
video:'видео',
create_story_to_share:'Создайте историю, чтобы поделиться с вашими принятыми друзьями.',
choose_story_image:'Выберите свой имидж и поделитесь с другими D8',
choose_story_video:'Выберите свою видео историю и поделитесь с другими D8',
choose_story_source:'Выберите источник истории',
image:'Образ',

//settings
settings:'настройки',
delete_account:'Удалить аккаунт',
delete_account_text:"Удаление вашей учетной записи является постоянным и удалит весь контент, включая друзей, запросы, чаты и настройки профиля. Вы уверены, что хотите удалить свой аккаунт?"









  
};

//THAI LANGUAGE

export default {
  //add story
  add_story: 'เพิ่มเรื่องราว',

//block user
  block_user: 'กีดกันผู้ใช้',
  block: 'บล็อก ',

//change language
  change_language:'เปลี่ยนภาษา',
  select_a_language:'เลือกภาษาและทำให้เป็นภาษาเริ่มต้นสำหรับทั้งแอป',
  save_language:'บันทึกภาษา',

  //Chat Lists
  chat_lists: 'รายการแชท',
  typing:'การพิมพ์',

  //create profile
    save:'บันทึก',
    choose_image_source:'เลือกแหล่งรูปภาพ',
    choose_video_source:'เลือกแหล่งวิดีโอ',
    camera:'กล้อง',
    gallery:'เฉลียง',
    choose_display_picture_making:"เลือกรูปภาพที่แสดงของคุณทำให้ D8 อื่น ๆ รู้จักคุณมากขึ้น",
    display_name:'ชื่อที่แสดง',
    profession:'อาชีพ',
    age:'อายุ',
    height:'ความสูง',
    weight:'น้ำหนัก',
    about:'เกี่ยวกับ',
    ethnicities:'ชาติพันธุ์',
    body_types:'ประเภทของร่างกาย',
    create_profile:'สร้างโปรไฟล์',

     //edit profile
     save:'บันทึก',
     name:'ชื่อ',
     edit_profile:'แก้ไขโปรไฟล์',
     email:'อีเมล์',
     ethnicity:'เชื้อชาติ',
     gender:'เพศ',
     interested_gender:"เพศที่สนใจ",
     country:'ประเทศ',
     state:'สถานะ',
     city:'เมือง',
     add_document:'อัปโหลดเอกสารการตรวจสอบสำหรับ D8 Gold Tick',
     document_added:'เพิ่มเอกสารแล้ว',
     add_video:'เพิ่มวิดีโอ',
     video_added:'เพิ่มวิดีโอแล้ว',

  

  //enter OTP
  otp:'OTP',
  send_otp:'ส่ง OTP',
  enter_otp:'ป้อน OTP',
  enter_otp_email:'ป้อน OTP ที่ส่งทางอีเมลของคุณ',
  verify_otp:'ตรวจสอบ OTP',




  //enter password
  forgot_password:'ลืมรหัสผ่าน',
  please_update_your_password:'โปรดอัปเดตรหัสผ่านของคุณ',
  new_password:'รหัสผ่านใหม่',
  confirm_password:'ยืนยันรหัสผ่าน',
  update:'ปรับปรุง',


  //Forgot password
  enter_email_address_used_to_register:'ป้อนที่อยู่อีเมลที่คุณใช้ในการลงทะเบียน',


    //filters
    choose_desired_location_for_d8:"เลือกเมืองที่ต้องการเพื่อรับรายการของ D8 ที่มีอยู่",
    cities:'เมือง',
    filters:'ฟิลเตอร์',
    change:'เปลี่ยนแปลง',
    states:'สหรัฐอเมริกา',
    countries:'ประเทศ',
    age:'อายุ',
    close:'ปิด',
    male:'ชาย',
    female:'หญิง',
    submit:'เสนอ',
    height_cms:'ความสูง (ซม.)',
    weight_kgs:'น้ำหนัก (กิโลกรัม)',
    about_bio:'เกี่ยวกับไบโอ (ไม่จำเป็น)',
    ethnicity:'เชื้อชาติ',
    body_type:'ประเภทของร่างกาย',

    
    //friends
    friends:'เพื่อน',
    see_pending_requests:'ดูคำขอ D8 ความเร็วที่รออนุมัติ',

    //friend story
    story:'เรื่องราว',

    //interested gender
    please_select_interested_gender:'โปรดเลือกเพศที่คุณสนใจ',
    //gender
    please_select_your_gender:'โปรดเลือกเพศของคุณ',

    //login
    user_name:'ชื่อผู้ใช้',
    password:'รหัสผ่าน',
    login_with_social_account:'เข้าสู่ระบบด้วยบัญชีโซเชียล',
    login:'เข้าสู่ระบบ',
    dont_have_account:"ยังไม่มีบัญชี",
    sign_up_now:"สมัครตอนนี้เลย",

    //register
    register:'สมัครสมาชิก',

    //models swiper
    reset:'ตั้งค่าใหม่',
    request_text:"โดยการขอ Speed D8 คุณจะบอกคนนี้ว่าคุณพร้อมและเต็มใจที่จะพบ หากคุณยังไม่พร้อมตอบสนองให้ใช้ปุ่มแชทแทนเพื่อทำความรู้จักก่อน",
    accept_and_dont_show:"ยอมรับและไม่แสดงสิ่งนี้อีก",
    please_wait_loading:"โปรดรอในขณะที่เรากำลังโหลด D8 ให้คุณ ...",
    you_have_winked:"คุณมีขยิบตา",

    //my profile
    my_profile:'ประวัติของฉัน',
    verified_user:'ตรวจสอบผู้ใช้',
    yes:'ใช่',
    no:'ไม่',
    years:'ปี',
    cms:'ซ.ม.',
    lbs:'ปอนด์',
    location:'ที่ตั้ง',

    //notifiactoin
    notifications:'การแจ้งเตือน',

    //payment-mode
    payment:'การชำระเงิน',
    payment_mode:'โหมดการชำระเงิน',
    payment_type_choose:"ใกล้ถึงแล้วให้เลือกประเภทการชำระเงินเพื่อให้ตรงกับ D8 ของคุณ",

    //pending friend
    accept_text:" ด้วยการยอมรับคำขอ 'Speed D8' คุณจะบอกคนนี้ว่าคุณพร้อมที่จะพบกันหากคุณไม่พร้อมที่จะพบคุณสามารถกดปุ่ม 'แชทแรก' ด้านล่างเพื่อตรวจสอบพวกเขาหรือกด 'ไม่' ปุ่มบอกลา",
    accept:'ยอมรับ',
    chat_first:'แชทก่อน',
    nope:'Nope',
    speed_d8_requests:'คำขอ Speed D8',


    //profile menu
    stories:'เรื่อง',
    profile:'ข้อมูลส่วนตัว',
    chats:'แชท',
    buy_subscriptions:'ซื้อการสมัครสมาชิก',
    change_language:'เปลี่ยนภาษา',
    reset_password:'รีเซ็ตรหัสผ่าน',
    logout:'ออกจากระบบ',
    indiscreet_mode:'โหมด Indiscreet',
    discreet_mode:'โหมดรอบคอบ',
    logout:'ออกจากระบบ',
    are_sure_to_logout:'คุณแน่ใจว่าจะออกจากระบบ?',


    //register 
    app_user_name:'ชื่อผู้ใช้แอป',
    accept_terms_and_conditions:'ยอมรับข้อกำหนดและเงื่อนไข',
    already_have_an_account :'มีบัญชีอยู่แล้ว?',
    sign_in_now:'ลงชื่อเข้าใช้ตอนนี้',
    register_with_social_account:'ลงทะเบียนด้วยบัญชีโซเชียล',


   //reset password
  reset_password:'รีเซ็ตรหัสผ่าน',
  please_update_your_password:'โปรดอัปเดตรหัสผ่านของคุณ',
  old_password:'รหัสผ่านเก่า',
  new_password:'รหัสผ่านใหม่',
  confirm_password:'ยืนยันรหัสผ่าน',
  reset:'ตั้งค่าใหม่',


  //stories
  my_story:'เรื่องราวของฉัน',
  friends_story:'เรื่องเพื่อน',
  stories:'เรื่อง',
  story_posted:'โพสต์เรื่องราว',
  no_story_found:'ไม่พบเรื่องราว',


  ///subscripti
  subscriptions:'การสมัครรับข้อมูล',
  choose_subscription:'เลือกการสมัครสมาชิก',
  yes_subscription_one:"คุณได้รับทางเลือกที่เป็นที่นิยม",
  yes_subscription_two:"ของโอกาส",
  no_subscription_one:"โอ๊ะคุณไม่มีการสมัครรับข้อมูล รีบเข้ามาเพื่อเริ่ม D8ing ทันที",
  no_subscription_two:"รีบทำการเลือกของคุณเพื่อเข้าถึงแบบเต็มและเริ่ม D8ing ทันที",
  select_subscription:"ทำการเลือกของคุณด้านล่าง",

  //update ountry
  choose_country:'เลือกประเทศ',

  //welcome
lets_get_started:"มาเริ่มกันเลย",
our_speedd8:"ปุ่ม SpeedD8 ของเราช่วยให้สมาชิกที่ชอบผจญภัยได้พบกันในตอนนี้",
hi:"สวัสดี",
welcome_to_d8:"สวัสดียินดีต้อนรับสู่ D8 แอพ D8ing ที่สนับสนุนและก้าวหน้าสำหรับทุกคน",



//filterlocationsheet
choose_location_search:"เลือกที่ตั้งเพื่อค้นหา D8",
ok:'Ok',


//location sheet chat
choose_location_chat:"Отправить, где вы хотели бы встретиться",
choose_range:"เลือกช่วงสำหรับ D8 ที่อยู่ใกล้คุณ",
distance:"ระยะทาง",
search:'ค้นหา',
cancel:'ยกเลิก',

//story dialoge
video:'วีดีโอ',
create_story_to_share:'สร้างเรื่องราวเพื่อแบ่งปันกับเพื่อน ๆ ที่คุณยอมรับ',
choose_story_image:'เลือกเรื่องราวภาพของคุณและแบ่งปันกับ D8 อื่น ๆ',
choose_story_video:'เลือกเรื่องราววิดีโอของคุณและแชร์กับ D8 อื่น ๆ',
choose_story_source:'เลือกที่มาของเรื่อง',
image:'ภาพ',

//settings
settings:'การตั้งค่า',
delete_account:'ลบบัญชี',
delete_account_text:"การลบบัญชีของคุณเป็นการถาวรและจะลบเนื้อหาทั้งหมดรวมถึงเพื่อนคำขอแชทและการตั้งค่าโปรไฟล์ คุณแน่ใจหรือว่าต้องการลบบัญชีของคุณ?"









  
};
//SPANISH LANGUAGE

export default {
  //add story
  add_story: 'Añadir historia',

//block user
  block_user: 'Bloquear usuario',
  block: 'Bloquear ',

//change language
  change_language:'Cambiar idioma',
  select_a_language:'Seleccione un idioma y configúrelo como idioma predeterminado para toda la aplicación',
  save_language:'Guardar idioma',

  //Chat Lists
  chat_lists: 'Listas de chat',
  typing:'mecanografía',

  //create profile
    save:'Salvar',
    choose_image_source:'Elegir fuente de imagen',
    choose_video_source:'Elegir fuente de video',
    camera:'Cámara',
    gallery:'Galería',
    choose_display_picture_making:"Elija su imagen de visualización haciendo que otros D8 lo conozcan más",
    display_name:'Nombre para mostrar',
    profession:'Profesión',
    age:'Años',
    height:'Altura',
    weight:'Peso',
    about:'Acerca de',
    ethnicities:'Etnias',
    body_types:'Tipos de cuerpo',
    create_profile:'Crear perfil',

     //edit profile
     save:'Salvar',
     name:'Nombre',
     edit_profile:'Editar perfil',
     email:'Email',
     ethnicity:'Etnicidad',
     gender:'Género',
     interested_gender:"Género interesado",
     country:'País',
     state:'Estado',
     city:'Ciudad',
     add_document:'Cargar documento de verificación para D8 Gold Tick',
     document_added:'Documento agregado',
     add_video:'Añadir video',
     video_added:'Video agregado',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Enviar OTP',
  enter_otp:'Entrar OTP',
  enter_otp_email:'Ingrese la OTP enviada en su correo electrónico',
  verify_otp:'Verificar OTP',




  //enter password
  forgot_password:'Se te olvidó tu contraseña',
  please_update_your_password:'Por favor actualice su contraseña',
  new_password:'Nueva contraseña',
  confirm_password:'Confirmar contraseña',
  update:'Actualizar',


  //Forgot password
  enter_email_address_used_to_register:'Ingrese la dirección de correo electrónico que utilizó para registrarse',


    //filters
    choose_desired_location_for_d8:"Elija las ciudades deseadas para obtener la lista de D8 disponibles",
    cities:'Ciudades',
    filters:'Filtros',
    change:'Cambio',
    states:'Estados',
    countries:'Países',
    age:'Años',
    close:'Cerca',
    male:'Masculina',
    female:'Hembra',
    submit:'Enviar',
    height_cms:'Altura (en centímetros)',
    weight_kgs:'Peso (en kgs)',
    about_bio:'Sobre Bio (Opcional)',
    ethnicity:'Etnicidad',
    body_type:'Tipo de cuerpo',

    
    //friends
    friends:'Amigas',
    see_pending_requests:'Ver solicitudes pendientes de Speed ​​D8',

    //friend story
    story:'Historia',

    //interested gender
    please_select_interested_gender:'Por favor seleccione su género interesado',
    //gender
    please_select_your_gender:'por favor seleccione su género',

    //login
    user_name:'Nombre de usuario',
    password:'Contraseña',
    login_with_social_account:'Iniciar sesión con cuenta social',
    login:'Iniciar sesión',
    dont_have_account:"No tengo una cuenta",
    sign_up_now:"Regístrate ahora",

    //register
    register:'Registrarse',

    //models swiper
    reset:'Reiniciar',
    request_text:"Al solicitar un Speed ​​D8, le está diciendo a esta persona que está listo y dispuesto a reunirse. Si no está listo para reunirse, use el botón Chat para conocerlos primero",
    accept_and_dont_show:"Acepta y no muestres esto de nuevo",
    please_wait_loading:"Espere mientras cargamos D8 para usted ...",
    you_have_winked:"Has guiñado",

    //my profile
    my_profile:'Mi perfil',
    verified_user:'Usuario verificada',
    yes:'si',
    no:'No',
    years:'Años',
    cms:'cms',
    lbs:'lbs',
    location:'Ubicación',

    //notifiactoin
    notifications:'Notificaciones',

    //payment-mode
    payment:'Pago',
    payment_mode:'Modo de pago',
    payment_type_choose:"Casi allí, seleccione el tipo de pago para cumplir con su D8",

    //pending friend
    accept_text:" Al aceptar una solicitud de 'Speed ​​D8', le está diciendo a esta persona que está listo para reunirse.Si no está listo para reunirse, puede presionar el botón 'Chatear primero' a continuación para verificarlo o presionar 'no'. botón para decir adiós.",
    accept:'Aceptar',
    chat_first:'Chatea primero',
    nope:'No',
    speed_d8_requests:'Solicitudes Speed ​​D8',


    //profile menu
    stories:'Cuentos',
    profile:'Perfil',
    chats:'Chats',
    buy_subscriptions:'Comprar suscripciones',
    change_language:'Cambiar idioma',
    reset_password:'Restablecer la contraseña',
    logout:'Cerrar sesión',
    indiscreet_mode:'Modo indiscreto',
    discreet_mode:'Modo discreto',
    are_sure_to_logout:'Estás seguro de cerrar sesión?',


    //register 
    app_user_name:'Nombre de usuario de la aplicación',
    accept_terms_and_conditions:'Aceptar términos y condiciones',
    already_have_an_account :'Ya tienes una cuenta ?',
    sign_in_now:'Regístrate',
    register_with_social_account:'Registrarse con cuenta social',


   //reset password
  reset_password:'Restablecer la contraseña',
  please_update_your_password:'Por favor actualice su contraseña',
  old_password:'Contraseña anterior',
  new_password:'Nueva contraseña',
  confirm_password:'Confirmar contraseña',
  reset:'Reiniciar',


  //stories
  my_story:'Mi historia',
  friends_story:'Historia de amigas',
  stories:'Cuentos',
  story_posted:'Historia publicada',
  no_story_found:'No se encontró historia',


  ///subscripti
  subscriptions:'Suscripciones',
  choose_subscription:'Elegir suscripciones',
  yes_subscription_one:"Has elegido una opción popular que proporciona",
  yes_subscription_two:"de oportunidades",
  no_subscription_one:"¡Vaya! No tienes suscripción. Date prisa y consigue uno para comenzar a D8ing inmediatamente",
  no_subscription_two:"Date prisa, haz tu elección para obtener acceso completo e iniciar D8ing de inmediato",
  select_subscription:"Haga su selección a continuación",

  //update ountry
  choose_country:'Elige país',

  //welcome
lets_get_started:"Empecemos",
our_speedd8:"Nuestro botón SpeedD8 permite que los miembros  \n aventureros se reúnan ahora.",
hi:"Hi",
welcome_to_d8:"Hola, bienvenido a D8. La aplicación D8ing de apoyo y progresiva para todos",



//filterlocationsheet
choose_location_search:"Elija ubicaciones para buscar D8",
ok:'Okay',


//location sheet chat
choose_location_chat:"Envíe uno donde le gustaría reunirse.",
choose_range:"Seleccione el rango para D8 cerca de usted",
distance:"Distancia",
search:'Buscar',
cancel:'Cancelar',

//story dialoge
video:'Video',
create_story_to_share:'Crea una historia para compartir con tus amigas aceptadas.',
choose_story_image:'Elige tu historia de imagen y comparte con otros D8',
choose_story_video:'Elige tu historia de video y compártela con otros D8',
choose_story_source:'Elija la fuente de la historia',
image:'Imagen',

//settings
settings:'Configuraciones',
delete_account:'Borrar cuenta',
delete_account_text:"Eliminar su cuenta es permanente y eliminará todo el contenido, incluidos amigos, solicitudes, chats y configuraciones de perfil. ¿Estás seguro de que deseas eliminar tu cuenta?"


  
};
//FRENCH LANGUAGE

export default {
  //add story
  add_story: 'Ajouter une histoire',

//block user
  block_user: 'Bloquer un utilisateur',
  block: 'Bloquer ',

//change language
  change_language:'Changer de langue',
  select_a_language:"Sélectionnez une langue et définissez-la comme langue par défaut pour toute l'application",
  save_language:'Enregistrer la langue',

  //Chat Lists
  chat_lists: 'Listes de chat',
  typing:'dactylographie',

  //create profile
    save:'sauvegarder',
    choose_image_source:"Choisissez la source de l'image",
    choose_video_source:'Choisissez la source vidéo',
    camera:'Caméra',
    gallery:'Galerie',
    choose_display_picture_making:"Choisissez votre image d'affichage en faisant d'autres D8 pour vous en savoir plus",
    display_name:'Afficher un nom',
    profession:'Profession',
    age:'Âge',
    height:'la taille',
    weight:'Poids',
    about:'Sur',
    ethnicities:'Ethnies',
    body_types:'Types de corps',
    create_profile:'Créer un profil',

     //edit profile
     save:'sauvegarder',
     name:'Nom',
     edit_profile:'Editer le profil',
     email:'Email',
     ethnicity:'Ethnicité',
     gender:'Le sexe',
     interested_gender:"Sexe intéressé",
     country:'Pays',
     state:'Etat',
     city:'Ville',
     add_document:'Télécharger le document de vérification pour D8 Gold Tick',
     document_added:'Document ajouté',
     add_video:'Ajouter une vidéo',
     video_added:'Vidéo ajoutée',

  

  //enter OTP
  otp:'OTP',
  send_otp:'Envoyer OTP',
  enter_otp:'Entrer OTP',
  enter_otp_email:'Enter the OTP sent on your email',
  verify_otp:'Vérifier OTP',




  //enter password
  forgot_password:'Mot de passe oublié',
  please_update_your_password:'Veuillez mettre à jour votre mot de passe',
  new_password:'nouveau mot de passe',
  confirm_password:'Confirmez le mot de passe',
  update:'Mise à jour',


  //Forgot password
  enter_email_address_used_to_register:"Entrez l'adresse e-mail que vous avez utilisée pour vous inscrire",


    //filters
    choose_desired_location_for_d8:"Choisissez les villes souhaitées pour obtenir la liste des D8 disponibles",
    cities:'Villes',
    filters:'Filtres',
    change:'Changement',
    states:'États',
    countries:'Des pays',
    age:'Âge',
    close:'Fermer',
    male:'Mâle',
    female:'Femme',
    submit:'Soumettre',
    height_cms:'Hauteur (en cm)',
    weight_kgs:'Poids (en kg)',
    about_bio:'À propos de la bio (facultatif)',
    ethnicity:'Ethnicité',
    body_type:'Type de corps',

    
    //friends
    friends:'Copains',
    see_pending_requests:'Voir les demandes de vitesse D8 en attente',

    //friend story
    story:'Récit',

    //interested gender
    please_select_interested_gender:'Veuillez sélectionner votre sexe intéressé',
    //gender
    please_select_your_gender:"S'il vous plait selectionnez votre genre",

    //login
    user_name:"Nom d'utilisateur",
    password:'Mot de passe',
    login_with_social_account:'Connectez-vous avec un compte social',
    login:"S'identifier",
    dont_have_account:"Je n'ai pas de compte",
    sign_up_now:"S'inscrire maintenant",

    //register
    register:"S'inscrire",

    //models swiper
    reset:'Réinitialiser',
    request_text:"En demandant un Speed ​​D8, vous dites à cette personne que vous êtes prêt et disposé à vous rencontrer. Si vous n'êtes pas prêt à vous rencontrer, utilisez plutôt le bouton Chat pour les connaître en premier.",
    accept_and_dont_show:"Acceptez et ne montrez plus cela",
    please_wait_loading:"Veuillez patienter pendant que nous chargeons les D8 pour vous ...",
    you_have_winked:"Vous avez fait un clin d'œil.",

    //my profile
    my_profile:'Mon profil',
    verified_user:'Utilisateur vérifié',
    yes:'Oui',
    no:'Non',
    years:'Ans',
    cms:'cms',
    lbs:'lbs',
    location:'Emplacement',

    //notifiactoin
    notifications:'Notifications',

    //payment-mode
    payment:'Paiement',
    payment_mode:'Mode de paiement',
    payment_type_choose:"Presque là, sélectionnez le type de paiement pour répondre à votre D8",

    //pending friend
    accept_text:"En acceptant une demande `` Speed ​​D8 '', vous dites à cette personne que vous êtes prêt à vous rencontrer.Si vous n'êtes pas prêt à vous rencontrer, vous pouvez cliquer sur le bouton `` Chat d'abord '' ci-dessous pour les vérifier, ou sur `` non '' bouton pour dire au revoir.",
    accept:"J'accepte",
    chat_first:'Chat en premier',
    nope:'Nan',
    speed_d8_requests:"Accélérer les demandes D8",


    //profile menu
    stories:'Histoires',
    profile:'Profil',
    chats:'Chattes',
    buy_subscriptions:'Acheter des abonnements',
    change_language:'Changer de langue',
    reset_password:'réinitialiser le mot de passe',
    logout:'Se déconnecter',
    indiscreet_mode:'Mode indiscret',
    discreet_mode:'Mode discret',
    logout:'Se déconnecter',
    are_sure_to_logout:'Êtes-vous sûr de vous déconnecter?',


    //register 
    app_user_name:"Nom d'utilisateur de l'application",
    accept_terms_and_conditions:'Accepter les termes et conditions',
    already_have_an_account :'Vous avez déjà un compte ?',
    sign_in_now:'Connectez vous maintenant',
    register_with_social_account:"S'inscrire avec un compte social",


   //reset password
  reset_password:'réinitialiser le mot de passe',
  please_update_your_password:'Veuillez mettre à jour votre mot de passe',
  old_password:'ancien mot de passe',
  new_password:'nouveau mot de passe',
  confirm_password:'Confirmez le mot de passe',
  reset:'Réinitialiser',


  //stories
  my_story:'Mon histoire',
  friends_story:"Histoire d'amis",
  stories:'Histoires',
  story_posted:'Histoire publiée',
  no_story_found:'Aucune histoire trouvée',


  ///subscripti
  subscriptions:'Abonnements',
  choose_subscription:'Choisissez les abonnements',
  yes_subscription_one:"Vous avez choisi un choix populaire en fournissant",
  yes_subscription_two:"d'opportunités",
  no_subscription_one:"Oups, vous n'avez pas d'abonnement. Dépêchez-vous et obtenez-en un pour commencer D8ing immédiatement",
  no_subscription_two:"Dépêchez-vous de faire votre choix pour obtenir un accès complet et démarrer D8ing immédiatement",
  select_subscription:"Faites votre choix ci-dessous",

  //update ountry
  choose_country:'Choisissez un pays',

  //welcome
lets_get_started:"Commençons",
our_speedd8:"Notre bouton SpeedD8 permet aux membres aventureux  \n de se rencontrer maintenant.",
hi:"salut",
welcome_to_d8:"Salut, bienvenue à D8. L'application D8ing de soutien et progressive pour tout le monde",



//filterlocationsheet
choose_location_search:"Choisissez des emplacements pour rechercher les D8",
ok:"D'accord",


//location sheet chat
choose_location_chat:"Envoyez-en un où vous aimeriez rencontrer.",
choose_range:"Sélectionnez la gamme de D8 près de chez vous",
distance:"Distance",
search:'Chercher',
cancel:'Annuler',

//story dialoge
video:'Vidéo',
create_story_to_share:'Créez une histoire à partager avec vos amis acceptés.',
choose_story_image:"Choisissez votre histoire d'image et partagez-la avec d'autres D8",
choose_story_video:"Choisissez votre histoire vidéo et partagez-la avec d'autres D8",
choose_story_source:"Choisissez la source de l'histoire",
image:'Image',

//settings
settings:'Réglages',
delete_account:'Supprimer le compte',
delete_account_text:"La suppression de votre compte est permanente et supprimera tout le contenu, y compris les amis, les demandes, les chats et les paramètres de profil. Voulez-vous vraiment supprimer votre compte?"









  
};
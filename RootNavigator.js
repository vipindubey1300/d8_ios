import { createAppContainer ,createSwitchNavigator} from 'react-navigation';
    import { createStackNavigator ,Header} from 'react-navigation-stack';
    import { createDrawerNavigator } from 'react-navigation-drawer';
     import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
  import React from 'react';
import { colors,urls} from './src/Constants';
import {StatusBar,TouchableOpacity,Image,Text,Platform,ToastAndroid} from 'react-native';


//screens
import Splash from './src/screens/Splash';
import Welcome from './src/screens/Welcome';
import Login from './src/screens/Login';
import Register from './src/screens/Register';
import ForgotPassword from './src/screens/ForgotPassword';
import ResetPassword from './src/screens/ResetPassword';
import SelectGender from './src/screens/SelectGender';
import InterestedGender from './src/screens/InterestedGender';
import PrivacyPolicy from './src/screens/PrivacyPolicy';
import CreateProfile from './src/screens/CreateProfile';
import ModelsSwiper from './src/screens/ModelsSwiper';
import ProfileMenu from './src/screens/ProfileMenu';
import EnterOtp from './src/screens/EnterOtp';
import EnterPassword from './src/screens/EnterPassword';
import UpdateCountry from './src/screens/UpdateCountry';
import TermsCondition from './src/screens/TermsCondition';
import Notifications from './src/screens/Notifications';
import Subscriptions from './src/screens/Subscriptions';
import PaymentMode from './src/screens/PaymentMode';
import Friends from './src/screens/Friends';
import UserProfile from './src/screens/UserProfile';
import PendingFriends from './src/screens/PendingFriends';
import MyProfile from './src/screens/MyProfile';
import EditProfile from './src/screens/EditProfile';
import Stories from './src/screens/Stories';
import FriendStory from './src/screens/FriendStory';
import MyStory from './src/screens/MyStory';
import AddStory from './src/screens/AddStory';
import ChatLists from './src/screens/ChatLists';
import Chats from './src/screens/Chats';
import BlockUser from './src/screens/BlockUser';
import ImageView from './src/screens/ImageView';
import ChangeLanguage from './src/screens/ChangeLanguage';
import Filters from './src/screens/Filters';
import ChooseLanguageRegister from './src/screens/ChooseLanguageRegister';
import Settings from './src/screens/Settings';
import DeleteAccount from './src/screens/DeleteAccount';
import ContactUs from './src/screens/ContactUs';

console.disableYellowBox = true;





const homeStack = createStackNavigator({
  ModelsSwiper :{ screen: ModelsSwiper},
  ProfileMenu :{ screen: ProfileMenu},
  ResetPassword :{ screen: ResetPassword},
  Notifications :{ screen: Notifications},
  Subscriptions :{ screen: Subscriptions},
  PaymentMode :{ screen: PaymentMode},
  Friends :{ screen: Friends},
  UserProfile :{ screen: UserProfile},
  PendingFriends :{ screen: PendingFriends},
  EditProfile :{ screen: EditProfile},
  MyProfile :{ screen: MyProfile},
  Stories :{ screen: Stories},
  FriendStory :{ screen: FriendStory},
  MyStory :{ screen: MyStory},
  AddStory :{ screen: AddStory},
  ChatLists :{ screen: ChatLists},
  Chats :{ screen: Chats},
  BlockUser :{ screen: BlockUser},
  ImageView :{ screen: ImageView},
  ChangeLanguage :{ screen: ChangeLanguage},
  Filters :{ screen: Filters},
  Settings :{ screen: Settings},
  DeleteAccount :{ screen: DeleteAccount},
  ContactUs :{ screen: ContactUs},
  


  }, {
     headerMode: 'none',
     initialRouteName: 'ModelsSwiper',
     navigationOptions: {
      gesturesEnabled: true
    }
})


    const welcomeStack = createStackNavigator({
      Welcome :{ screen: Welcome},
      Login :{ screen: Login},
      Register :{ screen: Register},
      ForgotPassword :{ screen: ForgotPassword},
      SelectGender :{ screen: SelectGender},
      InterestedGender :{ screen: InterestedGender},
      ChooseLanguageRegister :{ screen: ChooseLanguageRegister},
      CreateProfile :{ screen: CreateProfile},
      EnterOtp :{ screen: EnterOtp},
      EnterPassword :{ screen: EnterPassword},
      UpdateCountry :{ screen: UpdateCountry},
      TermsCondition :{ screen: TermsCondition},


    }, {
         headerMode: 'none',
         initialRouteName: 'Welcome',
         navigationOptions: {
          gesturesEnabled: true
        }
    })

    const AppStack = createSwitchNavigator({
      Splash : { screen: Splash},
      Welcome : { screen: welcomeStack},
      Home:{screen: homeStack},
      PrivacyPolicy : { screen: PrivacyPolicy},
     


    }, {
         headerMode: 'none',
         initialRouteName: 'Splash',
         navigationOptions: {
          gesturesEnabled: false
        }

    })


  const RootNavigator = createAppContainer(AppStack)
 export default RootNavigator;

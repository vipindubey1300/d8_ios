import React from 'react';

import store from './src/store/store';
import { Provider } from 'react-redux';
import RootNavigator from './RootNavigator';
import NavigationService from './NavigationService';
import firebase from 'react-native-firebase';
import type, { Notification, NotificationOpen } from 'react-native-firebase';
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';



export default class App extends React.Component {

	constructor(props) {
		super(props);
	}

	async createNotificationListeners() {

	const channel = new firebase.notifications.Android.Channel(
					'notification_channel_name', // To be Replaced as per use
					'Notifications', // To be Replaced as per use
					firebase.notifications.Android.Importance.Max)
					.setDescription('A Channel To manage the notifications related to Application');

	firebase.notifications().android.createChannel(channel);

	
	firebase.messaging().ios.registerForRemoteNotifications()

			/*
			* Triggered when a particular notification has been received in foreground
			* */
			//last update --- android foreground this is called
			this.notificationListener = firebase.notifications().onNotification((notification) => {
					const { title, body } = notification;
					console.log("GGGGGGGGGGGGGGGG",notification)
					this.display(notification)

			});


			firebase.notifications().onNotificationDisplayed((notification: Notification) => {
				console.log("HHHHHHHH",notification)
				// Process your notification as required
				// ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
			});

			/*
			* Triggered for data only payload in foreground
			* */
			//last update --- ios foreground this is called
		this.messageListener = firebase.messaging().onMessage((message) => {
					//this is called when app is in foregrpund
					console.log("AAYAAYAYAYAYAYAAYAAY",message)
					this.displayNotification(message)
		});


			/*
			* If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
			* */
	         const notificationOpen = await firebase.notifications().getInitialNotification();

			if (notificationOpen) {
			 const { title, body } = notificationOpen.notification;
			 //console.log("NOTIFICATION CLICK",JSON.stringify(notificationOpen.notification))
        	//this.showAlert(title, body);
			// ToastAndroid.show("Called when notification is clicked",ToastAndroid.LONG)
		

		var seen = [];
		console.log("NOTIFICATION CLICK BACKGROUND",JSON.stringify(notificationOpen, (key, val) => {
			if (val != null && typeof val == "object") {
				 if (seen.indexOf(val) >= 0) {
					 return;
				 }
				 seen.push(val);
			 }
			 return val;
		 }))


			
		firebase.notifications().removeAllDeliveredNotifications()

				//Alert.alert("ANdroid")
				// ToastAndroid.show("Called when notification is clicked in background android",ToastAndroid.LONG)

				//take action here

			}


			/*
			* If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
			* */
			this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {

				var seen = [];
			//ToastAndroid.show("this is called when app is open in android",ToastAndroid.LONG)
			console.log("NOTIFICATION CLICK",JSON.stringify(notificationOpen, (key, val) => {
				if (val != null && typeof val == "object") {
					 if (seen.indexOf(val) >= 0) {
						 return;
					 }
					 seen.push(val);
				 }
				 return val;
			 }))

			 firebase.notifications().removeAllDeliveredNotifications()

			
			//Alert.alert("IOS")

			//take action her
			});


			
		}


	displayNotification = (notification) => {
				 if (Platform.OS === 'android') {
		   
		   
					 const localNotification = new firebase.notifications.Notification({
						 sound: 'default',
						 show_in_foreground: true,
					 }).setNotificationId(notification._from)
					 .setTitle(notification._data.result)
					 .setSubtitle(notification.subtitle)
					 .setBody(notification._data.content)
					 .setData(notification.data)
						 .android.setChannelId('notification_channel_name') // e.g. the id you chose above
						 .android.setSmallIcon('logo') // create this icon in Android Studio
						 .android.setColor('#D3D3D3') // you can set a color here
						 .android.setPriority(firebase.notifications.Android.Priority.High);
		   
						// ToastAndroid.show("notification ...",ToastAndroid.LONG)
		   
					 firebase.notifications()
						 .displayNotification(localNotification)
						 .catch(err => ToastAndroid.show(JSON.stringify(err),ToastAndroid.LONG));
		   
				 }
				 else if (Platform.OS === 'ios') {
				   //console.log(notification);
				 	//Alert.alert("IOS 2")
					 const { title, body } = notification;
					 const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification._from)
            .setTitle(notification._data.result)
          .setSubtitle(notification.subtitle)
          .setBody(notification._data.content)
          .setData(notification.data)
					  // .ios.setBadge(notification.ios.badge);
		   
				   firebase.notifications()
					   .displayNotification(localNotification)
					   .catch(err => console.error("99999",err));
		   
			   }
			 }
	 display = (notification) => {
		   
			 //ToastAndroid.show(JSON.stringify(notification),ToastAndroid.LONG)
		   
		   
		   
			   if (Platform.OS === 'android') {
				 const { title, body } = notification;
				   const localNotification = new firebase.notifications.Notification({
					   sound: 'default',
					   show_in_foreground: true,
				   }).setNotificationId(notification.notificationId)
				   .setTitle(title)
				   .setSubtitle(title)
				   .setBody(body)
				   .setData(notification.data)
					   .android.setChannelId('notification_channel_name') // e.g. the id you chose above
					   .android.setSmallIcon('logo') // create this icon in Android Studio
					   .android.setColor('#D3D3D3') // you can set a color here
					   .android.setPriority(firebase.notifications.Android.Priority.High);
		   
				   firebase.notifications()
					   .displayNotification(localNotification)
					   .catch(err => ToastAndroid.show(err.message,ToastAndroid.LONG));
		   
			   }
			   else if (Platform.OS === 'ios') {
				// Alert.alert("called when app is in foreground ios")
				 const { title, body } = notification;
				 const localNotification = new firebase.notifications.Notification()
					 .setNotificationId(notification.notificationId)
					 .setTitle(title)
				   //.setSubtitle(title)
				   .setBody(body)
				   .setData(notification.data)
					// .ios.setBadge(notification.ios.badge);
		   
				 firebase.notifications()
					 .displayNotification(localNotification)
					 .catch(err => console.error(err));
		   
			 }
		   }		



 componentDidMount() {
			this.createNotificationListeners(); //add this line
	
    }
	
		componentWillUnmount() {
			this.notificationListener();
		  this.notificationOpenedListener();
		  }

	render() {
		return (
			<Provider store={store}>
					<RootNavigator 
					ref={navigatorRef => {
						NavigationService.setTopLevelNavigator(navigatorRef);
						}}
				/>
			</Provider>
		);
	}
}
